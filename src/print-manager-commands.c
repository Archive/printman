/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "print-manager-commands.h"

#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

/*
 * Finding Queues
 */
#if defined (linux)
#define LIST_PRINTERS "LANG=C lpstat -v | " \
		    "awk '" \
		    " $2 == \"for\" " \
		    "   { " \
		    "      x = match($3, /:/); " \
		    "      print substr($3, 1, x-1)" \
		    "   }'"
#else
#define LIST_PRINTERS "env LC_ALL=C lpstat -v | " \
		    "nawk '" \
		    " $2 == \"for\" " \
		    "   { " \
		    "      x = match($3, /:/); " \
		    "      print substr($3, 1, x-1)" \
		    "   }'"
#endif

/*
 * Querying Queues: queuename
 */
#ifdef linux
#define LIST_GPM_QUEUE_ATTRS "LANG=C lpstat -v%s 2>&1 | awk '" \
                        "BEGIN { device=\"\"; rhost=\"\"; rp=\"\" } " \
                        "/device for/ { device = $4 } " \
                        "/system for/ { rhost = $4; rp = $3 } " \
                        "END { print device,rhost,rp }' OFS=:"
#define LIST_GPM_QUEUE_STATUS "env LC_ALL=C lpstat -a%s | awk '" \
			 "{if ($2 == \"not\") {print \"disabled\"}}'"
#define LIST_GPM_DEVICE_STATUS "env LC_ALL=C lpstat -p%s | " \
			  "awk '/disabled/ {print \"disabled\"}'"
#else
#define LIST_GPM_QUEUE_ATTRS \
	"env LC_ALL=C lpstat -v %s 2>&1 | nawk '" \
	"BEGIN { device=\"\"; rhost=\"\"; rp=\"\" } " \
	"/device for/ { device = $4 } " \
	"/system for/ { rhost = $4; x = match($7, /\\)/); " \
	"               if (x == 0) " \
	"                  rp = substr($3, 1, match($3, /:/) - 1); " \
	"               else " \
	"                  rp = substr($7, 1, x - 1) } " \
	"END { print device,rhost,rp }' OFS=:"
#define LIST_GPM_QUEUE_STATUS "env LC_ALL=C lpstat -a%s | awk '" \
			 "{if ($2 == \"not\") {print \"disabled\"}}'"
#define LIST_GPM_DEVICE_STATUS "env LC_ALL=C lpstat -p%s | " \
			  "awk '/disabled/ {print \"disabled\"}'"
#endif

/*
 * Printing Files: queuename, filepath 
 */
#ifdef linux
#define PRINT_FILE_CMD "lpr -P '%s' '%s'"
#else
#define PRINT_FILE_CMD "lp -d '%s' '%s'"
#endif

/*
 * Killing Jobs: queuename, jobnum
 */
#if defined (linux)
#define CANCEL_JOB "lprm -P\"%s\" \"%s\""
#else
#define CANCEL_JOB "cancel \"%s\"-\"%s\""
#endif

/*
 * Listing Jobs: hostname, queuename
 */
#if defined (linux)
#define LIST_REMOTE_JOBS "env LC_ALL=C lpq -l -P \"%s\""
#define LIST_LOCAL_JOBS "env LC_ALL=C lpq -l -P \"%s\""
#else
#define LIST_REMOTE_JOBS "env LC_ALL=C /usr/ucb/lpq -l -P \"%s:%s\""
#define LIST_LOCAL_JOBS "env LC_ALL=C /usr/ucb/lpq -l -P \"%s\""
#endif


#define LIST_TIMESTAMP "env LC_ALL=C lpstat -o %s"
/*
 * Finding default printer
 */

#    ifdef linux
#      define LIST_DEFAULT_PRINTER "LC_ALL=C lpstat -d | awk '" \
                                   "BEGIN { device = \"\" } " \
			           "/system default/ { device = $4 } " \
			           "END { print device }'"
#    else
#      define LIST_DEFAULT_PRINTER "env LC_ALL=C lpstat -d | grep -v " \
				   "'no system default' | nawk '" \
                                   "BEGIN { device = \"\" } " \
			           "/system default/ { device = $4 } " \
			           "END { print device }'"
#    endif

typedef struct {
	gint           pid;
	LPStatDataFn   stdout_cb;
	LPStatDataFn   stderr_cb;
	LPStatEndFn    end_cb;
	gpointer       user_data;
	gint           stdout_fd;
	gint           stdout_tag;
	gint           stderr_fd;
	gint           stderr_tag;
	gchar         *cmdline;
	LPStatCommand  command;
} LPStatProcess;

static void
cancel_internal (LPStatProcess *proc, gboolean cancelled)
{
	g_return_if_fail (proc != NULL);

	g_source_remove (proc->stdout_tag);
	g_source_remove (proc->stderr_tag);

	close (proc->stdout_fd);
	close (proc->stderr_fd);

	if (cancelled)
		kill (proc->pid, SIGQUIT);

	if (proc->end_cb)
		(*proc->end_cb) (proc->pid, cancelled, proc->user_data);

	g_free (proc->cmdline);
	g_free (proc);
}

static gboolean 
data_received_internal (LPStatProcess *proc,
			LPStatDataFn   data_cb,
			GIOChannel    *src, 
			GIOCondition   cond)
{
	gchar *data;
	gsize len, term;
	GError *error = NULL;
	GIOStatus status;

	if (!(cond & G_IO_IN)) {
		cancel_internal (proc, FALSE);
		return FALSE;
	}

	while (1) {
		status = g_io_channel_read_line (src, 
						 &data, 
						 &len, 
						 &term, 
						 &error);
		if (status != G_IO_STATUS_NORMAL || data == NULL)
			break;

		/* kill trailing newline */
		data [term] = '\0';

		if (g_getenv ("GNOME_PRINT_MANAGER_DEBUG"))
			g_printerr ("print-manager-commands %s DATA:\n%s\nDONE\n", 
				    proc->cmdline, 
				    data);
		if ((proc->command == LPSTAT_LIST_REMOTE_JOBS) ||
		     (proc->command == LPSTAT_LIST_LOCAL_JOBS)) {
			/* kill leading/trailing whitespace */
			/* This would cause parsing of lprng format to fail */
			g_strstrip (data);
		}

		if (strlen (data) && data_cb)
			(*data_cb) (proc->pid, data, proc->user_data);

		g_free (data);
	}

	return TRUE;
}

static gboolean
stdout_data_received_cb (GIOChannel *src, GIOCondition cond, gpointer user_data)
{
	LPStatProcess *proc = user_data;
	return data_received_internal (proc, proc->stdout_cb, src, cond);
}

static void
dump_to_stderr (int pid, gchar *line, gpointer user_data)
{
	g_printerr (line);
}

static gboolean
stderr_data_received_cb (GIOChannel *src, GIOCondition cond, gpointer user_data)
{
	LPStatProcess *proc = user_data;
	return data_received_internal (proc, 
				       proc->stderr_cb ? 
				               proc->stderr_cb : 
				               dump_to_stderr, 
				       src, 
				       cond);
}

static GHashTable *lpstat_procs = NULL;

gint 
lpstat_run (LPStatDataFn   stdout_cb,
	    LPStatDataFn   stderr_cb,
	    LPStatEndFn    end_cb,
	    gpointer       user_data,
	    LPStatCommand  command,
	    gchar        **command_args)
{
	LPStatProcess *proc;
	GError *error = NULL;
	gchar *cmd = NULL;
	GIOChannel *stdout_chan, *stderr_chan;

	switch (command) {
	case LPSTAT_LIST_PRINTERS:
		cmd = g_strdup (LIST_PRINTERS);
		break;
	case LPSTAT_LIST_DEFAULT_PRINTER:
		cmd = g_strdup (LIST_DEFAULT_PRINTER);
		break;
	case LPSTAT_LIST_REMOTE_JOBS:
#if defined (linux)
		cmd = g_strdup_printf (LIST_REMOTE_JOBS,
				       command_args [1]);
#else
		cmd = g_strdup_printf (LIST_REMOTE_JOBS, 
				       command_args [0],
				       command_args [1]);
#endif
		break;
	case LPSTAT_LIST_TIMESTAMP:
		cmd = g_strdup_printf (LIST_TIMESTAMP, command_args[0]);
		break;
	case LPSTAT_LIST_LOCAL_JOBS:
		cmd = g_strdup_printf (LIST_LOCAL_JOBS, command_args [0]);
		break;
	case LPSTAT_GET_QUEUE_ATTRS:
		cmd = g_strdup_printf (LIST_GPM_QUEUE_ATTRS, command_args [0]);
		break;
	case LPSTAT_GET_QUEUE_STATUS:
		cmd = g_strdup_printf (LIST_GPM_QUEUE_STATUS, command_args [0]);
		break;
	case LPSTAT_GET_DEVICE_STATUS:
		cmd = g_strdup_printf (LIST_GPM_DEVICE_STATUS, command_args [0]);
		break;
	case LPSTAT_CANCEL_JOB:
		cmd = g_strdup_printf (CANCEL_JOB, 
				       command_args [0], 
				       command_args [1]);
		break;
	case LPSTAT_PRINT_FILE:
		cmd = g_strdup_printf (PRINT_FILE_CMD, 
				       command_args [0], 
				       command_args[1]);
		break;
	}

	proc = g_new0 (LPStatProcess, 1);
	proc->stdout_cb = stdout_cb;
	proc->stderr_cb = stderr_cb;
	proc->end_cb    = end_cb;
	proc->user_data = user_data;

	{
		gchar *argv [] = { "/bin/sh", "-c", cmd, NULL };

		if (!g_spawn_async_with_pipes (NULL,
					       argv,
					       NULL,
					       0,
					       NULL,
					       NULL,
					       &proc->pid,
					       NULL,
					       &proc->stdout_fd,
					       &proc->stderr_fd,
					       &error)) {
			if (stderr_cb)
				(*stderr_cb) (0, error->message, user_data);

			if (end_cb)
				(*end_cb) (0, TRUE, user_data);

			g_error_free (error);
			g_free (proc);
			return 0;
		}

		proc->cmdline = cmd;
	}

	stdout_chan = g_io_channel_unix_new (proc->stdout_fd);
	g_io_channel_set_encoding (stdout_chan, NULL, NULL);
	proc->stdout_tag = g_io_add_watch (stdout_chan, 
					   G_IO_IN | G_IO_ERR | G_IO_HUP,
					   stdout_data_received_cb,
					   proc);
	g_io_channel_unref (stdout_chan);

	stderr_chan = g_io_channel_unix_new (proc->stderr_fd);
	proc->stderr_tag = g_io_add_watch (stderr_chan, 
					   G_IO_IN, 
					   stderr_data_received_cb, 
					   proc);
	g_io_channel_unref (stderr_chan);

	if (!lpstat_procs) 
		lpstat_procs = g_hash_table_new (NULL, NULL);

	g_hash_table_insert (lpstat_procs, GINT_TO_POINTER (proc->pid), proc);

	return proc->pid;
}

/*
 * Would be nice to give lpstat_procs a destoy callback (cancel_internal), to
 * have this stuff simplified...  
 */

void 
lpstat_cancel (gint lpstat_id)
{
	LPStatProcess *proc;

	g_return_if_fail (lpstat_id < 1);
	g_return_if_fail (lpstat_procs != NULL);

	proc = g_hash_table_lookup (lpstat_procs, GINT_TO_POINTER (lpstat_id));
	if (proc) {
		cancel_internal (proc, TRUE);
		g_hash_table_remove (lpstat_procs, GINT_TO_POINTER (lpstat_id));
	}
}

static gboolean
foreach_cancel (gpointer key, gpointer val, gpointer notused)
{
	cancel_internal ((LPStatProcess *) val, TRUE);
	return TRUE;
}

void 
lpstat_cancel_all (void)
{
	g_return_if_fail (lpstat_procs != NULL);

	g_hash_table_foreach_remove (lpstat_procs, foreach_cancel, NULL);
}

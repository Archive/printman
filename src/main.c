/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <unistd.h>
#include <gnome.h>

#include "print-manager-backend.h"
#include "print-prefs.h"

#include "print-manager-main-window.h"
#include "print-manager-job-list.h"

static gchar    *arg_printer   = NULL;
static gboolean  arg_default   = FALSE;
static gchar    *arg_edit      = FALSE;
static gchar    *arg_edit_path = NULL;

static GList    *open_queues = NULL;
static guint     backend_changes_done_id = 0;


static gint
session_save_yourself_cb (GnomeClient        *client, 
			  gint                phase,
			  GnomeRestartStyle   save_style, 
			  gboolean            shutdown,
			  GnomeInteractStyle  interact_style, 
			  gboolean            fast,
			  gpointer            client_data)
{
	GList *open_job_lists, *elem;
	char **argv;
	int max_args, arg = 0;

	open_job_lists = print_manager_get_open_job_lists ();

	/* Calculate the maximum number of args we will need. */
	max_args = (g_list_length (open_job_lists) * 2) + 5;
	argv = g_new (char*, max_args);

	argv [arg++] = "gnome-printinfo";

	if (arg_default) {
		argv [arg++] = "--default";
	} else if (arg_printer) {
		argv [arg++] = "--printer";
		argv [arg++] = arg_printer;
	} else {
		/* Add any open windows with -o options. */
		for (elem = open_job_lists; elem; elem = elem->next) {
			argv [arg++] = "--open";
			argv [arg++] = elem->data;
		}
	}

	/* Add the edit gconf defaults path */
	if (arg_edit)
		argv [arg++] = "--edit";
	else if (arg_edit_path) {
		argv [arg++] = "--edit-path";
		argv [arg++] = arg_edit_path;
	}

	/* Check we haven't overflowed the buffer. Shouldn't happen. */
	if (arg >= max_args)
		g_warning ("Overflow of argv buffer");

	gnome_client_set_restart_command (client, arg, argv);

	g_free (argv);
	g_list_free (open_job_lists);

	return TRUE;
}
 
static void
session_die_cb (GnomeClient *client, gpointer client_data)
{
	gtk_main_quit ();
}

/* 
 * This is called for any --open command-line options. It adds the argument to
 * the list of queues to open. 
 */
static void
open_option_callback (poptContext              pctx, 
		      enum poptCallbackReason  reason,
		      const struct poptOption *opt, 
		      const char              *arg,
		      gpointer                 data)
{
	GList **open_queues = data;

	if (arg && *arg)
		*open_queues = g_list_prepend (*open_queues, g_strdup (arg));
}

static void
backend_changes_done (PrintManagerBackend *backend, gpointer data)
{
	g_signal_handler_disconnect (backend,
				     backend_changes_done_id);

	if (arg_default) {
		int default_idx = 
			print_manager_backend_get_default_printer (backend);

		if (default_idx == -1) {
			g_print (_("%s: No default printer set, or default "
				   "printer not found.  Showing printer "
				   "list.\n"),
				 g_get_prgname ());
			print_manager_show_main_window (backend);
		} else {
			PrintManagerDevice *device;
			device = print_manager_backend_get_device (backend, 
								   default_idx);
			print_manager_show_job_list (backend, 
						     device, 
						     TRUE, 
						     NULL);
		}
	} 
	else if (arg_printer) {
		int count = print_manager_backend_get_count (backend);
		int i;
		gboolean found = FALSE;

		for (i = 0; i < count; i++) {
			PrintManagerDevice *device;
			const char *queue;

			device = print_manager_backend_get_device (backend, i);
			queue = print_manager_device_get_queue_name (device);

			if (!strcmp (queue, arg_printer)) {
				print_manager_show_job_list (backend,
							     device,
							     TRUE,
							     NULL);
				found = TRUE;
				break;
			}
		}

		if (!found) {
			g_print (_("%s: Cannot open printer queue for unknown "
				   "printer \"%s\".\n"),
				 g_get_prgname (),
				 arg_printer);
			exit (1);
		}
	} 
	else if (open_queues) {
		int count = print_manager_backend_get_count (backend);
		int i;

		/* Show any queues specified with --open options. */
		for (i = 0; i < count; i++) {
			PrintManagerDevice *device;
			const char *queue;

			device = print_manager_backend_get_device (backend, i);
			queue = print_manager_device_get_queue_name (device);

			if (g_list_find_custom (open_queues, 
						queue,
						(GCompareFunc) strcmp)) {
				print_manager_show_job_list (backend,
							     device,
							     FALSE,
							     NULL);
			}
		}

		g_list_foreach (open_queues, (GFunc) g_free, NULL);
		g_list_free (open_queues);
	}

	/* release reference passed from backend creation in main () */
	g_object_unref (backend);
}

int
main (int argc, char *argv[])
{
	GnomeProgram *program;
	GnomeClient *client;
	PrintManagerBackend *backend;

	/* 
	 * We use a separate table for the --open option as we want to use
	 * our own callback to handle it. We need to do that as there may be
	 * multiple --open options. 
	 */
	struct poptOption open_option[] = {
		{ 
			NULL, 0,
			POPT_ARG_CALLBACK, open_option_callback, 0,
			(char*) &open_queues, NULL 
		},
		{ 
			"open", 'o',
			POPT_ARG_STRING, NULL, 0,
			_("Opens the printer queue window for the given "
			  "printer."),
			_("PRINTER") 
		},
		{ NULL, '\0', 0, NULL, 0, NULL, NULL },
	};

	struct poptOption options[] = {
		{ 
			"default", 'd', 
			POPT_ARG_NONE, &arg_default, 0, 
			_("Shows the printer queue for the default printer "
			  "only."), 
			NULL 
		},
		{ 
			"printer", 'p', 
			POPT_ARG_STRING, &arg_printer, 0, 
			_("Shows the printer queue window for the given "
			  "printer only."), 
			_("PRINTER") 
		},
		{ 
			"edit", 'e', 
			POPT_ARG_NONE, &arg_edit, 0, 
			_("Allows the superuser to edit the default label, "
			  "icon and description of printers, for the current "
			  "LANG setting."),
			NULL
		},
		{ 
			"edit-path", 0, 
			POPT_ARG_STRING, &arg_edit_path, 0, 
			_("Allows the superuser to edit the printer default "
			  "settings for the GConf source specified."),
			_("DEFAULTS SOURCE FILE")
		},
		{ 
			NULL, 0,
			POPT_ARG_INCLUDE_TABLE, open_option, 0,
			NULL, NULL 
		},
		{ NULL, '\0', 0, NULL, 0, NULL, NULL },
	};
	
	/* i18n calls */
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	program = gnome_program_init ("gnome-printinfo", 
				      VERSION, 
				      LIBGNOMEUI_MODULE,
				      argc, 
				      argv, 
				      GNOME_PARAM_POPT_TABLE, 
				      options,
				      GNOME_PARAM_HUMAN_READABLE_NAME, 
				      _("GNOME Print Manager"),
				      GNOME_PARAM_APP_DATADIR, DATADIR,
				      NULL);

	/* Set up session management*/
	client = gnome_master_client ();		 
	g_signal_connect (client, "save_yourself",
			  G_CALLBACK (session_save_yourself_cb), NULL);
	g_signal_connect (client, "die", G_CALLBACK (session_die_cb), NULL);

	/* 
	 * Use admin defaults editing.  arg_edit_path with either be NULL to 
	 * use the gconf defaults file, or specify a path to edit.
	 */
	if (arg_edit || arg_edit_path) {
		if (getuid () != 0) {
			g_print (_("%s: You must be root in order to edit "
				   "system printer default settings.\n"),
				 g_get_prgname ());
			return 1;
		}

		if (!print_manager_prefs_set_gconf_admin_path (arg_edit_path)) {
			g_print (_("%s: Unable to open system printer default "
				   "settings at path \"%s\".\n"),
				 g_get_prgname (),
				 arg_edit_path ? 
				 	arg_edit_path : 
				 	PRINTMAN_GCONF_DEFAULTS_PATH);
			return 1;
		}
	}

	backend = print_manager_backend_new ();

	if (!arg_printer && !arg_default)
		print_manager_show_main_window (backend);

	/* pass our creation reference along to backend_changes_done() */
	backend_changes_done_id =
		g_signal_connect (backend, "changes_done",
				  G_CALLBACK (backend_changes_done), NULL);

	gtk_main ();

	return 0;
}

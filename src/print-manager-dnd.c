/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-dnd.h"

#include <string.h>
#include <libgnomevfs/gnome-vfs-utils.h>

static GtkTargetEntry drop_types[] = {
	{ "text/uri-list", 0, 0 },
	{ "FILE_NAME", 0, 1 },
};

static GList *
extract_uris (const char *uri_list)
{
        /* 
	 * Note that this is mostly very stolen from old 
	 * libgnome/gnome-mime.c 
	 */
        const gchar *p, *q;
        gchar *retval;
        GList *result = NULL;

        g_return_val_if_fail (uri_list != NULL, NULL);

        p = uri_list;

        /* We don't actually try to validate the URI according to RFC
         * 2396, or even check for allowed characters - we just ignore
         * comments and trim whitespace off the ends.  We also
         * allow LF delimination as well as the specified CRLF.
         */
        while (p != NULL) {
                if (*p != '#') {
                        while (g_ascii_isspace (*p))
                                p++;

                        q = p;
                        while ((*q != '\0')
                               && (*q != '\n')
                               && (*q != '\r'))
                                q++;

                        if (q > p) {
                                q--;
                                while (q > p && 
				       g_ascii_isspace (*q))
                                        q--;

                                retval = g_malloc (q - p + 2);
                                strncpy (retval, p, q - p + 1);
                                retval[q - p + 1] = '\0';

                                result = g_list_prepend (result, retval);
                        }
                }
                p = strchr (p, '\n');
                if (p != NULL)
                        p++;
        }

        return g_list_reverse (result);
}

static void
free_uri_list (GList *uris)
{
	GList *l;
	
	for (l = uris; l != NULL; l = l->next)
		g_free (l->data);
	
	g_list_free (uris);
}

static const char *
uri_to_local_file (char *uri)
{
	if (uri[0] == G_DIR_SEPARATOR)
		return uri;

	if (!strncmp (uri, "file:", strlen ("file:"))) {
		char *p = uri + strlen ("file:");
		while (*p && *p == '/')
			p++;
			
		p--;
		if (*p != '/')
			return NULL;
		else
			return p;
	} 

	return NULL;
}

static void
print_done_internal (PrintManagerDevice *device,
		     const gchar        *filename,
		     gpointer            user_data)
{
	GList *uris = user_data;
	const char *next_file;
	gchar *file_name;

	if (!uris)
		return;
	
	next_file = uri_to_local_file ((gchar *) uris->data);

	if (next_file)
		file_name = gnome_vfs_unescape_string (next_file, 	
						       G_DIR_SEPARATOR_S);

	if (file_name) {
		g_print ("printing %s to device %s\n", 
			 file_name, 
			 print_manager_device_get_queue_name (device));
		print_manager_device_print_file (device, 
						 (const char *)file_name,
						 print_done_internal,
						 uris->next);
	}

	g_free (file_name);
	if (!uris->next)
		free_uri_list (g_list_first (uris));
}

void 
print_manager_dnd_print_uri_list (PrintManagerDevice *device, 
				  const char         *uri_list)
{
	GList *uris = extract_uris (uri_list);

	print_done_internal (device, NULL, uris);
}

void 
print_manager_dnd_setup (GtkWidget *object,
			 GCallback  drag_motion_cb,
			 GCallback  drag_drop_cb,
			 GCallback  drag_data_received_cb,
			 gpointer   user_data)
{
	gtk_drag_dest_set (object, 
			   0, 
			   drop_types, 					      
			   sizeof (drop_types) / sizeof (drop_types[0]), 
			   GDK_ACTION_COPY);

	g_signal_connect (object, 
			  "drag_motion",
			  drag_motion_cb,
			  user_data);
	g_signal_connect (object,
			  "drag_drop",
			  drag_drop_cb,
			  user_data);
	g_signal_connect (object,
			  "drag_data_received",
			  drag_data_received_cb,
			  user_data);
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-backend-test.h"
#include "print-manager-device-test.h"
#include "print-manager-commands.h"
#include "print-prefs.h"
#include <libxml/parser.h>
#include <string.h>

#define PARENT_TYPE (PRINT_MANAGER_TYPE_BACKEND)

/* Static functions */

struct _PrintManagerBackendTestPrivate {
	char *filename;

	int count;
	PrintManagerDevice **devices;

	int default_printer;

	guint show_all_jobs : 1;
	guint show_printer_details : 1;
	guint show_status_bar : 1;

	int update_interval;

	guint idle_id;

	guint current_idle;
	guint refresh_idle_id;
};

#define GCONF_KEY_SHOW_ALL_JOBS        "show_all_jobs"
#define GCONF_KEY_SHOW_PRINTER_DETAILS "show_printer_details"
#define GCONF_KEY_SHOW_STATUS_BAR      "show_status_bar"
#define GCONF_KEY_UPDATE_INTERVAL      "update_interval"
#define GCONF_KEY_DEFAULT_PRINTER      "default_printer"

static gboolean
backend_refresh_cb (gpointer data)
{
	PrintManagerBackendTest *backend = data;

	print_manager_backend_reload (PRINT_MANAGER_BACKEND (backend));

	return TRUE;
}

static void
setup_idle (PrintManagerBackendTest *backend)
{
	if (backend->priv->current_idle !=
	    backend->priv->update_interval * 1000) {
		backend->priv->current_idle =
			backend->priv->update_interval * 1000;
		if (backend->priv->refresh_idle_id != 0)
			g_source_remove (backend->priv->refresh_idle_id);
		backend->priv->refresh_idle_id =
			g_timeout_add (backend->priv->current_idle,
				       backend_refresh_cb,
				       backend);
	}
}

static gboolean
reload_settings_from_gconf (PrintManagerBackendTest *backend, gboolean admin, gboolean def)
{
	gboolean set;
	gboolean show_all_jobs =
		GPOINTER_TO_INT (print_manager_prefs_get_gconf
				 (GCONF_KEY_SHOW_ALL_JOBS, admin, def, &set));
	if (set) {
		backend->priv->show_all_jobs = show_all_jobs;
		backend->priv->show_printer_details =
			GPOINTER_TO_INT (print_manager_prefs_get_gconf
					 (GCONF_KEY_SHOW_PRINTER_DETAILS,
					  admin, 
					  def,
					  NULL));
		backend->priv->show_status_bar =
			GPOINTER_TO_INT (print_manager_prefs_get_gconf
					 (GCONF_KEY_SHOW_STATUS_BAR, admin,
					  def,
					  NULL));
		backend->priv->update_interval =
			GPOINTER_TO_INT (print_manager_prefs_get_gconf
					 (GCONF_KEY_UPDATE_INTERVAL, admin,
					  def,
					  NULL));
		backend->priv->default_printer =
			GPOINTER_TO_INT (print_manager_prefs_get_gconf
					 (GCONF_KEY_DEFAULT_PRINTER, admin,
					  def,
					  NULL));
	}
	return set;
}

static void
reload_settings (PrintManagerBackendTest *backend)
{
	if (print_manager_prefs_is_gconf_admin_mode ()) {
		if (reload_settings_from_gconf (backend, TRUE, TRUE))
			return;
		reload_settings_from_gconf (backend, FALSE, TRUE);
	} else {
		if (reload_settings_from_gconf (backend, FALSE, FALSE))
			return;
		if (reload_settings_from_gconf (backend, TRUE, TRUE))
			return;
		reload_settings_from_gconf (backend, FALSE, TRUE);
	}
}

static void
save_settings (PrintManagerBackendTest *backend)
{
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_ALL_JOBS,
		FALSE,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_all_jobs));
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_PRINTER_DETAILS,
		FALSE,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_printer_details));
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_STATUS_BAR,
		FALSE,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_status_bar));
	print_manager_prefs_set_gconf (
		GCONF_KEY_UPDATE_INTERVAL,
		FALSE,
		PRINT_PREF_TYPE_INT,
		GINT_TO_POINTER (backend->priv->update_interval));
	print_manager_prefs_set_gconf (
		GCONF_KEY_DEFAULT_PRINTER,
		FALSE,
		PRINT_PREF_TYPE_INT,
		GINT_TO_POINTER (backend->priv->default_printer));
	print_manager_backend_prefs_changed (PRINT_MANAGER_BACKEND (backend));
}

static gboolean
device_changed_idle (gpointer data)
{
	PrintManagerBackendTest *test = data;
	PrintManagerBackend *backend = data;

	print_manager_backend_changed (backend);
	print_manager_backend_changes_done (backend);
	test->priv->idle_id = 0;
	return FALSE;
}

static void
device_changed (PrintManagerDevice *device, PrintManagerBackendTest *test)
{
	if (test->priv->idle_id == 0) {
		test->priv->idle_id = g_idle_add (device_changed_idle, test);
	}
}

/* access functions */
static gint
pmbt_get_count (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	g_return_val_if_fail (test != NULL, -1);

	return test->priv->count;
}

static PrintManagerDevice *
pmbt_get_device (PrintManagerBackend *backend, int n)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	g_return_val_if_fail (test != NULL, NULL);
	g_return_val_if_fail (n >= 0, NULL);
	g_return_val_if_fail (n < test->priv->count, NULL);

	return test->priv->devices[n];
}

static PrintManagerDevice *
maybe_new_device (PrintManagerBackendTest *test,
		  char *directory,
		  char *queue_name)
{
	int i;
	int count = test->priv->count;
	PrintManagerDevice *new_device;
	for (i = 0; i < count; i++) {
		if (test->priv->devices[i] &&
		    !strcmp (print_manager_device_get_queue_name (
				     test->priv->devices[i]),
			     queue_name)) {
			new_device = test->priv->devices[i];
			test->priv->devices[i] = NULL;
			print_manager_device_reload (new_device);
			return new_device;
		}
	}
	new_device = print_manager_device_test_new (directory, queue_name);

	g_signal_connect (new_device, "changed",
			  G_CALLBACK (device_changed), test);
	return new_device;
}


/* Change functions */
static void
pmbt_reload (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);
	xmlDoc *doc;
	xmlNode *node;
	xmlNode *root = NULL;
	PrintManagerDevice **new_devices;
	int new_count;
	int count;
	int i;
	char *directory;

	reload_settings (test);
	setup_idle (test);

	if (test->priv->filename == NULL)
		return;

	doc = xmlParseFile (test->priv->filename);

	if (doc != NULL) {
		root = xmlDocGetRootElement (doc);
	}
	new_devices = NULL;
	new_count = 0;

	if (root != NULL) {

		for (node = root->xmlChildrenNode; node; node = node->next) {
			if (!xmlNodeIsText (node))
				new_count ++;
		}

		new_devices = g_new (PrintManagerDevice *, new_count);

		if (strchr (test->priv->filename, '/')) {
			directory = g_strdup (test->priv->filename);
			*(strrchr (directory, '/') + 1) = 0;
		} else {
			directory = g_strdup ("");
		}

		i = 0;

		for (node = root->xmlChildrenNode; node; node = node->next) {
			if (!xmlNodeIsText (node)) {
				char *queue_name = xmlGetProp (node,
							       "queue-name");

				new_devices[i] = maybe_new_device (test,
								   directory,
								   queue_name);

				xmlFree (queue_name);

				i++;
			}
		}

		g_free (directory);
	}

	count = test->priv->count;
	for (i = 0; i < count; i++) {
		if (test->priv->devices[i])
			g_object_unref (test->priv->devices[i]);
	}
	g_free (test->priv->devices);
	test->priv->count = new_count;
	test->priv->devices = new_devices;

	print_manager_backend_changed (backend);
	print_manager_backend_changes_done (backend);
}

static gboolean
pmbt_get_show_all_jobs (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	return test->priv->show_all_jobs;
}

static void
pmbt_set_show_all_jobs (PrintManagerBackend *backend,
			gboolean             show_all_jobs)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	test->priv->show_all_jobs = show_all_jobs;
	save_settings (test);
}

static gboolean
pmbt_get_show_printer_details (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	return test->priv->show_printer_details;
}

static void
pmbt_set_show_printer_details  (PrintManagerBackend *backend,
				gboolean             show_printer_details)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	test->priv->show_printer_details = show_printer_details;
	save_settings (test);
}

static gboolean
pmbt_get_show_status_bar (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	return test->priv->show_status_bar;
}

static void
pmbt_set_show_status_bar (PrintManagerBackend *backend,
			  gboolean             show_status_bar)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	test->priv->show_status_bar = show_status_bar;
	save_settings (test);
}

static gint
pmbt_get_update_interval (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	return test->priv->update_interval;
}

static void
pmbt_set_update_interval (PrintManagerBackend *backend,
			  gint                 update_interval)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	test->priv->update_interval = update_interval;
	save_settings (test);
	setup_idle (test);
}

static gint
pmbt_get_default_printer (PrintManagerBackend *backend)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	return test->priv->default_printer;
}

static void
pmbt_set_default_printer (PrintManagerBackend *backend,
						   gint default_printer)
{
	PrintManagerBackendTest *test = PRINT_MANAGER_BACKEND_TEST (backend);

	test->priv->default_printer = default_printer;
	save_settings (test);
}

static void
print_manager_backend_test_class_init (PrintManagerBackendTestClass *klass)
{
	PrintManagerBackendClass *backend_class = 
		PRINT_MANAGER_BACKEND_CLASS (klass);

        backend_class->get_count = pmbt_get_count;
        backend_class->get_device = pmbt_get_device;

        backend_class->reload = pmbt_reload;

        backend_class->get_show_all_jobs = pmbt_get_show_all_jobs;
        backend_class->set_show_all_jobs = pmbt_set_show_all_jobs;

        backend_class->get_show_printer_details = pmbt_get_show_printer_details;
        backend_class->set_show_printer_details = pmbt_set_show_printer_details;

        backend_class->get_show_status_bar = pmbt_get_show_status_bar;
        backend_class->set_show_status_bar = pmbt_set_show_status_bar;

        backend_class->get_update_interval = pmbt_get_update_interval;
        backend_class->set_update_interval = pmbt_set_update_interval;

        backend_class->get_default_printer = pmbt_get_default_printer;
        backend_class->set_default_printer = pmbt_set_default_printer;
}

static void
print_manager_backend_test_init (PrintManagerBackendTest *test)
{
	PrintManagerBackend *backend = PRINT_MANAGER_BACKEND (test);

	test->priv = g_new (PrintManagerBackendTestPrivate, 1);

	test->priv->filename             = NULL;

	test->priv->count                = 0;
	test->priv->devices              = NULL;

	test->priv->show_all_jobs        = TRUE;
	test->priv->show_printer_details = FALSE;
	test->priv->show_status_bar      = TRUE;

	test->priv->update_interval      = 30;
	test->priv->default_printer      = -1;

	test->priv->idle_id              = 0;
 
	test->priv->current_idle         = 0;
	test->priv->refresh_idle_id      = 0;

	print_manager_backend_reload (backend);
}

/**
 * print_manager_backend_test_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerBackendTest class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerBackendTest class.
 **/
GType
print_manager_backend_test_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerBackendTestClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) print_manager_backend_test_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerBackendTest),
			0,
			(GInstanceInitFunc) print_manager_backend_test_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerBackendTest",
					       &info, 0);
	}

	return type;
}

PrintManagerBackend *
print_manager_backend_test_new (char *filename)
{
	PrintManagerBackendTest *test = 
		PRINT_MANAGER_BACKEND_TEST (
			g_type_create_instance (
				PRINT_MANAGER_TYPE_BACKEND_TEST));

	test->priv->filename = g_strdup (filename);

	return PRINT_MANAGER_BACKEND (test);
}

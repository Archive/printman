/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Authors: 
 *   Christopher James Lahey <clahey@ximian.com>
 *
 * Copyright (C) 2002 Sun Microsystems, Inc.
 */

#include <config.h>

#include "print-manager-search.h"
#include "print-manager-job-list.h"
#include "print-manager-access.h"

#include <gnome.h>
#include <string.h>

typedef struct {
	PrintManagerBackend *backend;

	GList               *items; /* Of type SearchQueueItem * */
	guint                unfinished_count;

	GtkWidget           *start;
	GtkWidget           *stop;
	GtkWidget           *dialog;
	GtkWidget           *document_name_widget;
	GtkWidget           *only_user_owned_widget;
	GtkWidget           *ignore_case_widget;
	GtkWidget           *exact_match_widget;
	GtkWidget           *tree_view;
	GtkWidget           *frame;
	GtkWidget           *goto_document;
	GtkWidget           *cancel_document;

	gchar               *document_name;
	guint                only_user_owned : 1;
	guint                ignore_case : 1;
	guint                exact_match : 1;

	guint                queue_search_timeout_id;

	GtkListStore        *store;
} Search;

typedef struct {
	PrintManagerDevice *device;
	PrintManagerQueue  *queue;

	int                 queue_changed_id;
	int                 queue_changes_done_id;
	GList              *jobs; /* Of type PrintJob */
	Search             *search;

	guint               is_started : 1;
	guint               was_unfinished : 1;
	gboolean            done_reload;
} SearchQueueItem;

enum {
	COL_DOC_NAME,
	COL_OWNER,
	COL_DEVICE_LABEL,
	COL_JOB_NUM,
	COL_SUBMIT,
	COL_DEVICE,
	COL_JOB,
	LAST_COL
};

static void stop_search_cb (GtkWidget *widget, Search *search);

static gint
find_job_delete_event_cb (GtkWidget *dialog, GdkEventAny *event, gpointer user_data)
{
	Search *search = (Search *)user_data;
	stop_search_cb (NULL, search);
	gtk_widget_hide (search->dialog);
	return TRUE;
}
/*
 * Find job dialog.
 */

static void
find_job_response_cb (GtkWidget * dialog, gint response_id, Search *search)
{
	switch (response_id) {
	case GTK_RESPONSE_CLOSE:
		stop_search_cb (NULL, search);
		gtk_widget_hide (search->dialog);
		break;
	case GTK_RESPONSE_HELP:
		gnome_help_display ("gnome-print-manager.xml", 
				    "view-printjobs", 
				    NULL);
		break;
	}
}

static void
stop_signals_item (gpointer data, gpointer user_data)
{
	SearchQueueItem *item = data;

	if (item->queue_changed_id)
		g_signal_handler_disconnect (item->queue,
					     item->queue_changed_id);
	item->queue_changed_id = 0;

	if (item->queue_changes_done_id)
		g_signal_handler_disconnect (item->queue,
					     item->queue_changes_done_id);
	item->queue_changes_done_id = 0;
}

static void
free_search_item (gpointer data, gpointer user_data)
{
	SearchQueueItem *item = data;

	stop_signals_item (data, user_data);

	g_object_unref (item->queue);
	g_object_unref (item->device);

	g_list_foreach (item->jobs, (GFunc) print_job_free, NULL);
	g_list_free (item->jobs);

	g_free (item);
}

static void
add_job (Search *search, PrintManagerDevice *device, PrintJob *job)
{
	GtkTreeIter iter;
	gtk_list_store_append (search->store, &iter);
	gtk_list_store_set (
		search->store, &iter, 
		COL_DOC_NAME, job->doc_name,
		COL_OWNER, job->owner,
		COL_DEVICE_LABEL, print_manager_device_get_label (device),
		COL_JOB_NUM, job->job_num,
		COL_SUBMIT, job->submit_date,
		COL_DEVICE, device,
		COL_JOB, job,
		-1);
}

static void
build_store_from_item (SearchQueueItem *item)
{
	GList *iterator;
	for (iterator = item->jobs; iterator; iterator = iterator->next) {
		PrintJob *job = iterator->data;

		add_job (item->search, item->device, job);
	}
}

static void
check_job (SearchQueueItem *item,
	   PrintJob *job)
{
	char *job_name;

	if (item->search->only_user_owned && !job->user_owned)
		return;

	if (item->search->ignore_case)
		job_name = g_utf8_casefold (job->doc_name, -1);
	else
		job_name = job->doc_name;

	if (item->search->exact_match) {
		if (!strcmp (job_name, item->search->document_name))
			item->jobs = g_list_prepend (item->jobs,
						     print_job_copy (job));
	} else {
		if (strstr (job_name, item->search->document_name))
			item->jobs = g_list_prepend (item->jobs,
						     print_job_copy (job));
	}
	if (item->search->ignore_case)
		g_free (job_name);
}

static void
do_queue_search (SearchQueueItem *item)
{
	int i;
	int job_count;

	g_list_foreach (item->jobs, (GFunc) print_job_free, NULL);
	g_list_free (item->jobs);
	item->jobs = NULL;

	job_count = print_manager_queue_get_count (item->queue);
	for (i = 0; i < job_count; i++) {
		PrintJob *job = print_manager_queue_get_job (item->queue, i);
		check_job (item, job);
	}
	item->jobs = g_list_reverse (item->jobs);
	item->is_started = TRUE;
}

static void 
search_complete_or_load_next (Search *search)
{
	GList *iter;
	SearchQueueItem *item;

	for (iter = search->items; iter; iter = iter->next) {
		item = iter->data;

		if (!item->is_started) {
			/* load the next queue */
			if (!print_manager_device_get_is_visible (item->device)
			    && !item->done_reload) {
				/* We need to do this only for non-visibles */
				item->done_reload = TRUE;
				print_manager_queue_reload (item->queue);
			}

			return;
		}
	}

}

static void
queue_changes_done (PrintManagerQueue *queue, SearchQueueItem *item)
{
	if (item->was_unfinished) {
		g_signal_handler_disconnect (item->queue,
					     item->queue_changes_done_id);
		item->queue_changes_done_id = 0;
		item->was_unfinished = FALSE;
		item->search->unfinished_count --;
		if (item->search->unfinished_count == 0)
			/* FIXME */;

		if (!item->is_started)
		{
			do_queue_search (item);	
			build_store_from_item (item);
			search_complete_or_load_next (item->search);
		}
	}
} 

static gboolean
check_if_job_exists (SearchQueueItem *item, PrintJob *new_job)
{
	int job_count, i;

	job_count = print_manager_queue_get_count (item->queue);

	for (i = 0; i < job_count; i++) {
		PrintJob *job = print_manager_queue_get_job (item->queue, i);

		if (print_job_equal (job, new_job))
			return TRUE;
	}

	return FALSE;
}

static void
remove_nonexistant_jobs (SearchQueueItem *item)
{
	gboolean ret_val;
	GtkTreeIter iter;
	Search *search = item->search;

	ret_val = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (search->store), &iter);

	while (ret_val) {
		PrintJob *job;
		const char *queue_name;

		gtk_tree_model_get (GTK_TREE_MODEL (search->store),
				    &iter,
				    COL_JOB, &job,
				    -1);

		queue_name = print_manager_queue_get_queue_name (item->queue);

		if (strcmp (queue_name, job->queue_name) == 0 &&
			!check_if_job_exists (item, job)) {
			item->jobs = g_list_remove (item->jobs, job);
			print_job_free (job);
			ret_val = gtk_list_store_remove (search->store, &iter);
			continue;
		}

		ret_val = gtk_tree_model_iter_next (GTK_TREE_MODEL (search->store), &iter);
	}
	return;

}

static void
queue_changed (PrintManagerQueue *queue, SearchQueueItem *item)
{
	/* If we have completed the search, remove non-existant jobs */
	if (item->is_started)
		remove_nonexistant_jobs (item);
}

#define QUEUE_TIMEOUT_MS 10

static gboolean
queue_search_timeout (gpointer user_data)
{
	GSList *list = (GSList *)user_data;
	SearchQueueItem *item;
	Search *search;
	GList *iterator;

	while (list) {
		item = list->data;
		search = item->search;

		if (!item->is_started && !item->was_unfinished)	{
			do_queue_search (item);	
			build_store_from_item (item);

			if (list->next != NULL) {
				search->queue_search_timeout_id =
					g_timeout_add (QUEUE_TIMEOUT_MS,
						       (GSourceFunc) queue_search_timeout,
						       list->next);
				return FALSE;
			}
		}	

		list = list->next;
	}

	search->queue_search_timeout_id = 0;

	/* Check if we havent searched a queue yet */
	search_complete_or_load_next (search);
	return FALSE;
}

static void
do_search (Search *search)
{
	int i, device_count;
	const gchar *document_name;
	char *frame_string;

	if (search->queue_search_timeout_id)
		g_source_remove (search->queue_search_timeout_id);
	search->queue_search_timeout_id = 0;

	gtk_list_store_clear (search->store);

	search->only_user_owned = 
		gtk_toggle_button_get_active (
			GTK_TOGGLE_BUTTON (search->only_user_owned_widget));

	search->ignore_case = 
		gtk_toggle_button_get_active (
			GTK_TOGGLE_BUTTON (search->ignore_case_widget));

	search->exact_match = 
		gtk_toggle_button_get_active (
			GTK_TOGGLE_BUTTON (search->exact_match_widget));

	g_free (search->document_name);
	document_name = 
		gtk_entry_get_text (GTK_ENTRY (search->document_name_widget));

	if (search->ignore_case)
		search->document_name = g_utf8_casefold (document_name, -1);
	else
		search->document_name = g_strdup (document_name);

	g_list_foreach (search->items, free_search_item, search);
	g_list_free (search->items);
	search->items = NULL;

	device_count = print_manager_backend_get_count (search->backend);

	search->unfinished_count = 0;

	for (i = 0; i < device_count; i++) {
		PrintManagerDevice *device =
			print_manager_backend_get_device (search->backend, i);
		PrintManagerQueue *queue =
			print_manager_device_get_queue (device);

		SearchQueueItem *item = g_new (SearchQueueItem, 1);

		item->search = search;

		item->device = device;
		g_object_ref (device);
		item->queue = queue;
		g_object_ref (queue);

		item->queue_changed_id =
			g_signal_connect (queue, "changed",
					  G_CALLBACK (queue_changed), item);

		item->jobs = NULL;
		item->was_unfinished = 
			!print_manager_queue_get_is_loaded (queue) ||
			!print_manager_device_get_is_visible (device);
		if (item->was_unfinished) {
			search->unfinished_count ++;
			item->queue_changes_done_id =
				g_signal_connect (
					queue, "changes_done",
					G_CALLBACK (queue_changes_done),
					item);
		} else
			item->queue_changes_done_id = 0;

		item->is_started = FALSE;
		item->done_reload = FALSE;

		search->items = g_list_prepend (search->items, item);
	}
	search->items = g_list_reverse (search->items);

	frame_string = g_strdup_printf (_("Search Results for: %s"), 
					document_name);
	gtk_frame_set_label (GTK_FRAME (search->frame),
			     frame_string);
	g_free (frame_string);

	/* We no longer start the search at an idle time */
	if (device_count)
		queue_search_timeout (search->items);
}

static void
stop_search_cb (GtkWidget *widget, Search *search)
{
	const char *text;

	g_list_foreach (search->items, stop_signals_item, search);
	if (search->queue_search_timeout_id)
		g_source_remove (search->queue_search_timeout_id);
	search->queue_search_timeout_id = 0;
	gtk_widget_set_sensitive (search->stop,
				  FALSE);
	text = gtk_entry_get_text (GTK_ENTRY (search->document_name_widget));
	gtk_widget_set_sensitive (search->start,
				  (g_utf8_strlen (text, -1) > 0));
}

static void
search_cb (GtkWidget *widget, Search *search)
{
	if (GTK_WIDGET_SENSITIVE (search->start)) {
		do_search (search);
		gtk_widget_set_sensitive (search->stop,
					  TRUE);
		gtk_widget_set_sensitive (search->start,
					  FALSE);
	}
}

static void
changed_cb (GtkWidget *widget, Search *search)
{
	const char *text;
	
	text = gtk_entry_get_text (GTK_ENTRY (widget));
	gtk_widget_set_sensitive (search->start, 
				  (g_utf8_strlen (text, -1) > 0));
}

static void
goto_cb (GtkWidget *widget, Search *search)
{
	GtkTreePath *path;
	GtkTreeViewColumn *focus_column;

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (search->tree_view),
				  &path,
				  &focus_column);

	if (path) {
		GtkTreeIter iter;
		PrintManagerDevice *device;
		PrintJob *job;
		gtk_tree_model_get_iter (GTK_TREE_MODEL
					 (search->store),
					 &iter,
					 path);
		gtk_tree_model_get (GTK_TREE_MODEL (search->store),
				    &iter,
				    COL_DEVICE, &device,
				    COL_JOB, &job,
				    -1);
		print_manager_show_job_list (search->backend, device,
					     FALSE,
					     job);
	}
}

static void
job_cancel_output_fn (PrintManagerQueue *queue,
		      PrintJob          *job,
		      gboolean           success,
		      gchar             *errstr,
		      gpointer           user_data)
{
	Search *search = user_data;
	GtkWidget *dialog;

	if (!errstr || !strlen (errstr))
		return;

	/* 
	 * We only have the job here, and not the device, so don't show the
	 * printer name. 
	 */
	dialog = 
		gtk_message_dialog_new (
			GTK_WINDOW (search->dialog), 
			GTK_DIALOG_MODAL, 
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_OK,
			_("Error cancelling \"%s\".\n"
			  "Reason given by server: %s.\n"),
			job->doc_name,
			errstr);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

typedef struct {
	PrintManagerDevice *device;
	PrintJob           *job;
} CancelJobData;

static void
cancel_jobs_list_foreach_cb (GtkTreeModel *model,
			     GtkTreePath  *path,
			     GtkTreeIter  *iter,
			     gpointer      user_data)
{
	GSList **job_list = user_data;
	CancelJobData *cancel_data = g_new0 (CancelJobData, 1);
	
	gtk_tree_model_get (model, 
			    iter, 
			    COL_DEVICE, &cancel_data->device, 
			    COL_JOB, &cancel_data->job, 
			    -1);

	*job_list = g_slist_append (*job_list, cancel_data);	
}

static void
cancel_cb (GtkWidget *widget, Search *search)
{
	GtkTreeSelection *selection;
	GSList *selected = NULL, *iter;
	gint length, resp;
	GtkWidget *dialog;

	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (search->tree_view));
	
	gtk_tree_selection_selected_foreach (selection, 
					     cancel_jobs_list_foreach_cb,
					     &selected);
	length = g_slist_length (selected);

	if (length == 1) {
		CancelJobData *cancel_data = selected->data;
		const gchar *label;

		label = print_manager_device_get_label (cancel_data->device);
		dialog = 
			gtk_message_dialog_new (
				GTK_WINDOW (search->dialog), 
				GTK_DIALOG_MODAL, 
				GTK_MESSAGE_QUESTION,
				GTK_BUTTONS_YES_NO,
				_("Cancel printing of \"%s\" "
				  "on printer \"%s\"?"),
				cancel_data->job->doc_name,
				label);
	} else
		dialog = 
			gtk_message_dialog_new (
				GTK_WINDOW (search->dialog), 
				GTK_DIALOG_MODAL, 
				GTK_MESSAGE_QUESTION,
				GTK_BUTTONS_YES_NO,
				_("Cancel printing of %d documents?"),
				length);

	resp = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	if (resp != GTK_RESPONSE_YES) {
		g_slist_foreach (selected, (GFunc) g_free, NULL);
		g_slist_free (selected);
		return;
	}

	for (iter = selected; iter; iter = iter->next) {
		CancelJobData *cancel_data = iter->data;
		PrintManagerQueue *queue;

		queue = print_manager_device_get_queue (cancel_data->device);
		print_manager_queue_cancel_job (queue,
						cancel_data->job,
						job_cancel_output_fn,
						search);
	}

	g_slist_foreach (selected, (GFunc) g_free, NULL);
	g_slist_free (selected);
}

static void 
count_selection_foreach_cb (GtkTreeModel *model,
			    GtkTreePath *path,
			    GtkTreeIter *iter,
			    gpointer data)
{
	gint *cnt = data;
	(*cnt)++;
}

static gint
count_selected_jobs (Search *search)
{
	GtkTreeSelection *selection;
	gint ret = 0;

	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (search->tree_view));
	
	gtk_tree_selection_selected_foreach (selection, 
					     count_selection_foreach_cb,
					     &ret);
	
	return ret;
}

static void
tree_selection_changed_cb (GtkTreeSelection *selection,
			   Search           *search)
{
	gint count = count_selected_jobs (search);

	gtk_widget_set_sensitive (search->goto_document, count == 1);
	gtk_widget_set_sensitive (search->cancel_document, count > 0);
}


static GtkWidget *
create_search (GtkWindow * parent, Search *search)
{
	GtkWidget *dialog;
	GtkWidget *vbox2;
	GtkWidget *frame3;
	GtkWidget *vbox3;
	GtkWidget *hbox2;
	GtkWidget *label15;
	GtkWidget *entry3;
	GtkWidget *hbox3;
	GtkWidget *checkbutton;
	GtkWidget *button;
	GtkWidget *hbox4;
	GtkWidget *vbox4;
	GtkWidget *hbox5;
	GtkWidget *scrolledwin;
	GtkTreeViewColumn *col;
	GtkCellRenderer *rend;
	GtkTreeSelection *selection;

	dialog = gtk_dialog_new_with_buttons (_("Search for Document "
						"Being Printed"),
					      parent, 
					      0, 
					      GTK_STOCK_CLOSE,
					      GTK_RESPONSE_CLOSE, 
					      GTK_STOCK_HELP, 
					      GTK_RESPONSE_HELP, 
					      NULL);
	search->dialog = dialog;
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (find_job_response_cb), 
			  search);

	g_signal_connect (dialog,
			  "delete_event",
			  G_CALLBACK (find_job_delete_event_cb), 
			  search);

	vbox2 = gtk_vbox_new (FALSE, 8);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), 
			    vbox2, 
			    TRUE,
			    TRUE, 
			    0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox2), 6);

	frame3 = gtk_frame_new (_("Search Options:"));
	gtk_widget_show (frame3);
	gtk_box_pack_start (GTK_BOX (vbox2), frame3, FALSE, FALSE, 0);

	vbox3 = gtk_vbox_new (FALSE, 4);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame3), vbox3);
	gtk_container_set_border_width (GTK_CONTAINER (vbox3), 4);

	hbox2 = gtk_hbox_new (FALSE, 8);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox2, FALSE, FALSE, 0);

	label15 = gtk_label_new_with_mnemonic (_("_Document Name:"));
	gtk_widget_show (label15);
	gtk_box_pack_start (GTK_BOX (hbox2), label15, FALSE, FALSE, 0);

	entry3 = gtk_entry_new ();
	gtk_widget_show (entry3);
	gtk_box_pack_start (GTK_BOX (hbox2), entry3, TRUE, TRUE, 0);
	search->document_name_widget = entry3;
	g_signal_connect (entry3,
			  "activate",
			  G_CALLBACK (search_cb), 
			  search);
	g_signal_connect (G_OBJECT (entry3),
			  "changed",
			  G_CALLBACK (changed_cb),
			  search);

	hbox3 = gtk_hbox_new (FALSE, 8);
	gtk_widget_show (hbox3);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox3, FALSE, FALSE, 0);

	checkbutton = 
		gtk_check_button_new_with_mnemonic (_("_Only My Documents"));
	gtk_toggle_button_set_active (
		GTK_TOGGLE_BUTTON (checkbutton),
		! print_manager_backend_get_show_all_jobs (search->backend));
	gtk_widget_show (checkbutton);
	gtk_box_pack_end (GTK_BOX (hbox3), checkbutton, FALSE, FALSE, 0);
	search->only_user_owned_widget = checkbutton;

	checkbutton = gtk_check_button_new_with_mnemonic (_("Igno_re Case"));
	gtk_widget_show (checkbutton);
	gtk_box_pack_end (GTK_BOX (hbox3), checkbutton, FALSE, FALSE, 0);
	search->ignore_case_widget = checkbutton;

	checkbutton = gtk_check_button_new_with_mnemonic (_("_Exact Match"));
	gtk_widget_show (checkbutton);
	gtk_box_pack_end (GTK_BOX (hbox3), checkbutton, FALSE, FALSE, 0);
	search->exact_match_widget = checkbutton;

	hbox4 = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (hbox4);
	gtk_box_pack_start (GTK_BOX (vbox3), hbox4, FALSE, FALSE, 0);

	button = gtk_button_new_with_mnemonic (_("S_top Search"));
	gtk_widget_show (button);
	gtk_box_pack_end (GTK_BOX (hbox4), button, TRUE, TRUE, 10);
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (stop_search_cb), 
			  search);
	gtk_widget_set_sensitive (button, FALSE);
	search->stop = button;

	button = gtk_button_new_with_mnemonic (_("_Start Search"));
	gtk_widget_show (button);
	gtk_box_pack_end (GTK_BOX (hbox4), button, TRUE, TRUE, 10);
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (search_cb), 
			  search);
	gtk_widget_set_sensitive (button, FALSE);
	search->start = button;
	
	search->frame = gtk_frame_new (_("Search Results:"));
	gtk_widget_show (search->frame);
	gtk_box_pack_start (GTK_BOX (vbox2), search->frame, TRUE, TRUE, 0);

	vbox4 = gtk_vbox_new (FALSE, 4);
	gtk_widget_show (vbox4);
	gtk_container_add (GTK_CONTAINER (search->frame), vbox4);
	gtk_container_set_border_width (GTK_CONTAINER (vbox4), 4);

	/* List view - Job Name, Owner, Number, Size, Submitted Date/Time. */
	scrolledwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwin);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwin),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwin),
					     GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (vbox4), scrolledwin, TRUE, TRUE, 0);

	search->store = gtk_list_store_new (LAST_COL,
					    G_TYPE_STRING,
					    G_TYPE_STRING,
					    G_TYPE_STRING,
					    G_TYPE_STRING,
					    G_TYPE_STRING,
					    G_TYPE_POINTER,
					    G_TYPE_POINTER);

	search->tree_view = 
		gtk_tree_view_new_with_model (GTK_TREE_MODEL (search->store));
	g_object_unref (G_OBJECT (search->store));

	selection = 
		gtk_tree_view_get_selection (GTK_TREE_VIEW (search->tree_view));
	gtk_tree_selection_set_mode (GTK_TREE_SELECTION (selection),
				     GTK_SELECTION_MULTIPLE);
	g_signal_connect (selection,
			  "changed",
			  G_CALLBACK (tree_selection_changed_cb), 
			  search);

	_add_atk_name_desc (GTK_WIDGET (search->tree_view),
			    _("Search Result List"),
			    _("Lists print jobs matching the search criteria"));

	_add_atk_relation (search->start,
			   GTK_WIDGET (search->tree_view),
			   ATK_RELATION_CONTROLLER_FOR,
			   ATK_RELATION_CONTROLLED_BY);

	gtk_widget_show (search->tree_view);
	gtk_container_add (GTK_CONTAINER (scrolledwin), search->tree_view);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Document"), 
							rend,
							"text",
							COL_DOC_NAME,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (search->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Owner"), 
							rend,
							"text", 
							COL_OWNER,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (search->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Printer"), 
							rend,
							"text", 
							COL_DEVICE_LABEL,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (search->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Position"), 
							rend,
							"text", 
							COL_JOB_NUM,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (search->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Time Submitted"),
							rend,
							"text",
							COL_SUBMIT,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (search->tree_view), col);


	hbox5 = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (hbox5);
	gtk_box_pack_start (GTK_BOX (vbox4), hbox5, FALSE, FALSE, 0);

	button = gtk_button_new_with_mnemonic (_("_Goto Document"));
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox5), button, TRUE, TRUE, 10);
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (goto_cb), 
			  search);
	gtk_widget_set_sensitive (button, FALSE);
	search->goto_document = button;

	_add_atk_description (button, 
			      _("Open the queue window for the selected "
				"document"));

	button = gtk_button_new_with_mnemonic (_("Ca_ncel Documents"));
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox5), button, TRUE, TRUE, 10);
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (cancel_cb), 
			  search);
	gtk_widget_set_sensitive (button, FALSE);
	search->cancel_document = button;

	gtk_label_set_mnemonic_widget (GTK_LABEL (label15), entry3);

	gtk_window_set_default_size (GTK_WINDOW (dialog), 500, 400);

	return dialog;
}


void
print_manager_show_search (PrintManagerBackend *backend,
			   GtkWindow *parent_window)
{
	static Search *search = NULL;

	if (!search) {
		search                         = g_new0 (Search, 1);
		search->backend                = backend;
		g_object_ref (backend);

		search->document_name          = NULL;
		search->only_user_owned        = FALSE;
		search->ignore_case            = FALSE;
		search->exact_match            = FALSE;

		create_search (parent_window, search);
	}

	gtk_widget_show (search->dialog);
	if (search->dialog->window)
		gdk_window_raise (search->dialog->window);
}

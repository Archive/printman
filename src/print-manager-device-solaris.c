/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * FIXME:
 *  - Handle failure result from print_file
 *  - Bootstrap from CDE only once?
 */

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include "print-manager-device-solaris.h"
#include "print-manager-queue-solaris.h"
#include "print-prefs.h"
#include "print-manager-commands.h"

enum {
	CHANGED,
	LAST_SIGNAL
};

#define PARENT_TYPE (PRINT_MANAGER_TYPE_DEVICE)

struct _PrintManagerDeviceSolarisPrivate {
	gchar             *label;
	gchar             *desc;
	gchar             *icon_path;

	gchar             *queue_name;

	PrintManagerQueue *queue;

	guint              is_visible : 1;
	guint              is_default : 1;
	gboolean           default_visible : 1;
	gboolean           visibility_set;
};

/* Static functions */

static gchar *
pmds_get_gconf_key_name (PrintManagerDeviceSolaris *pmds,
			 gchar *lastkey)
{
	return g_strjoin ("/",
			  "printers",
			  pmds->priv->queue_name,
			  lastkey,
			  NULL);
}

static gpointer
pmds_get_gconf (PrintManagerDeviceSolaris *pmds,
		char                      *key,
		gboolean                   admin,
		gboolean                   def,
		gboolean                  *set_ret)
{
	char *fullkey;
	gpointer ret_val;

	fullkey = pmds_get_gconf_key_name (pmds, key);
	ret_val = print_manager_prefs_get_gconf (fullkey, admin, def, set_ret);
	g_free (fullkey);

	return ret_val;
}

static gboolean
pmds_set_gconf (PrintManagerDeviceSolaris *pmds,
		char                      *key,
		PrintPrefType              type,
		gconstpointer              value)
{
	char *fullkey;
	gboolean ret_val;
	gboolean admin = print_manager_prefs_is_gconf_admin_mode ();

	fullkey = pmds_get_gconf_key_name (pmds, key);
	ret_val = print_manager_prefs_set_gconf (fullkey, admin, type, value);
	g_free (fullkey);

	return ret_val;
}

static gboolean
reload_settings_from_gconf (PrintManagerDeviceSolaris *pmds, gboolean admin, gboolean def)
{
	gboolean set = FALSE;
	gchar *label;
	guint is_visible = FALSE;

	/* Making a printer visible no longer sets the label too */
	if (!pmds->priv->visibility_set) {
		is_visible = GPOINTER_TO_INT (pmds_get_gconf (pmds, "visible",
					      admin,
					      def,
					      &set));

		if (set) {
			pmds->priv->is_visible = is_visible;
			pmds->priv->visibility_set = TRUE;
		}
		set = FALSE;
	}

	/* Setting the label however also sets the description and icon_path */
	label = pmds_get_gconf (pmds, "label", admin, def, &set);

	if (set) {
		g_free (pmds->priv->label);
		g_free (pmds->priv->desc);
		g_free (pmds->priv->icon_path);

		pmds->priv->label = label;
		pmds->priv->desc = pmds_get_gconf (pmds,
						   "description",
						   admin,
						   def,
						   NULL);
		pmds->priv->icon_path = pmds_get_gconf (pmds,
							"icon_path",
							admin,
							def,
							NULL);
	}
	return set;
}

static gboolean
reload_settings_from_cde (PrintManagerDeviceSolaris *pmds, gboolean admin)
{
	gboolean set;
	gchar *action;
	gchar *label;
	const gchar *queuename = 
		print_manager_device_get_queue_name (
			PRINT_MANAGER_DEVICE (pmds));

	action = g_strconcat (queuename, "_Print", NULL);

	label = print_manager_prefs_get_cde_dt (queuename,
						admin,
						action,
						"LABEL",
						&set);

	if (set) {
		pmds->priv->label = label;
		pmds->priv->desc = 
			print_manager_prefs_get_cde_dt (queuename,
							admin,
							action,
							"DESCRIPTION",
							&set);
		if (!pmds->priv->visibility_set) {
			pmds->priv->is_visible = pmds->priv->default_visible;
			pmds->priv->visibility_set = TRUE;
		}
	}

	g_free (action);

	return set;
}

/* access functions */
static gboolean
pmds_get_is_visible (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->is_visible;
}

static void
pmds_set_is_visible (PrintManagerDevice *pmd, gboolean is_visible)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	pmds->priv->is_visible = is_visible;

	pmds_set_gconf (pmds, "visible",
			PRINT_PREF_TYPE_BOOLEAN,
			GINT_TO_POINTER (pmds->priv->is_visible));

	print_manager_device_changed (pmd);
}

static gboolean
pmds_get_is_default (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->is_default;
}

static const char *
pmds_get_label (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->label;
}

static void
pmds_set_label (PrintManagerDevice *pmd, const char *label)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	g_free (pmds->priv->label);
	pmds->priv->label = g_strdup (label);

	pmds_set_gconf (pmds, "label",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->label);

	print_manager_device_changed (pmd);
}

static const char *
pmds_get_desc (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->desc;
}

static void
pmds_set_desc (PrintManagerDevice *pmd, const char *desc)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	g_free (pmds->priv->desc);
	pmds->priv->desc = g_strdup (desc);

	pmds_set_gconf (pmds, "description",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->desc);

	print_manager_device_changed (pmd);
}

static const char *
pmds_get_queue_name (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->queue_name;
}

static void
pmds_set_queue_name (PrintManagerDevice *pmd, const char *queue_name)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	g_free (pmds->priv->queue_name);
	pmds->priv->queue_name = g_strdup (queue_name);

	print_manager_device_changed (pmd);
}


static const char *
pmds_get_icon_path (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	return pmds->priv->icon_path;
}

static void
pmds_set_icon_path (PrintManagerDevice *pmd, const char *icon_path)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);


	if (pmds->priv->icon_path != icon_path) {
		g_free (pmds->priv->icon_path);
		pmds->priv->icon_path = g_strdup (icon_path);
	}

	pmds_set_gconf (pmds, "icon_path",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->icon_path);

	print_manager_device_changed (pmd);
}

/* Change functions */
static void
pmds_reload (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	if (pmds->priv->queue)
		print_manager_queue_reload (pmds->priv->queue);

	/* Setting the visibility no longer sets the label */
	pmds->priv->visibility_set = FALSE;

	/* Only load user settings if not in admin mode */
	if (print_manager_prefs_is_gconf_admin_mode ()) {
		if (reload_settings_from_gconf (pmds, TRUE, TRUE))
			return;
		if (reload_settings_from_cde (pmds, TRUE))
			return;
		reload_settings_from_gconf (pmds, FALSE, TRUE);
	} else {
		if (reload_settings_from_gconf (pmds, FALSE, FALSE))
			return;
		if (reload_settings_from_cde (pmds, FALSE))
			return;
		if (reload_settings_from_gconf (pmds, TRUE, TRUE))
			return;
		if (reload_settings_from_cde (pmds, TRUE))
			return;
		reload_settings_from_gconf (pmds, FALSE, TRUE);
	}
}

static void
pmds_save (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	pmds_set_gconf (pmds, "visible",
			PRINT_PREF_TYPE_BOOLEAN,
			GINT_TO_POINTER (pmds->priv->is_visible));
	pmds_set_gconf (pmds, "label",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->label);
	pmds_set_gconf (pmds, "description",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->desc);
	pmds_set_gconf (pmds, "icon_path",
			PRINT_PREF_TYPE_STRING,
			pmds->priv->icon_path);
}

static PrintManagerQueue *
pmds_get_queue (PrintManagerDevice *pmd)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);

	g_return_val_if_fail (pmds != NULL, NULL);

	if (pmds->priv->queue == NULL)
		pmds->priv->queue =
			print_manager_queue_solaris_new (
				pmds->priv->queue_name);

	return pmds->priv->queue;
}

typedef struct {
	PrintManagerDevice *device;
	gchar              *filename;
	PrintFileDoneFn     done_cb;
	gpointer            user_data;
} PrintFileData;

static void
print_file_end (gint     lpstat_id,
		gboolean cancelled,
		gpointer user_data)
{
	PrintFileData *data = user_data;
	PrintManagerDeviceSolaris *pmds;

	pmds = PRINT_MANAGER_DEVICE_SOLARIS (data->device);
	if (pmds->priv->queue)
		print_manager_queue_reload (pmds->priv->queue);

	(*data->done_cb) (data->device, data->filename, data->user_data);
	
	g_object_unref (data->device);
	g_free (data->filename);
	g_free (data);
}

static void
pmds_print_file (PrintManagerDevice *pmd,
		 const char         *filename,
		 PrintFileDoneFn     done_cb,
		 gpointer            user_data)
{
	PrintManagerDeviceSolaris *pmds = PRINT_MANAGER_DEVICE_SOLARIS (pmd);
	gchar *command_args [] = { pmds->priv->queue_name, 
				   (char *) filename, 
				   NULL };
	PrintFileData *data = NULL;

	if (done_cb) {
		data = g_new0 (PrintFileData, 1);

		data->filename  = g_strdup (filename);
		data->done_cb   = done_cb;
		data->user_data = user_data;

		data->device = pmd;
		g_object_ref (data->device);
	}

	lpstat_run (NULL,
		    NULL,
		    data ? print_file_end : NULL,
		    data,
		    LPSTAT_PRINT_FILE,
		    command_args);
}

static void
pmds_class_init (PrintManagerDeviceSolarisClass *klass)
{
	PrintManagerDeviceClass *device_class;

	device_class = PRINT_MANAGER_DEVICE_CLASS (klass);

	device_class->get_is_visible = pmds_get_is_visible;
	device_class->set_is_visible = pmds_set_is_visible;

	device_class->get_is_default = pmds_get_is_default;

	device_class->get_label      = pmds_get_label;
	device_class->set_label      = pmds_set_label;

	device_class->get_desc       = pmds_get_desc;
	device_class->set_desc       = pmds_set_desc;

	device_class->get_queue_name = pmds_get_queue_name;
	device_class->set_queue_name = pmds_set_queue_name;

	device_class->get_icon_path  = pmds_get_icon_path;
	device_class->set_icon_path  = pmds_set_icon_path;

	device_class->get_queue      = pmds_get_queue;

	device_class->print_file     = pmds_print_file;

	device_class->reload         = pmds_reload;
	device_class->save           = pmds_save;
}

static void
pmds_init (PrintManagerDeviceSolaris *pmds)
{
	pmds->priv = g_new0 (PrintManagerDeviceSolarisPrivate, 1);
	pmds->priv->icon_path = g_strdup (PRINT_MANAGER_ICONDIR "/inkjet.png");
}

/**
 * print_manager_device_solaris_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerDeviceSolaris class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerDeviceSolaris class.
 **/
GType
print_manager_device_solaris_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerDeviceSolarisClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pmds_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerDeviceSolaris),
			0,
			(GInstanceInitFunc) pmds_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerDeviceSolaris",
					       &info, 0);
	}

	return type;
}

PrintManagerDevice *
print_manager_device_solaris_new (char *name, gboolean default_visible)
{
	PrintManagerDeviceSolaris *pmds = 
		PRINT_MANAGER_DEVICE_SOLARIS (
			g_type_create_instance (
				PRINT_MANAGER_TYPE_DEVICE_SOLARIS));

	g_free (pmds->priv->queue_name);
	g_free (pmds->priv->label);
	g_free (pmds->priv->desc);

	pmds->priv->queue_name = g_strdup (name);
	pmds->priv->label      = g_strdup (name);
	pmds->priv->desc       = g_strdup (name);

	pmds->priv->default_visible = default_visible;

	print_manager_device_reload (PRINT_MANAGER_DEVICE (pmds));

	return PRINT_MANAGER_DEVICE (pmds);
}



void
print_manager_device_solaris_set_default_visible (PrintManagerDeviceSolaris *pmds,
						  gboolean default_visible)
{
	if (pmds->priv->default_visible != default_visible) {
		pmds->priv->default_visible = default_visible;
		print_manager_device_reload (PRINT_MANAGER_DEVICE (pmds));
	}
}

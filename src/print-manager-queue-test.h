/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_QUEUE_TEST_H
#define PRINT_MANAGER_QUEUE_TEST_H 1

#include <glib-object.h>
#include "print-manager-queue.h"

#define PRINT_MANAGER_TYPE_QUEUE_TEST            (print_manager_queue_test_get_type ())
#define PRINT_MANAGER_QUEUE_TEST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PRINT_MANAGER_TYPE_QUEUE_TEST, PrintManagerQueueTest))
#define PRINT_MANAGER_QUEUE_TEST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), PRINT_MANAGER_TYPE_QUEUE_TEST, PrintManagerQueueTestClass))
#define PRINT_MANAGER_IS_QUEUE_TEST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PRINT_MANAGER_TYPE_QUEUE_TEST))
#define PRINT_MANAGER_IS_QUEUE_TEST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PRINT_MANAGER_TYPE_QUEUE_TEST))

typedef struct _PrintManagerQueueTest PrintManagerQueueTest;
typedef struct _PrintManagerQueueTestClass PrintManagerQueueTestClass;
typedef struct _PrintManagerQueueTestPrivate PrintManagerQueueTestPrivate;

struct _PrintManagerQueueTest {
	PrintManagerQueue object;

	PrintManagerQueueTestPrivate *priv;
};

struct _PrintManagerQueueTestClass {
	PrintManagerQueueClass parent_class;
};

/* Standard gobject functions */
GType              print_manager_queue_test_get_type    (void);
PrintManagerQueue *print_manager_queue_test_new         (const char            *directory,
							 const char            *queue_name);
void               print_manager_queue_test_print_file  (PrintManagerQueueTest *pmqt,
							 const char            *filename);

#endif /* ! __PRINT_MANAGER_QUEUE_TEST_H__ */

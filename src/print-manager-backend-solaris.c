/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * FIXME:
 *  - Load queues from ~/.printers aliases
 *  - ~/.printers.pid files left around
 *  - Clean changes/changes_done split for query_end, default_printer
 *  - write_dot_printers should only overwrite _all/_default lines
 *  - simplify query_end
 */

/*
 * INDICATIONS
 * ===========
 * 
 * ON CREATE:
 *   - create backend
 *   - move ~/.printers to ~/.printers.pid
 *   - call `lpstat -v` to list all printers
 *     - foreach printer
 *       - if lp queuename is unknown
 *         - create printer
 *         - register for device_changed signal
 *       - else
 *         - reload printer prefs
 *       - add to temporary queue
 *       - if admin-mode 
 *         - make printer visible
 *       - else
 *         - make printer invisible
 *     - foreach printer not in temporary queue
 *       - delete printer
 *   - if user-mode
 *     - move ~/.printers.pid back to ~/.printers
 *     - call `lpstat -v` to list user visible printers
 *       - foreach printer
 *         - set visibility to TRUE
 *     - call `lpstat -d` to get user's default printer
 *     - aware gui of changes_done
 *   - else
 *     - call `lpstat -d` to get system default printer
 *     - move ~/.printers.pid back to ~/.printers
 *     - aware gui of changes_done
 *   - load app-global settings from gconf/CDE/admin gconf/admin CDE (why here?)
 *   - add timeout handler for refresh interval
 *   - sleep until refresh
 *     - goto 2
 * 
 * ON DEVICE CHANGED:
 *   - create idle handler (why?)
 *   - in idle handler
 *     - write visible printers and default printer to ~/.printers
 *     - aware gui of changes_done 
 */

#include <config.h>

#include "print-manager-backend-solaris.h"

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <gnome.h>
#include <string.h>
#include <signal.h>

#include "print-manager-device-solaris.h"
#include "print-manager-commands.h"
#include "print-prefs.h"

#define PARENT_TYPE (PRINT_MANAGER_TYPE_BACKEND)

/* Static functions */

typedef struct {
	PrintManagerDevice *device;
	guint signal_id;
} PrintManagerDeviceItem;

struct _PrintManagerBackendSolarisPrivate {
	GPtrArray *devices; /* of PrintManagerDeviceItem * */

	guint in_reload : 1;
	guint queue_reload : 1;

	guint show_all_jobs : 1;
	guint show_printer_details : 1;
	guint show_status_bar : 1;

	int update_interval;
	int default_printer;

	guint idle_id;

	guint      worker_cnt;
	GSList    *found_printers;
	GPtrArray *old_devices;
	gboolean   dot_printers_moved;
};

#define GCONF_KEY_SHOW_ALL_JOBS        "show_all_jobs"
#define GCONF_KEY_SHOW_PRINTER_DETAILS "show_printer_details"
#define GCONF_KEY_SHOW_STATUS_BAR      "show_status_bar"
#define GCONF_KEY_UPDATE_INTERVAL      "update_interval"
#define GCONF_KEY_DEFAULT_PRINTER_NAME "default_printer_name"

static gboolean
reload_settings_from_gconf (PrintManagerBackendSolaris *backend, 
			    gboolean                    admin, 
			    gboolean                    def)
{
	gboolean set;
	gboolean show_all_jobs = 
		GPOINTER_TO_INT (
			print_manager_prefs_get_gconf (GCONF_KEY_SHOW_ALL_JOBS,
						       admin,
						       def,
						       &set));
	if (set) {
		backend->priv->show_all_jobs = show_all_jobs;
		backend->priv->show_printer_details = 
			GPOINTER_TO_INT (
				print_manager_prefs_get_gconf (
					GCONF_KEY_SHOW_PRINTER_DETAILS,
					admin,
					def,
					NULL));
		backend->priv->show_status_bar = 
			GPOINTER_TO_INT (
				print_manager_prefs_get_gconf (
					GCONF_KEY_SHOW_STATUS_BAR, 
					admin,
					def,
					NULL));
		backend->priv->update_interval = 
			GPOINTER_TO_INT (
				print_manager_prefs_get_gconf (
					GCONF_KEY_UPDATE_INTERVAL, 
					admin,
					def,
					NULL));
	}
	return set;
}

#define CDE_KEY_SHOW_MESSAGE_LINE "ShowMessageLine"
#define CDE_KEY_SHOW_ONLY_MINE    "ShowOnlyMine"
#define CDE_KEY_REPRESENTATION    "Representation"
#define CDE_KEY_UPDATE_INTERVAL   "UpdateInterval"

static gboolean
get_bool_from_string (gchar *string)
{
	gboolean ret = FALSE;

	if (string && !strcmp (string, "1"))
		ret = TRUE;

	g_free (string);
	return ret;
}

static gboolean
reload_settings_from_cde (PrintManagerBackendSolaris *backend)
{
	gchar *preffile;
	gchar *representation;
	gchar *interval;

	preffile = gnome_util_prepend_user_home (".dt/.Printers/Printmgr");
	if (!g_file_test (preffile, G_FILE_TEST_EXISTS)) {
		g_free (preffile);
		return FALSE;
	}

	backend->priv->show_all_jobs = 
		! get_bool_from_string (
			  print_manager_prefs_get_cde_simple (
				  preffile,
				  CDE_KEY_SHOW_ONLY_MINE, 
				  NULL));

	representation = 
		print_manager_prefs_get_cde_simple (preffile, 
						    CDE_KEY_REPRESENTATION, 
						    NULL);
	if (representation)
		backend->priv->show_printer_details = 
			g_ascii_strcasecmp (representation, "Details") == 0;

	backend->priv->show_status_bar = 
		get_bool_from_string (
			print_manager_prefs_get_cde_simple (
				preffile, 
				CDE_KEY_SHOW_MESSAGE_LINE, 
				NULL));

	interval = print_manager_prefs_get_cde_simple (preffile, 
						       CDE_KEY_UPDATE_INTERVAL, 
						       NULL);
	if (interval) 
		backend->priv->update_interval = atoi (interval);
	else 
		backend->priv->update_interval = 30;

	g_free (interval);
	g_free (representation);
	g_free (preffile);
	return TRUE;
}

static void
reload_settings (PrintManagerBackendSolaris *solaris)
{
	if (print_manager_prefs_is_gconf_admin_mode ()) {
		if (reload_settings_from_gconf (solaris, TRUE, TRUE))
			return;
	} else {
		if (reload_settings_from_gconf (solaris, FALSE, FALSE))
			return;
		if (reload_settings_from_cde (solaris))
			return;
		if (reload_settings_from_gconf (solaris, TRUE, TRUE))
			return;
		if (reload_settings_from_gconf (solaris, FALSE, TRUE))
			return;
	}
}

static void
save_settings (PrintManagerBackendSolaris *backend)
{
	gboolean admin = print_manager_prefs_is_gconf_admin_mode ();
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_ALL_JOBS,
		admin,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_all_jobs));
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_PRINTER_DETAILS,
		admin,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_printer_details));
	print_manager_prefs_set_gconf (
		GCONF_KEY_SHOW_STATUS_BAR,
		admin,
		PRINT_PREF_TYPE_BOOLEAN,
		GINT_TO_POINTER (backend->priv->show_status_bar));
	print_manager_prefs_set_gconf (
		GCONF_KEY_UPDATE_INTERVAL,
		admin,
		PRINT_PREF_TYPE_INT,
		GINT_TO_POINTER (backend->priv->update_interval));
	print_manager_backend_prefs_changed (PRINT_MANAGER_BACKEND (backend));
}

#if 0
static void
read_dot_printers (gchar **default_printer,
		   gchar **all_printers)
{
	GError *error = NULL;
	gchar *dot_printers_path;
	GIOChannel *dot_printers;

	dot_printers_path = gnome_util_prepend_user_home (".printers");
	dot_printers = g_io_channel_new_file (dot_printers_path, "r", &error); 
	g_free (dot_printers_path);

	if (!dot_printers) 
		return;

	if (default_printer)
		*default_printer = NULL;
	if (all_printers)
		*all_printers = NULL;

	while (1) {
		GIOStatus status;
		gchar *content, *iter;
		gint len;

		status = g_io_channel_read_line (dot_printers, 
						 &content, 
						 &len, 
						 NULL, 
						 &error);
		if (status != G_IO_STATUS_NORMAL ||  content == NULL)
			break;

		iter = content;

		iter += strspn (iter, " \t");
		if (*iter == '#') 
			goto NEXT;

		if (!strncmp (iter, "_all", strlen ("_all")) &&
		    all_printers) {
			iter += strlen ("_all");
			iter += strspn (iter, " \t");
			*all_printers = g_strndup (iter, strlen (iter - 1));
		} else if (!strncmp (iter, "_default", strlen ("_default")) &&
			   default_printer) {
			iter += strlen ("_default");
			iter += strspn (iter, " \t");
			*default_printer = g_strndup (iter, strlen (iter - 1));
		}

	NEXT:
		g_free (content);
	}
}
#endif

/*
 * .printers file utilities
 */
static gboolean
dot_printers_exists (void)
{
	gchar *dot_printers_path;
	gboolean exists;

	dot_printers_path = gnome_util_prepend_user_home (".printers");
	exists = g_file_test (dot_printers_path, G_FILE_TEST_EXISTS);
	g_free (dot_printers_path);

	return exists;
}

static void
write_dot_printers (PrintManagerBackend *backend)
{
	gint default_idx, idx, cnt;
	gboolean first_visible = TRUE;
	GString *contents = g_string_new (NULL);
	gchar *dot_printers_path; 
	gchar *dot_printers_path_temp;
	GError *error = NULL;
	GIOChannel *dot_printers;

	GString *line = g_string_new (NULL);
	GIOChannel *dot_printers_temp;
	gsize written_size;
	gint write_success = 1;
	gint line_found = 0;

	/* write "_default queuename\n" */
	default_idx = print_manager_backend_get_default_printer (backend);
	if (default_idx > -1) {
		PrintManagerDevice *def_dev;

		def_dev = print_manager_backend_get_device (backend, 
							    default_idx);
		g_string_append_printf (
			contents, 
			"_default %s\n",
			print_manager_device_get_queue_name (def_dev));
	}

	/* write "_all visiblequeuename1,visiblequeuename2" */
	g_string_append (contents, "_all ");
	cnt = print_manager_backend_get_count (backend);
	for (idx = 0; idx < cnt; idx++) {
		PrintManagerDevice *device;
		const gchar *queuename; 

		device = print_manager_backend_get_device (backend, idx);
		queuename = print_manager_device_get_queue_name (device);

		if (print_manager_device_get_is_visible (device)) {
			if (first_visible) {				
				g_string_append_printf (contents, 
							"%s",
							queuename);
				first_visible = FALSE;
			} else
				g_string_append_printf (contents, 
							",%s",
							queuename);
		}
	}
	g_string_append (contents, "\n");

	dot_printers_path = gnome_util_prepend_user_home (".printers");
	dot_printers_path_temp = g_strdup_printf ("%s.%d", 
						  dot_printers_path, 
						  (int) getpid ());

	dot_printers = g_io_channel_new_file (dot_printers_path,
					      "r",
					      &error);
	error = NULL;
	dot_printers_temp = g_io_channel_new_file (dot_printers_path_temp,
						   "w+",
						   &error);
	if (dot_printers_temp) {
		error = NULL;
		if (dot_printers) {
			while (g_io_channel_read_line_string (dot_printers,
					line, NULL, &error) != G_IO_STATUS_EOF) {
				if ((g_strrstr (line->str, "|_default ")) ||
				   (g_strrstr (line->str, "|_default\t")) ||
				   (g_strrstr (line->str, "|_default|")) ||
				   (!strncmp (line->str, "_default ", 9)) ||
				   (!strncmp (line->str, "_default\t", 9))) {
					g_io_channel_write_chars (dot_printers_temp,
								   contents->str,
								   contents->len,
								   &written_size,
								   &error);
					line_found = 1;
					if (written_size != contents->len) {
						g_warning (_("Cannot write %s : %s"),
								dot_printers_path_temp,
								strerror (errno));
						write_success = 0;
					}
				}
				else if ((!strncmp (line->str, "_all ", 5)) ||
					(!strncmp (line->str, "_all\t", 5))) {
					continue;
				}		
				else {
					g_io_channel_write_chars (dot_printers_temp,
								  line->str,
								  line->len,
								  &written_size,
								  &error);
					if (written_size != line->len)  {
						g_warning (_("Cannot write %s : %s"), 
								dot_printers_path_temp, 
								strerror (errno));
						write_success = 0;
					}
				}
			}			
			g_io_channel_unref (dot_printers);
		}
		if (!dot_printers || !line_found){
			g_io_channel_write_chars (dot_printers_temp,
						  contents->str,
						  contents->len,
						  &written_size,
						  &error);
			if (written_size != contents->len) {
				g_warning (_("Cannot write %s : %s"),
						dot_printers_path_temp,
						strerror (errno));
				write_success = 0;
			}
		}
		if (write_success) { 
			if (rename (dot_printers_path_temp, dot_printers_path)) { 
				g_warning (_("Cannot write %s : %s"),
						dot_printers_path,
						strerror (errno));
				unlink (dot_printers_path_temp);
			}
		}
		else 
			unlink (dot_printers_path_temp);

		g_io_channel_unref (dot_printers_temp);
	}
	else { 
		g_warning (_("Cannot create %s : %s"),
							dot_printers_path_temp,
							strerror (errno));
	} 

	g_free (dot_printers_path);
	g_free (dot_printers_path_temp);
	g_string_free (contents, TRUE);
	if (error)
		g_error_free (error);
	g_string_free (line, TRUE);
}

static gchar *dot_printers_path = NULL, *dot_printers_backup_path = NULL;

static gboolean
move_dot_printers_back (void)
{
	gint pid = getpid();
	gboolean exists;

	if (!dot_printers_path || !dot_printers_backup_path)
		return FALSE;

	/* 
	 * move ~/.printers.pid back to ~/.printers, to limit lpstat results to
	 * visible printers.
	 */
	exists = g_file_test (dot_printers_backup_path, G_FILE_TEST_EXISTS);
	if (exists) {
		if (rename (dot_printers_backup_path, dot_printers_path)) {
			g_warning ("Cannot move ~/.printers.%d to ~/.printers",
				   pid);
			exists = FALSE;
		}
	}

	g_free (dot_printers_path);
	dot_printers_path = NULL;

	g_free (dot_printers_backup_path);
	dot_printers_backup_path = NULL;

	return exists;
}

static void
move_dot_printers_back_sighandler (int sigid)
{
	if (move_dot_printers_back ()) 
		g_warning ("%s Handler: replacing ~/.printers from "
			   "backup copy.",
			   g_strsignal (sigid));
	exit (1);
}

static void
move_dot_printers_back_atexit (void)
{
	if (move_dot_printers_back ()) 
		g_warning ("Exit Handler: replacing ~/.printers from "
			   "backup copy.");
}

static gboolean
move_dot_printers (void)
{
	static gboolean first_time = TRUE;
	gint pid = getpid();
	gboolean exists;

	if (first_time) {
		if (!g_getenv ("GNOME_PRINT_MANAGER_DEBUG")) {
			signal (SIGHUP,  move_dot_printers_back_sighandler);
			signal (SIGQUIT, move_dot_printers_back_sighandler);
			signal (SIGSEGV, move_dot_printers_back_sighandler);
			signal (SIGPIPE, move_dot_printers_back_sighandler);
			g_atexit (move_dot_printers_back_atexit);
		}
		first_time = FALSE;
	}

	if (dot_printers_path || dot_printers_backup_path)
		return TRUE;

	dot_printers_path = gnome_util_prepend_user_home (".printers");

	/* 
	 * move ~/.printers to ~/.printers.pid, to avoid interference
	 * with lpstat.
	 */
	exists = g_file_test (dot_printers_path, G_FILE_TEST_EXISTS);
	if (exists) {
		dot_printers_backup_path = g_strdup_printf ("%s.%d",
							    dot_printers_path,
							    pid);
		if (rename (dot_printers_path, dot_printers_backup_path)) {
			g_warning ("Cannot move ~/.printers to ~/.printers.%d",
				   pid);
			exists = FALSE;
		}
	}

	if (!exists) {
		g_free (dot_printers_path);
		dot_printers_path = NULL;
			
		g_free (dot_printers_backup_path);
		dot_printers_backup_path = NULL;
	}

	return exists;
}

static gboolean
make_device_visible (PrintManagerDeviceItem  *item, gboolean visible) 
{
	g_return_val_if_fail (item != NULL, FALSE);

	if (print_manager_device_get_is_visible (item->device) != visible) {
		/* 
		 * avoid having device_changed() called, so
		 * ~/.printers is not written inadvertently.  
		 */
		g_signal_handler_block (item->device, item->signal_id);

		print_manager_device_solaris_set_default_visible (
			PRINT_MANAGER_DEVICE_SOLARIS (item->device), 
			visible);

		g_signal_handler_unblock (item->device, item->signal_id);

		return TRUE;
	}

	return FALSE;
}

static gboolean
device_changed_idle (gpointer data)
{
	PrintManagerBackendSolaris *solaris = data;
	PrintManagerBackend *backend = data;

	/* Only update ~/.printers if we aren't in admin mode */
	if (!print_manager_prefs_is_gconf_admin_mode ())
		write_dot_printers (backend);

	print_manager_backend_changed (backend);
	print_manager_backend_changes_done (backend);
	solaris->priv->idle_id = 0;
	return FALSE;
}

static void
device_changed (PrintManagerDevice *device, PrintManagerBackendSolaris *solaris)
{
	if (solaris->priv->idle_id == 0)
		solaris->priv->idle_id = g_idle_add (device_changed_idle, 
						     solaris);
}

static void
free_device_item (PrintManagerDeviceItem *item)
{
	g_signal_handler_disconnect (item->device, item->signal_id);
	g_object_unref (item->device);
	g_free (item);
}

/* access functions */
static gint
pmbs_get_count (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	g_return_val_if_fail (solaris != NULL, -1);

	return solaris->priv->devices->len;
}

static PrintManagerDevice *
pmbs_get_device (PrintManagerBackend *backend, int n)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);
	PrintManagerDeviceItem *item;

	g_return_val_if_fail (solaris != NULL, NULL);
	g_return_val_if_fail (n >= 0, NULL);
	g_return_val_if_fail (n < solaris->priv->devices->len, NULL);

	item = solaris->priv->devices->pdata[n];

	return item->device;
}

typedef struct {
	gchar    *queuename;
	gboolean  visible;
	gboolean  is_default;
} FoundPrinterData;

static FoundPrinterData *
locate_found_data (PrintManagerBackendSolaris *solaris, gchar *line)
{
	GSList *iter;
	FoundPrinterData *found_data;

	for (iter = solaris->priv->found_printers; iter; iter = iter->next) {
		found_data = iter->data;
		if (!strcmp (line, found_data->queuename))
			return found_data;
	}

	/* Do not show the _default entry #98086 */
	if (strcmp (line, "_default") == 0)
		return NULL;

	found_data = g_new0 (FoundPrinterData, 1);
	found_data->queuename = g_strdup (line);

	solaris->priv->found_printers = 
		g_slist_prepend (solaris->priv->found_printers, 
				 found_data);

	return found_data;
}

static void
add_non_visible_to_list (gint       lpstat_id,
			 gchar     *line,
			 gpointer   user_data)
{
	PrintManagerBackendSolaris *solaris = user_data;
	locate_found_data (solaris, line);
}

static void
add_visible_to_list (gint       lpstat_id,
		     gchar     *line,
		     gpointer   user_data)
{
	PrintManagerBackendSolaris *solaris = user_data;
	FoundPrinterData *found_data;

	found_data = locate_found_data (solaris, line);

	if (found_data)
		found_data->visible = TRUE;
}

static void
add_default_to_list (gint       lpstat_id,
		     gchar     *line,
		     gpointer   user_data)
{
	PrintManagerBackendSolaris *solaris = user_data;
	FoundPrinterData *found_data;

	found_data = locate_found_data (solaris, line);

	if (found_data) {
		found_data->is_default = TRUE;
		found_data->visible = TRUE;
	}
}

static void
setup_backend_reload (PrintManagerBackendSolaris *solaris)
{
	solaris->priv->old_devices = solaris->priv->devices;
	solaris->priv->devices = g_ptr_array_new ();
}

static void
cleanup_backend_reload (PrintManagerBackendSolaris *solaris)
{
	PrintManagerBackend *backend = PRINT_MANAGER_BACKEND (solaris);
	GSList *iter;
	gint i;

	if (solaris->priv->old_devices) {
		/* remove missing printers */
		for (i = 0; i < solaris->priv->old_devices->len; i++) {
			free_device_item (solaris->priv->old_devices->pdata[i]);
		}
		g_ptr_array_free (solaris->priv->old_devices, TRUE);
		solaris->priv->old_devices = NULL;
	}

	/* free found printer list */
	for (iter = solaris->priv->found_printers; iter; iter = iter->next) {
		FoundPrinterData *found_data = iter->data;
		g_free (found_data->queuename);
		g_free (found_data);
	}
	g_slist_free (solaris->priv->found_printers);
	solaris->priv->found_printers = NULL;

	/* restart if another reload was requested */
	solaris->priv->in_reload = FALSE;
	if (solaris->priv->queue_reload) {
		solaris->priv->queue_reload = FALSE;
		print_manager_backend_reload (backend);
	}
}

static PrintManagerDeviceItem *
find_matching_device_item (PrintManagerBackendSolaris *solaris, 
			   gchar                      *line, 
			   GPtrArray                  *arr,
			   gint                       *out_idx) 
{
	int idx;

	g_return_val_if_fail (line != NULL, NULL);

	if (!arr)
		return NULL;

	for (idx = 0; idx < arr->len; idx++) {
		PrintManagerDeviceItem *item = arr->pdata[idx];
		if (!item)
			continue;

		if (!strcmp (print_manager_device_get_queue_name (item->device),
			     line)) {
			if (out_idx)
				*out_idx = idx;
			return item;
		}
	}

	return NULL;
}

static PrintManagerDeviceItem *
create_new_device_item (PrintManagerBackendSolaris *solaris,
			FoundPrinterData           *found_data,
			gint                       *out_idx)
{
	PrintManagerDeviceItem *item = NULL;
	PrintManagerDevice *device;

	/* create new device if an existing one was not found */
	device = print_manager_device_solaris_new (found_data->queuename, 
						   found_data->visible);
	item = g_new (PrintManagerDeviceItem, 1);
	item->device = device;
	item->signal_id = g_signal_connect (device, "changed",
					    G_CALLBACK (device_changed),
					    PRINT_MANAGER_BACKEND (solaris));

	/* add to the list of devices to keep */
	g_ptr_array_add (solaris->priv->devices, item);

	if (out_idx)
		*out_idx = solaris->priv->devices->len - 1;
	
	return item;
}

static void
create_or_update_device (PrintManagerBackendSolaris *solaris,
			 FoundPrinterData           *found_data)
{
	PrintManagerDeviceItem *item;
	int idx = -1;

	/* find existing device, and reload it */
	item = find_matching_device_item (solaris, 
					  found_data->queuename, 
					  solaris->priv->old_devices,
					  &idx);
	if (item) {
		g_ptr_array_add (solaris->priv->devices, item);
		g_ptr_array_remove_index (solaris->priv->old_devices, idx);
	} 
	else {
		/* find previously loaded device */
		item = find_matching_device_item (solaris, 
						  found_data->queuename, 
						  solaris->priv->devices,
						  &idx);
		/* otherwise create new one */
		if (!item) 
			item = create_new_device_item (solaris, 
						       found_data, 
						       &idx);
	}

	if (item) {
		make_device_visible (item, found_data->visible);
		if (found_data->is_default)
			solaris->priv->default_printer = idx;
	}
}

static void
list_printers_end (gint       lpstat_id,
		   gboolean   cancelled,
		   gpointer   user_data)
{
	PrintManagerBackendSolaris *solaris = user_data;
	PrintManagerBackend *backend = PRINT_MANAGER_BACKEND (solaris);
	GSList *iter;
	gboolean call_changed = FALSE;

	solaris->priv->worker_cnt--;

	if (!solaris->priv->worker_cnt) {
		call_changed = TRUE;

		if (!solaris->priv->dot_printers_moved) {
			/* 
			 * list the non-visible printers (after moving
                         *  ~/.printers). 
			 */
			if (g_getenv ("GNOME_PRINT_MANAGER_DEBUG"))
				g_warning ("MOVING ~/.printers\n");

			move_dot_printers ();
			solaris->priv->dot_printers_moved = TRUE;

			lpstat_run (add_non_visible_to_list,
				    NULL,
				    list_printers_end,
				    solaris,
				    LPSTAT_LIST_PRINTERS,
				    NULL);
			solaris->priv->worker_cnt++;
		} else {
			if (g_getenv ("GNOME_PRINT_MANAGER_DEBUG"))
				g_warning ("MOVING ~/.printers BACK\n");

			move_dot_printers_back ();
			solaris->priv->dot_printers_moved = FALSE;
		}
	}

	for (iter = solaris->priv->found_printers; iter; iter = iter->next) {
		FoundPrinterData *found_data = iter->data;
		create_or_update_device (solaris, found_data);
	}

	if (call_changed)
		print_manager_backend_changed (backend);

	if (!solaris->priv->worker_cnt) {
		print_manager_backend_changes_done (backend);
		cleanup_backend_reload (solaris);
	}
}

/* Change functions */
static void
pmbs_reload (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris;

	solaris = PRINT_MANAGER_BACKEND_SOLARIS (backend);

	/* only reload once at a time */
	if (!solaris->priv->in_reload) {
		reload_settings (solaris);

		setup_backend_reload (solaris);

		/* list the visible printers */
		if (dot_printers_exists ()) {
			lpstat_run (add_visible_to_list,
				    NULL,
				    list_printers_end,
				    solaris,
				    LPSTAT_LIST_PRINTERS,
				    NULL);
			solaris->priv->worker_cnt++;
		}

		/* list the default printer */
		lpstat_run (add_default_to_list,
			    NULL,
			    list_printers_end,
			    solaris,
			    LPSTAT_LIST_DEFAULT_PRINTER,
			    NULL);
		solaris->priv->worker_cnt++;

		solaris->priv->in_reload = TRUE;
	} else {
		/* otherwise queue another reload */
		solaris->priv->queue_reload = TRUE;
	}
}

static gboolean
pmbs_get_show_all_jobs (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	return solaris->priv->show_all_jobs;
}

static void
pmbs_set_show_all_jobs (PrintManagerBackend *backend,
			gboolean             show_all_jobs)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	solaris->priv->show_all_jobs = show_all_jobs;
	save_settings (solaris);
}

static gboolean
pmbs_get_show_printer_details (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	return solaris->priv->show_printer_details;
}

static void
pmbs_set_show_printer_details  (PrintManagerBackend *backend,
				gboolean             show_printer_details)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	solaris->priv->show_printer_details = show_printer_details;
	save_settings (solaris);
}

static gboolean
pmbs_get_show_status_bar (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	return solaris->priv->show_status_bar;
}

static void
pmbs_set_show_status_bar (PrintManagerBackend *backend,
			  gboolean             show_status_bar)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	solaris->priv->show_status_bar = show_status_bar;
	save_settings (solaris);
}

static gint
pmbs_get_update_interval (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	return solaris->priv->update_interval;
}

static void
pmbs_set_update_interval (PrintManagerBackend *backend,
			  gint                 update_interval)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	solaris->priv->update_interval = update_interval;
	save_settings (solaris);
}

static gint
pmbs_get_default_printer (PrintManagerBackend *backend)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	return solaris->priv->default_printer;
}

static void
pmbs_set_default_printer (PrintManagerBackend *backend,
			  gint default_printer)
{
	PrintManagerBackendSolaris *solaris = 
		PRINT_MANAGER_BACKEND_SOLARIS (backend);

	solaris->priv->default_printer = default_printer;

	/* Only update ~/.printers if we aren't in admin mode */
	/* FIXME: Handle reload running while this is called */
	if (!print_manager_prefs_is_gconf_admin_mode ())
		write_dot_printers (backend);
}

static void
pmbs_class_init (PrintManagerBackendSolarisClass *klass)
{
	PrintManagerBackendClass *backend_class = 
		PRINT_MANAGER_BACKEND_CLASS (klass);

        backend_class->get_count = pmbs_get_count;
        backend_class->get_device = pmbs_get_device;

        backend_class->reload = pmbs_reload;

        backend_class->get_show_all_jobs = pmbs_get_show_all_jobs;
        backend_class->set_show_all_jobs = pmbs_set_show_all_jobs;

        backend_class->get_show_printer_details = pmbs_get_show_printer_details;
        backend_class->set_show_printer_details = pmbs_set_show_printer_details;

        backend_class->get_show_status_bar = pmbs_get_show_status_bar;
        backend_class->set_show_status_bar = pmbs_set_show_status_bar;

        backend_class->get_update_interval = pmbs_get_update_interval;
        backend_class->set_update_interval = pmbs_set_update_interval;

        backend_class->get_default_printer = pmbs_get_default_printer;
        backend_class->set_default_printer = pmbs_set_default_printer;
}

static void
pmbs_init (PrintManagerBackendSolaris *solaris)
{
	PrintManagerBackend *backend = PRINT_MANAGER_BACKEND (solaris);

	solaris->priv = g_new0 (PrintManagerBackendSolarisPrivate, 1);

	solaris->priv->show_all_jobs        = TRUE;
	solaris->priv->show_printer_details = FALSE;
	solaris->priv->show_status_bar      = TRUE;

	solaris->priv->update_interval      = 30;
	solaris->priv->default_printer      = -1;
	solaris->priv->dot_printers_moved   = FALSE;
 
	print_manager_backend_reload (backend);
}

/**
 * print_manager_backend_solaris_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerBackendSolaris class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerBackendSolaris class.
 **/
GType
print_manager_backend_solaris_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerBackendSolarisClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pmbs_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerBackendSolaris),
			0,
			(GInstanceInitFunc) pmbs_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerBackendSolaris",
					       &info, 0);
	}

	return type;
}

PrintManagerBackend *
print_manager_backend_solaris_new (void)
{
	return PRINT_MANAGER_BACKEND (
	               g_type_create_instance (
		                PRINT_MANAGER_TYPE_BACKEND_SOLARIS));
}

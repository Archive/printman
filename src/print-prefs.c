/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * FIXME:
 *  - Bootstrap from CDE?
 */

#include "print-prefs.h"

#include <stdlib.h>
#include <string.h>
#include <locale.h>

/* 
 * Sigh.  Have to use gconf_engine_get_local, or gconf prints a warning.  
 * In order to get this we have to enable gconf internals 
 */
#define GCONF_ENABLE_INTERNALS
#include <gconf/gconf-client.h>
#include <gconf/gconf-engine.h>
#undef  GCONF_ENABLE_INTERNALS

static gchar *defaults_edit_address = NULL;

gboolean
print_manager_prefs_is_gconf_admin_mode (void)
{
	return defaults_edit_address != NULL;
}

static GConfClient *
get_gconf_client (gboolean admin)
{
	static GConfClient *conf = NULL;
	static GConfClient *admin_conf = NULL;

	if (admin && defaults_edit_address) {
		if (!admin_conf) {
			GConfEngine *defaults = NULL;
			GError *err = NULL;

			defaults = 
				gconf_engine_get_local (defaults_edit_address, 
							&err);
			if (!defaults)
				return NULL;

			admin_conf = gconf_client_get_for_engine (defaults);
		}

		return admin_conf;
	} else {
		if (!conf) 
			conf = gconf_client_get_default ();

		return conf;
	}
}

gboolean 
print_manager_prefs_set_gconf_admin_path (gchar *edit_path)
{
	gchar *gconf_source_address = NULL;
	g_free (defaults_edit_address);

	gconf_source_address = strrchr (GCONF_CONFIG_SOURCE, ':');

	if (!gconf_source_address)
		gconf_source_address = PRINTMAN_GCONF_DEFAULTS_PATH;
	else
		gconf_source_address++;

	if (edit_path)
		defaults_edit_address = g_strconcat ("xml:readwrite:", 
						     edit_path, 
						     NULL);
	else
		defaults_edit_address = 
			g_strconcat ("xml:readwrite:", 
				     gconf_source_address, 
				     NULL);

	return get_gconf_client (TRUE) != NULL;
}

static gchar *
get_locale_name (void)
{
	gchar *lang;
  
	lang = g_strdup (setlocale (LC_CTYPE, NULL));
	if (!lang)
		lang = g_strdup (setlocale (LC_ALL, NULL));

	if (lang) {
		gchar *p;

		p = strchr (lang, '.');
		if (p)
			*p = '\0';
		p = strchr (lang, '@');
		if (p)
			*p = '\0';
	}

	return lang;
}

static gchar *
get_pref_name (gchar *name, gboolean use_locale)
{
	if (name && *name == '/')
		return g_strdup (name);
	else {
		gchar *ret_val;
		gchar *locale = NULL;

		if (use_locale)
			locale = get_locale_name ();

		/*
		 * If we are getting/setting an administrator or
		 * default key, prefix the last key with the current
		 * locale.
		 **/
		if (locale) {
			gchar *prefix = g_strdup (name);
			gchar *slash;

			slash = strrchr (prefix, '/');
			if (slash) {
				*slash = '\0';
				ret_val = g_build_filename (PRINTMAN_GCONF_DIR, 
							    prefix,
							    locale,
							    slash + 1,
							    NULL);
			} else
				ret_val = g_build_filename (PRINTMAN_GCONF_DIR, 
							    locale,
							    name,
							    NULL);
			g_free (prefix);
		} else 
			ret_val = g_build_filename (PRINTMAN_GCONF_DIR,
						    name,
						    NULL);

		g_free (locale);
		return ret_val;
	}
}

gboolean
print_manager_prefs_set_gconf (gchar         *name,
			       gboolean       admin,
			       PrintPrefType  type,
			       gconstpointer  value)
{
	GConfClient *conf;
	gboolean ret = FALSE;
	gchar *key;

	conf = get_gconf_client (admin);
	if (!conf) 
		return FALSE;

	key = get_pref_name (name, admin);

	switch (type) {
	case PRINT_PREF_TYPE_STRING:
		ret = gconf_client_set_string (conf, 
					       key, 
					       (gchar *) value, 
					       NULL);
		break;
	case PRINT_PREF_TYPE_BOOLEAN:
		ret = gconf_client_set_bool (conf, 
					     key, 
					     GPOINTER_TO_INT (value), 
					     NULL);
		break;
	case PRINT_PREF_TYPE_INT:
		ret = gconf_client_set_int (conf, 
					    key, 
					    GPOINTER_TO_INT (value), 
					    NULL);
		break;
	}

	/* 
	 * Need this or gconf never serializes to disk.  
	 * Might be because we are using a local GConfEngine.  
	 */
	gconf_client_suggest_sync (conf, NULL);

	g_free (key);

	return ret;
}

gpointer
print_manager_prefs_get_gconf (char     *name,
			       gboolean  admin,
			       gboolean  use_default,
			       gboolean *set_ret)
{
	char *myname;
	GConfClient *conf;
	GConfValue *val;
	gpointer ret = NULL;
	gboolean set = FALSE;

	conf = get_gconf_client (print_manager_prefs_is_gconf_admin_mode ());
	if (!conf)
		return NULL;

	myname = get_pref_name (name, admin);
	if (use_default)
		val = gconf_client_get (conf, myname, NULL);
	else
		val = gconf_client_get_without_default (conf, myname, NULL);
	g_free (myname);

	if (val) {
		switch (val->type) {
		case GCONF_VALUE_STRING:
			ret = g_strdup (gconf_value_get_string (val));
			set = TRUE;
			break;
		case GCONF_VALUE_INT:
			ret = GINT_TO_POINTER (gconf_value_get_int (val));
			set = TRUE;
			break;		
		case GCONF_VALUE_BOOL:
			ret = GINT_TO_POINTER (gconf_value_get_bool (val));
			set = TRUE;
			break;
		default:
			break;
		}

		gconf_value_free (val);
	}

	if (set_ret)
		*set_ret = set;
	return ret;
}

/*
 * CDE DT file parsing
 */
static gchar *
parse_dt_action_clause (gchar *src, const gchar *key)
{
	gchar *iter = src;
	gboolean line_cont = FALSE, key_found = FALSE;
	GString *val = g_string_new (NULL);

	while (iter && *iter) {
		int valend;

		/* skip whitespace at start of line */
		iter += strspn (iter, " \t\n");

		/* check for key if not multiline value */
		if (!line_cont) {
			if (!strncmp (iter, key, strlen (key)))
				key_found = TRUE;

			/* skip action name */
			iter += strcspn (iter, " \t");

			/* skip (madatory) whitespace before value */
			iter += strspn (iter, " \t");
		}

		valend = strcspn (iter, "\n");
		if (!valend) 
			valend = strlen (iter);

		/* check for multi-line value */
		if (iter [valend - 1] == '\\') {
			/* add next line to value */
			g_string_append_len (val, iter, valend - 1);
			line_cont = TRUE;
		}
		else {
			g_string_append_len (val, iter, valend);
			line_cont = FALSE;
		}

		if (!line_cont) {
			if (key_found) {
				gchar *ret = val->str;
				g_string_free (val, FALSE);
				return ret;
			} else
				g_string_truncate (val, 0);
		}

		/* skip newline */
		iter += valend;
	}

	g_string_free (val, TRUE);
	return NULL;
}

static gchar *
get_cde_dt_from_file (const gchar    *filename,
		      const gchar    *action,
		      const gchar    *key,
		      gboolean       *set_ret)
{
	GError *error = NULL;
	gchar  *contents, *src;
	gint len;
	gchar *ret = NULL;

	if (!g_file_get_contents (filename, &contents, &len, &error))
		return NULL;

	src = contents;

	/* parse ACTION foo_Print { } clauses */
	while (1) {
		gboolean found_action = FALSE;
		gchar *start, *end;

		src = strstr (src, "\nACTION ");
		if (!src)
			break;

		src += strlen ("\nACTION ");
		if (!strncmp (src, action, strlen (action)))
			found_action = TRUE;

		start = strchr (src, '{');
		if (!start)
			break;
		else
			src = start + 1;

		end = strchr (src, '}');
		if (!end)
			break;
		else
			*end = '\0';

		if (found_action) {
			ret = parse_dt_action_clause (src, key);
			if (ret)
				break;
		}

		src = end + 1;
	}

	g_free (contents);

	if (set_ret)
		*set_ret = (ret != NULL);

	return ret;
}

gchar *
print_manager_prefs_get_cde_dt (const gchar    *objname,
				gboolean        admin,
				const gchar    *action,
				const gchar    *key,
				gboolean       *set_ret)
{
	gchar *filename;
	gchar *path_lang = NULL, *path_no_lang = NULL;
	gchar *ret = NULL;
	gchar *locale_format = NULL;
	gboolean set = FALSE;
	gchar *lang;

	g_return_val_if_fail (objname != NULL, NULL);
	g_return_val_if_fail (action != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);

	lang = get_locale_name ();
	filename = g_strconcat (objname, ".dt", NULL);

	if (admin) {
		if (lang) 
			path_lang = g_build_filename ("/etc/dt/appconfig/types",
						      lang, 
						      filename, 
						      NULL);
		path_no_lang = g_build_filename ("/etc/dt/appconfig/types", 
						 filename,
						 NULL); 
	} else {
		if (lang)
			path_lang = g_build_filename (g_get_home_dir (), 
						      ".dt/types",
						      lang, 
						      filename, 
						      NULL);
		path_no_lang = g_build_filename (g_get_home_dir (),
						 ".dt/types", 
						 filename, 
						 NULL);
	}

	g_free (lang);
	g_free (filename);

	if (path_lang && g_file_test (path_lang, G_FILE_TEST_EXISTS))
		locale_format = get_cde_dt_from_file (path_lang, action, key, &set);
	else if (path_no_lang && g_file_test (path_no_lang, G_FILE_TEST_EXISTS))
		locale_format = get_cde_dt_from_file (path_no_lang, action, key, &set);

	g_free (path_lang);
	g_free (path_no_lang);

	if (set_ret)
		*set_ret = set;

	/* need to convert to UTF-8 */
	if (locale_format) {
		ret = g_locale_to_utf8 (locale_format,
					strlen (locale_format),
					NULL, NULL, NULL);
		if (!ret)
			ret = g_strdup (locale_format);
		g_free (locale_format);
	}
	return ret;
}

/*
 * CDE simple preferences file parsing
 */
static gchar *
get_cde_simple_read_line (GIOChannel *file)
{
	GError *error = NULL;
	GIOStatus status;
	gchar *line;
	gsize len;

	g_return_val_if_fail (file != NULL, NULL);

	status = g_io_channel_read_line (file, &line, &len, NULL, &error);
	if (status == G_IO_STATUS_ERROR || 
	    status == G_IO_STATUS_EOF || 
	    line == NULL)
		return NULL;

	g_strstrip (line);

	return line;
}

gchar *
print_manager_prefs_get_cde_simple (gchar         *filename,
				    gchar         *keyname,
				    gboolean      *set_ret)
{
	GIOChannel *filechan;
	GError *error = NULL;

	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (keyname != NULL, NULL);

	if (set_ret)
		*set_ret = FALSE;

	filechan = g_io_channel_new_file (filename, "r", &error);
	if (!filechan)
		return NULL;

	while (1) {
		gchar *key, *val;

		/* key */
		key = get_cde_simple_read_line (filechan);
		if (!key || !strlen (key))
			break;

		/* value */
		val = get_cde_simple_read_line (filechan);
		if (!val)
			break;
		else if (strlen (val) == 0)
			val = NULL;

		if (strcmp (key, keyname) == 0) {
			g_free (key);
			if (set_ret)
				*set_ret = TRUE;
			return val;
		} else {
			g_free (key);
			g_free (val);
		}
	}

	g_io_channel_unref (filechan);
	return NULL;
}

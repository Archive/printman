/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-main-window.h"

#include <gnome.h>
#include <string.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkiconfactory.h>

#include "print-manager-settings-dialog.h"
#include "print-manager-printer-properties.h"
#include "print-manager-printer-selection.h"
#include "print-manager-job-list.h"
#include "print-manager-about.h"
#include "print-manager-search.h"
#include "print-manager-dnd.h"
#include "print-manager-access.h"
#include "print-prefs.h"

#define MENU_KEY "/desktop/gnome/interface/menus_have_icons"

static GConfClient *gconf_client;

static GtkIconSize print_manager_icon_size = 0;

typedef struct {
	PrintManagerBackend *backend;

	GtkWidget           *window;
	GtkWidget           *tree_view;
	GtkWidget           *icon_list;
	GtkWidget           *notebook;
	GtkWidget           *statusbar;

	GtkWidget           *open;
	GtkWidget           *make_default;
	GtkWidget           *hide;
	GtkWidget           *properties;

	GtkWidget           *popup_open;
	GtkWidget           *popup_make_default;
	GtkWidget           *popup_hide;
	GtkWidget           *popup_properties;

	guint                backend_changes_done_id;
	GtkWidget           *no_printers;
	guint                no_printers_close_id;

	GtkWidget	    *popup_selection_menu;
	GtkListStore        *store;

	int                  drop_icon;

	guint                update_interval;
	guint                refresh_timeout_id;

	GSList              *printer_data_list; /* contains PrinterData */
} MainWindow;

typedef struct {
	MainWindow         *main_window;
	PrintManagerDevice *device;
	PrintManagerQueue  *queue;
	gboolean            is_available;
	gboolean            is_default;
	guint               connection_id;
	guint               connection_id_reload;
	GtkTreeIter         iter;
	gint                icon_idx;
} PrinterData;

typedef struct
{
	char *stock_id;
	char *name;
} PrintmanStockIcon;

enum {
	COL_LABEL,
	COL_STATUS,
	COL_QUEUE_SIZE,
	COL_DEVICE,
	LAST_COL
};

static PrintmanStockIcon items [] = {
	{ PRINT_MANAGER_DEFAULT, PRINT_MANAGER_ICONDIR"/default.png" },
	{ PRINT_MANAGER_BROKEN, PRINT_MANAGER_ICONDIR"/broken.png" },
	{ PRINT_MANAGER_INKJET, PRINT_MANAGER_ICONDIR"/inkjet.png" },
	{ PRINT_MANAGER_PERSONAL_LASER, PRINT_MANAGER_ICONDIR"/personal-laser.png" },
	{ PRINT_MANAGER_WORKGROUP_LASER, PRINT_MANAGER_ICONDIR"/workgroup-laser.png" }
};

static void  print_manager_init_stock_icons (void);

static void window_style_event_cb 		(GtkWidget *w,        GtkStyle *prev_style, 
						 MainWindow 	      *main_window);
static void menuitem_icon_visibility 		(GtkWidget *image,    gboolean use_image);
static void menuitem_icon_visibility_notify 	(GConfClient *client, guint cnxn_id,
						 GConfEntry  *entry,  gpointer user_data);

static GList *
get_icon_selected_devices (MainWindow *main_window)
{
	GList *selection;
	GList *ret = NULL;

	selection = 
		gnome_icon_list_get_selection (
			GNOME_ICON_LIST (main_window->icon_list));

	for (; selection != NULL; selection = selection->next) {
		PrintManagerDevice *device;
		
		device = 
			gnome_icon_list_get_icon_data (
				GNOME_ICON_LIST (main_window->icon_list),
				GPOINTER_TO_INT (selection->data));
		
		ret = g_list_prepend (ret, device);
	}

	return g_list_reverse (ret);
}

static void 
selection_foreach_cb (GtkTreeModel *model,
		      GtkTreePath *path,
		      GtkTreeIter *iter,
		      gpointer data)
{

	GList **list = data;
	PrintManagerDevice *device;
	
	gtk_tree_model_get (model, iter, COL_DEVICE, &device, -1);

	*list = g_list_prepend (*list, device);
}

static GList *
get_treeview_selected_devices (MainWindow *main_window)
{
	GtkTreeSelection *selection;
	GList *ret = NULL;

	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (main_window->tree_view));
	
	gtk_tree_selection_selected_foreach (selection, 
					     selection_foreach_cb,
					     &ret);
	
	return ret;
}

static GList *
get_selected_devices (MainWindow *main_window)
{
	switch (gtk_notebook_get_current_page (
			GTK_NOTEBOOK (main_window->notebook))) {
	case 0:
		return get_icon_selected_devices (main_window);
	case 1:
		return get_treeview_selected_devices (main_window);
	}

	return NULL;
}

static PrintManagerDevice *
get_single_selected_device (MainWindow *main_window)
{
	PrintManagerDevice *device;
	GList *selected;
	
	selected = get_selected_devices (main_window);
	if (!selected)
		return NULL;
	
	if (g_list_length (selected) > 1)
		g_warning ("More than one item selected and a single-item "
			   "command was selected.");

	device = PRINT_MANAGER_DEVICE (selected->data);
	
	g_list_free (selected);
	
	return device;
}

static void
show_selected_job_lists (MainWindow *main_window)
{
	GList *devices;
	GList *l;
	
	devices = get_selected_devices (main_window);
	
	for (l = devices; l != NULL; l = l->next) {
		PrintManagerDevice *device = PRINT_MANAGER_DEVICE (l->data);
		print_manager_show_job_list (main_window->backend,
					     device,
					     FALSE, NULL);
	}

	g_list_free (devices);
}


static void
set_menu_sensitivities (MainWindow *main_window)
{
	gboolean admin = print_manager_prefs_is_gconf_admin_mode ();
	GList *selected;
	int length;

	selected = get_selected_devices (main_window);
	length = g_list_length (selected);
	g_list_free (selected);

	gtk_widget_set_sensitive (main_window->open, length > 0);
	gtk_widget_set_sensitive (main_window->make_default, 
				  admin ? FALSE : length == 1);
	gtk_widget_set_sensitive (main_window->hide, length > 0);
	gtk_widget_set_sensitive (main_window->properties, length == 1);
}

static gboolean 
no_printers_close (GtkWidget *no_printers, MainWindow *main_window)
{
	g_signal_handler_disconnect (main_window->no_printers,
				     main_window->no_printers_close_id);
	main_window->no_printers_close_id = 0;
	main_window->no_printers = NULL;

	return FALSE;
}

static gint
count_visible_printers (PrintManagerBackend *backend)
{
	gint i, cnt, ret = 0;

	cnt = print_manager_backend_get_count (backend);
	for (i = 0; i < cnt; i++) {
		PrintManagerDevice *device;
		device = print_manager_backend_get_device (backend, i);
		if (print_manager_device_get_is_visible (device))
			ret++;
	}

	return ret;
}

static void
backend_changes_done (PrintManagerBackend *backend, MainWindow *main_window)
{
	if (print_manager_backend_get_count (backend) == 0) {
		if (main_window->no_printers == NULL) {
			main_window->no_printers = 
				gnome_app_warning (
					GNOME_APP (main_window->window),
					_("No printers found."));

			main_window->no_printers_close_id =
				g_signal_connect (
					main_window->no_printers, "close",
					G_CALLBACK (no_printers_close),
					main_window);
		}

		gtk_widget_show (main_window->no_printers);
		if (main_window->no_printers->window)
			gdk_window_raise (main_window->no_printers->window);

		gtk_statusbar_push (GTK_STATUSBAR (main_window->statusbar), 
				    0,
				    _("No Printers"));
	} else if (print_manager_backend_get_default_printer (backend) == -1 &&
		   count_visible_printers (backend) == 0) {
		GtkWidget *dialog;
		gint resp;

		dialog = 
			gtk_message_dialog_new (
				GTK_WINDOW (main_window->window), 
				GTK_DIALOG_MODAL, 
				GTK_MESSAGE_QUESTION,
				GTK_BUTTONS_YES_NO,
				_("There are no printers set as visible.  "
				  "Would you like to select the printers to "
				  "show?"));
		resp = gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		if (resp == GTK_RESPONSE_YES)
			print_manager_show_printer_selection (
				main_window->backend, 
				GTK_WINDOW (main_window->window));
	}

	g_signal_handler_disconnect (backend,
				     main_window->backend_changes_done_id);
	main_window->backend_changes_done_id = 0;
}


static void
about_cb (GtkWidget *widget, gpointer data)
{
	print_manager_about ();
}

static void
open_cb (GtkWidget * widget, MainWindow *main_window)
{
	show_selected_job_lists (main_window);
}

static void
default_cb (GtkWidget * widget, MainWindow *main_window)
{
	PrintManagerDevice *device;
	
	device = get_single_selected_device (main_window);

	if (device) {
		int index = 
			print_manager_backend_get_device_index (
				main_window->backend, 
				device);
		print_manager_backend_set_default_printer (main_window->backend,
							   index);
		print_manager_backend_reload (main_window->backend);
	}
}

static void
icons_cb (GtkWidget * widget, MainWindow *main_window)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (main_window->notebook), 0);
}

static GdkPixbuf *
load_pixbuf (const char *filename,
	     gboolean is_default,
	     gboolean is_broken,
	     MainWindow *main_window)
{
	GdkPixbuf *pixbuf; 
	GdkPixbuf *pixbuf_rendered;
	gint i;

	pixbuf = NULL;
	pixbuf_rendered = NULL;

	for (i = 0; i <  G_N_ELEMENTS (items); ++i) {
		if (filename && items[i].name && !strcmp (filename, items[i].name)) {
			pixbuf_rendered = gtk_widget_render_icon (GTK_WIDGET (main_window->window), 
								  items[i].stock_id, print_manager_icon_size, NULL);
			break;
		}
	}

	if (!pixbuf_rendered) {
		pixbuf_rendered = gdk_pixbuf_new_from_file (filename, NULL);
	}

	pixbuf = gdk_pixbuf_copy (pixbuf_rendered);

	if (pixbuf_rendered) {
		g_object_unref (pixbuf_rendered);
		pixbuf_rendered = NULL;
	}
		

	if (is_default) {
		GdkPixbuf *default_pixbuf;

		default_pixbuf = gtk_widget_render_icon (GTK_WIDGET (main_window->window), 
							 PRINT_MANAGER_DEFAULT, print_manager_icon_size, NULL);

		gdk_pixbuf_composite (default_pixbuf,
				      pixbuf,
				      0, 0,
				      gdk_pixbuf_get_width (default_pixbuf),
				      gdk_pixbuf_get_height (default_pixbuf),
				      0, 0,
				      1, 1,
				      GDK_INTERP_HYPER,
				      255);
		if (default_pixbuf) {
			g_object_unref (default_pixbuf);
			default_pixbuf = NULL;
		}
	}

	if (is_broken) {
		GdkPixbuf *broken_pixbuf = NULL;

		broken_pixbuf =	gtk_widget_render_icon (GTK_WIDGET (main_window->window),
			       				PRINT_MANAGER_BROKEN, print_manager_icon_size, NULL);

		gdk_pixbuf_composite (broken_pixbuf,
				      pixbuf,
				      0, 0,
				      gdk_pixbuf_get_width (broken_pixbuf),
				      gdk_pixbuf_get_height (broken_pixbuf),
				      0, 0,
				      1, 1,
				      GDK_INTERP_HYPER,
				      255);
		if (broken_pixbuf) {
			g_object_unref (broken_pixbuf);
			broken_pixbuf = NULL;
		}
	}

	return pixbuf;
}

static gboolean
printer_is_default (PrintManagerBackend *backend, 
		    PrintManagerDevice  *device)
{
	gint dev_idx, default_idx;

	dev_idx = print_manager_backend_get_device_index (backend, device);
	default_idx = print_manager_backend_get_default_printer (backend);

	return dev_idx == default_idx;
}

static void
update_icon_for_device (MainWindow         *main_window, 
			PrinterData        *data,
			gboolean            create,
			gboolean            get_status)
{
	GnomeIconList *icon_list;
	gboolean is_default, is_available = TRUE;
	const char *filename;
	GdkPixbuf *pixbuf = NULL;

	filename = print_manager_device_get_icon_path (data->device);
	icon_list = GNOME_ICON_LIST (main_window->icon_list);

	is_default = printer_is_default (main_window->backend, data->device);

	if (get_status) {
		PrintManagerQueue *queue;
		queue = print_manager_device_get_queue (data->device);
		is_available = print_manager_queue_get_available (queue);
	}

	/* Don't remove and recreate the icon if status is the same */
	if (!create && 
	    data->is_available == is_available && 
	    data->is_default == is_default) {
		return;
	} else {
		data->is_available = is_available;
		data->is_default = is_default;
	}

	pixbuf = load_pixbuf (filename, is_default, !is_available, main_window);

	if (create) {
		data->icon_idx = 
			gnome_icon_list_append_pixbuf (
				icon_list,
				pixbuf,
				filename,
				print_manager_device_get_label (data->device));
	} else {
		GList *selection;
		gboolean selected;
		gint adjust;

		selection = gnome_icon_list_get_selection (icon_list);
		selected = 
			g_list_find (selection, 
				     GINT_TO_POINTER (data->icon_idx)) != NULL;

		adjust = gtk_adjustment_get_value (icon_list->adj);

		gnome_icon_list_remove (icon_list, data->icon_idx);
		gnome_icon_list_insert_pixbuf (
			icon_list,
			data->icon_idx,
			pixbuf,
			filename,
			print_manager_device_get_label (data->device));

		gtk_adjustment_set_value (icon_list->adj, adjust);

		if (selected)
			gnome_icon_list_select_icon (icon_list, data->icon_idx);
	}

	gnome_icon_list_set_icon_data (icon_list, data->icon_idx, data->device);
	
	if (pixbuf) {
		g_object_unref (pixbuf);
		pixbuf = NULL;
	}
}

static void
update_list_for_device (MainWindow         *main_window, 
			PrinterData        *data,
			gboolean            create,
			gboolean            get_status)
{
	gboolean is_default = printer_is_default (main_window->backend, data->device);

	if (create) {
		char *label;
		char *status;
		char *tmp;

		if (is_default) {
			tmp = g_markup_escape_text (print_manager_device_get_label (data->device), -1);
			label = g_strdup_printf ("<span weight=\"bold\">%s</span>", tmp);
			g_free (tmp);
			status = "<span weight=\"bold\">Checking...</span>"; 
		}
		else {
			label = g_markup_escape_text (print_manager_device_get_label (data->device), -1);
			status = "Checking..."; 
		}

		/* Add printer to list store */
		gtk_list_store_append (main_window->store, &data->iter);
		gtk_list_store_set (
			main_window->store,
			&data->iter,
			COL_LABEL, 
			label,	
			COL_STATUS, status,
			COL_DEVICE, data->device,
			-1);
		g_free (label);
		g_object_ref (data->device);
	}

	if (get_status) {
		PrintManagerQueue *queue;
		gchar *count;
		gchar *status;
		gchar *status_text;

		/* set status and job count */
		queue = print_manager_device_get_queue (data->device);
		if (print_manager_queue_get_available (queue)) {
			status_text = g_strdup_printf ("%s", "Active");
		}
		else {
			status_text = g_strdup_printf ("%s", "Not Active");
		}
		if (is_default) {
			count = g_strdup_printf ("<span weight=\"bold\">%d</span>", 
					 	print_manager_queue_get_count (queue));
			status = g_strdup_printf ("<span weight=\"bold\">%s</span>",
						status_text);
		}
		else {
			count = g_strdup_printf ("%d", 
					 	print_manager_queue_get_count (queue));
			status = g_strdup_printf ("%s", status_text);
		}
		gtk_list_store_set (
			main_window->store,
			&data->iter,
			COL_STATUS, status, 
			COL_QUEUE_SIZE, count,
			-1);
		g_free (count);
		g_free (status);
		g_free (status_text);
	}
}

static void
list_cb (GtkWidget * widget, MainWindow *main_window)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (main_window->notebook), 1);
}

static void
select_printers_cb (GtkWidget * widget, MainWindow *main_window)
{
	print_manager_show_printer_selection (main_window->backend, 
					      GTK_WINDOW (main_window->window));
}

static void
search_cb (GtkWidget * widget, MainWindow *main_window)
{
	print_manager_show_search (main_window->backend, 
				   GTK_WINDOW (main_window->window));
}

static void
show_status_bar_cb (GtkWidget * widget, MainWindow *main_window)
{
	if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (widget)))
		gtk_widget_show (main_window->statusbar);
	else
		gtk_widget_hide (main_window->statusbar);
}


static void
quit_cb (GtkWidget * widget, MainWindow *main_window)
{
	gtk_main_quit ();
}

static void
refresh_main_cb (GtkWidget *widget, MainWindow *main_window)
{
	if (main_window->backend_changes_done_id == 0) {
		main_window->backend_changes_done_id =
			g_signal_connect (main_window->backend, "changes_done",
					  G_CALLBACK (backend_changes_done),
					  main_window);
	}
	print_manager_backend_reload (main_window->backend);
}

static void
hide_cb (GtkWidget * widget, MainWindow *main_window)
{
	GList *selected;
	GList *l;

	selected = get_selected_devices (main_window);
	
	for (l = selected; l != NULL; l = l->next) {
		PrintManagerDevice *device = PRINT_MANAGER_DEVICE (l->data);
		print_manager_device_set_is_visible (device, FALSE);
	}

	g_list_free (selected);
}

static void
printer_properties_cb (GtkWidget * widget, MainWindow *main_window)
{
	GList *selected;
	GList *l;

	selected = get_selected_devices (main_window);
	
	for (l = selected; l != NULL; l = l->next) {
		PrintManagerDevice *device = PRINT_MANAGER_DEVICE (l->data);
		print_manager_show_printer_properties (
			device,
			GTK_WINDOW (main_window->window));
	}
}

static void
preferences_cb (GtkWidget * widget, MainWindow *main_window)
{
	print_manager_show_settings_dialog (main_window->backend, 
					    GTK_WINDOW (main_window->window));
}

#if 0
/* Might need if we choose not to use GNOMEUIINFO_HELP, cause its busted */
static void
help_contents_cb (GtkWidget * widget, MainWindow *main_window)
{
	gnome_help_display ("gnome-print-manager.xml", NULL, NULL);
}
#endif

static GtkWidget *
create_menubar (GtkAccelGroup       *accel_group, 
		GtkTooltips         *tooltips, 
		MainWindow          *main_window)
{
	GtkWidget *menubar, *menuitem, *menu;
	GSList *group;


	menubar = gtk_menu_bar_new ();
	gtk_widget_show (menubar);

	/* Printer menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Printer"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_OPEN, 
					  accel_group, G_CALLBACK (open_cb), 
					  gconf_client, main_window);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem, 
			      _("Show the printer's queue"),
			      NULL);

	main_window->open = menuitem;

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic (_("Set as _Default"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem,
			      _("Set the printer as the default printer"),
			      NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (default_cb), 
			  main_window);

	main_window->make_default = menuitem;

	menuitem = gtk_menu_item_new_with_mnemonic (_("_Hide"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, menuitem, _("Hide the printer"), NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (hide_cb), main_window);

	main_window->hide = menuitem;

	menuitem = append_stock_menuitem (menu, GTK_STOCK_PROPERTIES, 
					  accel_group, G_CALLBACK (printer_properties_cb), 
					  gconf_client, main_window);

	main_window->properties = menuitem;

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_QUIT, 
					  accel_group, G_CALLBACK (quit_cb), 
					  gconf_client, main_window);

	/* Edit menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Edit"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = gtk_menu_item_new_with_mnemonic (_("_Search for Document"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem,
			      _("Search for a document being printed"), 
			      NULL);
	main_window->popup_selection_menu = menuitem;
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (search_cb), 
			  main_window);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_PREFERENCES, 
					  accel_group, G_CALLBACK (preferences_cb), 
					  gconf_client, main_window);


	/* View menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_View"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = gtk_radio_menu_item_new_with_mnemonic (NULL, _("_Icons"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (icons_cb), 
			  main_window);

	group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (menuitem));
	menuitem = gtk_radio_menu_item_new_with_mnemonic (group, _("_List"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (list_cb), 
			  main_window);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem =
		gtk_menu_item_new_with_mnemonic (_("Select _Printers to Show"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (select_printers_cb), 
			  main_window);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem =
		gtk_check_menu_item_new_with_mnemonic (_("Show _Status Bar"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menuitem), TRUE);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (
		tooltips, 
		menuitem,
		_("Whether to show the status bar at the bottom of the window"),
		NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem,
			  "activate",
			  G_CALLBACK (show_status_bar_cb), 
			  main_window);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_REFRESH, 
					  accel_group, G_CALLBACK (refresh_main_cb), 
					  gconf_client, main_window);

	/* Help menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Help"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);
	
	{
		GnomeUIInfo uiinfo [] = {
			GNOMEUIINFO_HELP ((gchar *) "gnome-print-manager"),
			GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
			GNOMEUIINFO_END
		};
		gnome_app_fill_menu (GTK_MENU_SHELL (menu), 
				     uiinfo,
				     accel_group,
				     TRUE,
				     0);
	}

	/* -- What is this supposed to do?
	menuitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_INDEX,
						       accel_group);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	*/

	return menubar;
}


static void
show_popup_menu (GdkEvent * event, MainWindow *main_window)
{
	gboolean admin = print_manager_prefs_is_gconf_admin_mode ();
	static GtkWidget *menu = NULL;
	GtkWidget *menuitem;
	GList *selection;
	int length;

	if (!menu) {
		menu = gtk_menu_new ();
		menuitem = append_stock_menuitem (menu, GTK_STOCK_OPEN,
						  NULL, G_CALLBACK (open_cb),
						  gconf_client, main_window);

		main_window->popup_open = menuitem;

		menuitem = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem =
			gtk_menu_item_new_with_mnemonic (_("Set as _Default"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		g_signal_connect (menuitem, "activate",
				  G_CALLBACK (default_cb), main_window);
		main_window->popup_make_default = menuitem;

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Hide"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		g_signal_connect (menuitem, "activate",
				  G_CALLBACK (hide_cb), main_window);
		main_window->popup_hide = menuitem;
		
		menuitem = append_stock_menuitem (menu, GTK_STOCK_PROPERTIES,
						  NULL, G_CALLBACK (printer_properties_cb),
						  gconf_client, main_window);
		main_window->popup_properties = menuitem;
	}

	selection = get_selected_devices (main_window);
	length = g_list_length (selection);
	g_list_free (selection);

	gtk_widget_set_sensitive (main_window->popup_open, length > 0);
	gtk_widget_set_sensitive (main_window->popup_make_default, 
				  admin ? FALSE : length == 1);
	gtk_widget_set_sensitive (main_window->popup_hide, length > 0);
	gtk_widget_set_sensitive (main_window->popup_properties, length == 1);

	gtk_menu_popup (GTK_MENU (menu), 
			NULL, 
			NULL, 
			NULL, 
			NULL,
			event ? event->button.button : 3, 
			event ? event->button.time : GDK_CURRENT_TIME);
}

static void
popup_cb (GtkWidget *widget, MainWindow *main_window)
{
	show_popup_menu (NULL, main_window);
}

static gboolean
key_press_event_cb (GtkWidget *widget,
		    GdkEventKey *event,
		    MainWindow *main_window)
{
	if (event)
		if (event->keyval == GDK_Return ||
			event->keyval == GDK_KP_Enter ||
			event->keyval == GDK_ISO_Enter ||
			event->keyval == GDK_3270_Enter ||
			event->keyval == GDK_space ||
			event->keyval == GDK_KP_Space) {
			show_selected_job_lists (main_window);
			return TRUE;
		}
	return FALSE;
}

static void
icon_unselected_cb (GtkWidget  *widget,
		    int         num, 
		    GdkEvent   *event,
		    MainWindow *main_window)
{
	set_menu_sensitivities (main_window);
}

static void
icon_selected_cb (GtkWidget           *widget, 
		  int                  num, 
		  GdkEvent            *event, 
		  MainWindow *main_window)
{
	set_menu_sensitivities (main_window);

	if (event && event->type == GDK_2BUTTON_PRESS)
		show_selected_job_lists (main_window);

	if (event && 
	    event->type == GDK_BUTTON_PRESS && 
	    event->button.button == 3)
		show_popup_menu (event, main_window);
}

static void
tree_selection_changed_cb (GtkTreeSelection *selection, 
			   MainWindow       *main_window)
{
	set_menu_sensitivities (main_window);
}

static void
row_activated_cb (GtkWidget   *widget, 
		  GtkTreePath *path, 
		  int          column,
		  MainWindow  *main_window)
{
	show_selected_job_lists (main_window);
}

static gboolean
list_button_press_event_cb (GtkWidget  *widget, 
			    GdkEvent   *event, 
			    MainWindow *main_window)
{
	if (event->type == GDK_BUTTON_PRESS && event->button.button == 3) {
		show_popup_menu (event, main_window);
		return TRUE;
	}

	return FALSE;
}

static gboolean
main_window_delete_event_cb (GtkWidget *widget, 
			     GdkEvent  *event,
			     gpointer   data)
{
	gtk_main_quit ();
	return TRUE;
}

static gint
printer_name_compare (gconstpointer p1, gconstpointer p2) 
{
	PrintManagerDevice *printer1 = (PrintManagerDevice *) p1;
	PrintManagerDevice *printer2 = (PrintManagerDevice *) p2;

	return strcmp (print_manager_device_get_queue_name (printer1),
			       print_manager_device_get_queue_name (printer2));
}

static void
disconnect_handlers (MainWindow *main_window)
{
	GSList *iter;

	for (iter = main_window->printer_data_list; iter; iter = iter->next) {
		PrinterData *data = iter->data;

		if (data->connection_id_reload)
			g_signal_handler_disconnect (
				data->queue, 
				data->connection_id_reload);
		g_signal_handler_disconnect (data->queue, data->connection_id);
		g_object_unref (data->device);
		g_object_unref (data->queue);
		g_free (data);		
	}
	main_window->printer_data_list = NULL;

	if (main_window->refresh_timeout_id) {
		g_source_remove (main_window->refresh_timeout_id);
		main_window->refresh_timeout_id = 0;
	}
}

static void
queue_changed (PrintManagerQueue *queue, gpointer user_data)
{
	PrinterData *data = user_data;

	update_icon_for_device (data->main_window, 
				data,
				FALSE,
				TRUE);
	update_list_for_device (data->main_window, 
				data,
				FALSE,
				TRUE);
}

static void
update_status_bar (MainWindow *main_window, gboolean loading)
{
	PrintManagerBackend *backend = main_window->backend;
	PrintManagerDevice *printer;
	GString *status_str;
	int hidden_cnt = 0;
	int count, i;

	count = print_manager_backend_get_count (backend);
	for (i = 0; i < count; i++) {
		printer = print_manager_backend_get_device (backend, i);
		if (!print_manager_device_get_is_visible (printer))
			hidden_cnt++;
	}

	status_str = g_string_new (NULL);

	if (count == 1)
		g_string_append (status_str, _("1 Printer"));
	else if (count > 1)
		g_string_append_printf (status_str, _("%d Printers"), count);

	if (status_str->len) {
		if (hidden_cnt > 0) {
			g_string_append_printf (status_str, 
						_(", %d Hidden"), 
						hidden_cnt);
		}
		
		if (loading) {
			g_string_append (status_str, 
					 _(" (reading printer status...)"));
		}

		gtk_statusbar_push (GTK_STATUSBAR (main_window->statusbar), 
				    0,
				    status_str->str);
	}

	g_string_free (status_str, TRUE);	
}

static gboolean start_queue_reload_timeout (gpointer user_data);

static void
queue_changed_load_next (PrintManagerQueue *queue, gpointer user_data)
{
	GSList *cur = user_data;
	PrinterData *data;
	gint interval;
	
	if (!cur)
		return;

	data = cur->data;

	if (data->connection_id_reload) {
		g_signal_handler_disconnect (data->queue,
					     data->connection_id_reload);
		data->connection_id_reload = 0;
	}

	if (cur->next) {
		PrinterData *next_data = cur->next->data;
		next_data->connection_id_reload = 
			g_signal_connect (next_data->queue,
					  "changed",
					  G_CALLBACK (queue_changed_load_next),
					  cur->next);
		print_manager_queue_reload (next_data->queue);
	} else {
		interval = 
			print_manager_backend_get_update_interval (
				data->main_window->backend);

		data->main_window->refresh_timeout_id =
			g_timeout_add (interval * 1000,
				       start_queue_reload_timeout,
				       data->main_window);

		update_status_bar (data->main_window, FALSE);
	}
}

static gboolean
start_queue_reload_timeout (gpointer user_data)
{
	MainWindow *main_window = user_data;
	PrinterData *data;

	if (!main_window->printer_data_list) {
		update_status_bar (main_window, FALSE);
		return FALSE;
	}

	update_status_bar (main_window, TRUE);

	data = main_window->printer_data_list->data;
	data->connection_id_reload = 
		g_signal_connect (data->queue,
				  "changed",
				  G_CALLBACK (queue_changed_load_next),
				  main_window->printer_data_list);
	print_manager_queue_reload (data->queue);
	main_window->refresh_timeout_id = 0;
	return FALSE;
}

static void
backend_changed (PrintManagerBackend *backend, MainWindow *main_window)
{
	int count, i;
	GSList *sort_list = NULL, *iter;

	disconnect_handlers (main_window);

	gtk_list_store_clear (main_window->store);
	gnome_icon_list_clear (GNOME_ICON_LIST (main_window->icon_list));

	count = print_manager_backend_get_count (backend);
	for (i = 0; i < count; i++) {
		PrintManagerDevice *printer;

		printer = print_manager_backend_get_device (backend, i);
		sort_list = g_slist_insert_sorted (sort_list, 
						   printer, 
						   printer_name_compare);
	}
	for (iter = sort_list; iter; iter = iter->next) {
		PrintManagerDevice *printer = iter->data;

		if (print_manager_device_get_is_visible (printer)) {
			PrinterData *data;
			gboolean load_status;

			data = g_new0 (PrinterData, 1);
			data->main_window = main_window;

			data->device = printer;
			g_object_ref (data->device);

			data->queue = print_manager_device_get_queue (printer);
			g_object_ref (data->queue);

			load_status = 
				print_manager_queue_get_is_loaded (data->queue);

			update_icon_for_device (main_window, 
						data,
						TRUE,
						load_status);
			update_list_for_device (main_window, 
						data,
						TRUE,
						load_status);

			data->connection_id =
				g_signal_connect (data->queue,
						  "changed",
						  G_CALLBACK (queue_changed),
						  data);

			main_window->printer_data_list = 
				g_slist_prepend (main_window->printer_data_list,
						 data);
		}
	}
	g_slist_free (sort_list);

	if (main_window->printer_data_list) {
		main_window->printer_data_list = 
			g_slist_reverse (main_window->printer_data_list);

		start_queue_reload_timeout (main_window);
	} else {
		update_status_bar (main_window, FALSE);
	}

	gtk_widget_set_sensitive (main_window->popup_selection_menu, (print_manager_backend_get_count (main_window->backend) > 0));
	set_menu_sensitivities (main_window);
}


static gboolean
tree_drag_motion_cb (GtkWidget *tree_view, 
		     GdkDragContext *context, 
		     int x, int y, 
		     guint time, 
		     MainWindow *main_window)
{
	GtkTreePath *path;

	if (gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (tree_view),
					       x, y, 
					       &path, 
					       NULL)) {
		gtk_tree_view_set_drag_dest_row (
			GTK_TREE_VIEW (tree_view), 
			path,
			GTK_TREE_VIEW_DROP_INTO_OR_AFTER);
		gtk_tree_path_free (path);
		gdk_drag_status (context, GDK_ACTION_COPY, time);
	} else {
		gtk_tree_view_set_drag_dest_row (
			GTK_TREE_VIEW (tree_view), 
			NULL,
			GTK_TREE_VIEW_DROP_INTO_OR_AFTER);
		gdk_drag_status (context, 0, time);
	}

	return TRUE;
}

static gboolean 
tree_drag_drop_cb (GtkWidget *tree_view, 
		   GdkDragContext *context,
		   int x, int y, 
		   guint time)
{
	GtkTreePath *path;

	if (gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (tree_view),
					       x, y, 
					       &path, 
					       NULL)) {
		GdkAtom target;
		
		gtk_tree_view_set_drag_dest_row (
			GTK_TREE_VIEW (tree_view), 
			path,
			GTK_TREE_VIEW_DROP_INTO_OR_AFTER);
		gtk_tree_path_free (path);

		target = 
			gtk_drag_dest_find_target (
				tree_view, 
				context, 
				gtk_drag_dest_get_target_list (tree_view));

		if (target != GDK_NONE)
			gtk_drag_get_data (tree_view, context, target, time);
	}

	return TRUE;
}

static PrintManagerDevice *
get_tree_drag_dest_device (MainWindow *main_window)
{
	GtkTreePath *path;
	GtkTreeIter iter;
	PrintManagerDevice *device = NULL;

	gtk_tree_view_get_drag_dest_row (
		GTK_TREE_VIEW (main_window->tree_view), 
		&path, 
		NULL);

	if (path) {
		gtk_tree_model_get_iter (
			GTK_TREE_MODEL (main_window->store), 
			&iter, 
			path);
		gtk_tree_model_get (GTK_TREE_MODEL (main_window->store),
				    &iter, 
				    COL_DEVICE, 
				    &device, 
				    -1);
	}
	
	return device;
}

static void
tree_drag_data_received_cb (GtkWidget *tree_view, 
			    GdkDragContext *context, 
			    int x, int y, 
			    GtkSelectionData *selection_data,
			    guint info, 
			    guint time, 
			    MainWindow *main_window)
{
	PrintManagerDevice *device;

	device = get_tree_drag_dest_device (main_window);
	
	gtk_tree_view_set_drag_dest_row (GTK_TREE_VIEW (tree_view), 
					 NULL,
					 GTK_TREE_VIEW_DROP_INTO_OR_AFTER);

	if (device)
		print_manager_dnd_print_uri_list (
			device, 
			(char *) selection_data->data);

	gtk_drag_finish (context, device != NULL, FALSE, time);
}


static gboolean
icon_list_drag_motion_cb (GtkWidget *icon_list, 
			  GdkDragContext *context, 
			  int x, int y, 
			  guint time, 
			  MainWindow *main_window)
{
	int icon;
	
	icon = gnome_icon_list_get_icon_at (GNOME_ICON_LIST (icon_list), x, y);
	
	if (icon > -1)
		gdk_drag_status (context, GDK_ACTION_COPY, time);
	else
		gdk_drag_status (context, 0, time);

	return TRUE;
}

static gboolean 
icon_list_drag_drop_cb (GtkWidget *icon_list, GdkDragContext *context,
		       int x, int y, guint time, MainWindow *main_window)
{
	int icon;
	
	icon = gnome_icon_list_get_icon_at (GNOME_ICON_LIST (icon_list), x, y);
	
	if (icon > -1) {
		GdkAtom target;		

		target = 
			gtk_drag_dest_find_target (
				icon_list, 
				context, 
				gtk_drag_dest_get_target_list (icon_list));

		if (target != GDK_NONE) {
			main_window->drop_icon = icon;
			gtk_drag_get_data (icon_list, context, target, time);
		}
	}

	return TRUE;
}

static PrintManagerDevice *
get_icon_list_drag_dest_device (MainWindow *main_window)
{
	PrintManagerDevice *device;

	device = 
		gnome_icon_list_get_icon_data (
			GNOME_ICON_LIST (main_window->icon_list),
			main_window->drop_icon);

	return device;
}

static void
icon_list_drag_data_received_cb (GtkWidget *icon_list, 
				 GdkDragContext *context, 
				 int x, int y, 
				 GtkSelectionData *selection_data,
				 guint info, 
				 guint time, 
				 MainWindow *main_window)
{
	PrintManagerDevice *device;

	device = get_icon_list_drag_dest_device (main_window);
	
	if (device)
		print_manager_dnd_print_uri_list (
			device, 
			(char *) selection_data->data);

	gtk_drag_finish (context, device != NULL, FALSE, time);
}


static void
create_main_window (MainWindow *main_window)
{
	GtkWidget *menubar;
	GtkWidget *scrolledwin;
	GtkAccelGroup *accel_group;
	GtkTooltips *tooltips;
	GtkTreeViewColumn *col;
	GtkCellRenderer *rend;
	GtkTreeSelection *selection;

	main_window->window =
		gnome_app_new ("printman", _("Print Manager"));
	g_signal_connect (main_window->window,
			  "delete-event",
			  G_CALLBACK (main_window_delete_event_cb),
			  main_window);

	accel_group = gtk_accel_group_new ();
	gtk_window_add_accel_group (GTK_WINDOW (main_window->window),
				    accel_group);

	tooltips = gtk_tooltips_new ();

	menubar = create_menubar (accel_group, tooltips, main_window);
	gnome_app_set_menus (GNOME_APP (main_window->window),
			     GTK_MENU_BAR (menubar));

	main_window->notebook = gtk_notebook_new ();
	gtk_widget_show (main_window->notebook);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (main_window->notebook),
				    FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (main_window->notebook),
				      FALSE);
	gnome_app_set_contents (GNOME_APP (main_window->window),
				main_window->notebook);

	/* Icon view. */
	print_manager_init_stock_icons ();
	scrolledwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwin);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwin),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwin),
					     GTK_SHADOW_IN);
	gtk_notebook_append_page (GTK_NOTEBOOK (main_window->notebook), 
				  scrolledwin,
				  gtk_label_new (NULL));

	main_window->icon_list = gnome_icon_list_new (64, NULL, 0);
	gnome_icon_list_set_col_spacing (
		GNOME_ICON_LIST (main_window->icon_list),
		20);
	gnome_icon_list_set_selection_mode (
		GNOME_ICON_LIST (main_window->icon_list),
		GTK_SELECTION_MULTIPLE);

	_add_atk_name_desc (GTK_WIDGET (main_window->icon_list),
			    _("Printer Icon View"),
			    _("Lists visible printers"));
	
	gtk_widget_show (main_window->icon_list);
	gtk_container_add (GTK_CONTAINER (scrolledwin),
			   main_window->icon_list);

	g_signal_connect (main_window->icon_list,
			  "select_icon",
			  G_CALLBACK (icon_selected_cb), 
			  main_window);
	g_signal_connect (main_window->icon_list,
			  "unselect_icon",
			  G_CALLBACK (icon_unselected_cb), 
			  main_window);
	g_signal_connect (main_window->icon_list,
			  "popup_menu",
			  G_CALLBACK (popup_cb),
			  main_window);
	g_signal_connect (main_window->icon_list,
			  "key_press_event",
			  G_CALLBACK (key_press_event_cb),
			  main_window);
	g_signal_connect (main_window->window,
			  "style-set",
			  (GCallback) window_style_event_cb,
			   main_window);

	print_manager_dnd_setup (main_window->icon_list,
				 G_CALLBACK (icon_list_drag_motion_cb),
				 G_CALLBACK (icon_list_drag_drop_cb),
				 G_CALLBACK (icon_list_drag_data_received_cb),
				 main_window);

	/* List view. */
	scrolledwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwin);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwin),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwin),
					     GTK_SHADOW_IN);
	gtk_notebook_append_page (GTK_NOTEBOOK (main_window->notebook), 
				  scrolledwin,
				  gtk_label_new (NULL));

	main_window->store = gtk_list_store_new (LAST_COL, 
						 G_TYPE_STRING, 
						 G_TYPE_STRING,
						 G_TYPE_STRING,
						 G_TYPE_POINTER);

	main_window->tree_view = 
		gtk_tree_view_new_with_model (
			GTK_TREE_MODEL (main_window->store));
	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (main_window->tree_view));
	gtk_tree_selection_set_mode (GTK_TREE_SELECTION (selection),
				     GTK_SELECTION_MULTIPLE);
	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (tree_selection_changed_cb),
			  main_window);

	_add_atk_name_desc (GTK_WIDGET (main_window->tree_view),
			    _("Printer List View"),
			    _("Lists visible printer details"));

	print_manager_dnd_setup (main_window->tree_view,
				 G_CALLBACK (tree_drag_motion_cb),
				 G_CALLBACK (tree_drag_drop_cb),
				 G_CALLBACK (tree_drag_data_received_cb),
				 main_window);

	g_object_unref (G_OBJECT (main_window->store));
	gtk_widget_show (main_window->tree_view);
	gtk_container_add (GTK_CONTAINER (scrolledwin),
			   main_window->tree_view);
	g_signal_connect (main_window->tree_view,
			  "row_activated",
			  G_CALLBACK (row_activated_cb), 
			  main_window);
	g_signal_connect (main_window->tree_view,
			  "button_press_event",
			  G_CALLBACK (list_button_press_event_cb), 
			  main_window);
	g_signal_connect (main_window->tree_view,
			  "popup_menu",
			  G_CALLBACK (popup_cb),
			  main_window);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Printer Name"),
							rend, 
							"markup", 
							COL_LABEL,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (main_window->tree_view), 
				     col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Documents"), 
							rend,
							"markup", 
							COL_QUEUE_SIZE, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (main_window->tree_view), 
				     col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Status"), 
							rend,
							"markup", 
							COL_STATUS, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (main_window->tree_view), 
				     col);

	main_window->statusbar = gtk_statusbar_new ();
	gtk_statusbar_push (GTK_STATUSBAR (main_window->statusbar), 
			    0,
			    _("Loading Printers..."));
	gnome_app_set_statusbar (GNOME_APP (main_window->window), 
				 main_window->statusbar);

	/* Start receiving the list of printers */
	g_signal_connect (main_window->backend, "changed",
			  G_CALLBACK (backend_changed), 
			  main_window); 
	main_window->backend_changes_done_id =
		g_signal_connect (main_window->backend, "changes_done",
				  G_CALLBACK (backend_changes_done),
				  main_window);

	/* run manually in case printer loading has finished already */
	backend_changed (main_window->backend, main_window);

	set_menu_sensitivities (main_window);

	gtk_window_set_default_size (GTK_WINDOW (main_window->window), 
				     360, 
				     250);
	gtk_widget_show (main_window->window);
}

void
print_manager_show_main_window (PrintManagerBackend *backend)
{
	static MainWindow *main_window = NULL;

	gconf_client = gconf_client_get_default ();

	if (!main_window) {
		main_window = g_new0 (MainWindow, 1);

		main_window->backend = backend;
		g_object_ref (backend);

		create_main_window (main_window);
	}

	gtk_widget_show (main_window->window);
	if (main_window->window->window)
		gdk_window_raise (main_window->window->window);
}

GtkWidget*
append_stock_menuitem (GtkWidget	*menu,
		       const char	*text,
		       GtkAccelGroup	*accel_group,
		       GCallback	callback,
		       GConfClient	*gconf_client, 
		       gpointer		data)
{
	GtkWidget *menu_item, *image;
	GError *err;
	gboolean use_image;

	menu_item = gtk_image_menu_item_new_from_stock (text, accel_group);
	gtk_widget_show (menu_item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

	if (callback) 
		g_signal_connect (G_OBJECT (menu_item), "activate",
				  callback, data);

	image = gtk_image_menu_item_get_image (GTK_IMAGE_MENU_ITEM (menu_item));
   
	use_image = gconf_client_get_bool (gconf_client, MENU_KEY, NULL);
	menuitem_icon_visibility (image, use_image);

	err = NULL;
	gconf_client_notify_add (gconf_client, MENU_KEY,
				 menuitem_icon_visibility_notify, 
				 image, NULL, &err);
	if (err){
		g_printerr (_("There was an error subscribing to notification of menu icon visibility changes: (%s)\n"), err->message);
		g_error_free (err);
	}

	return menu_item;
}

static void
menuitem_icon_visibility_notify (GConfClient *client,
				 guint        cnxn_id,
				 GConfEntry  *entry,
				 gpointer     user_data) 
{
	gboolean use_image;

	use_image = gconf_value_get_bool (entry->value);
	menuitem_icon_visibility (GTK_WIDGET (user_data), use_image);
}

static void 
menuitem_icon_visibility (GtkWidget *image, gboolean use_image)
{
	if (use_image)
		gtk_widget_show (image);
	else
		gtk_widget_hide (image);
}

static void
window_style_event_cb (GtkWidget *w, GtkStyle *prev_style, MainWindow *main_window)
{
	GSList *iter;

	if (main_window->printer_data_list) {
		for (iter = main_window->printer_data_list; iter; iter = iter->next) {
			PrinterData *data = iter->data;
			if (data) {
				data->is_available = FALSE;
				update_icon_for_device (main_window, data, FALSE, TRUE);
			}
		}
        }
}

static void
print_manager_register_stock_icons (GtkIconFactory *factory)
{
	gint i;
	GtkIconSource *source;

	source =  gtk_icon_source_new ();

	for (i = 0; i <  G_N_ELEMENTS (items); ++i) {
		GtkIconSet *icon_set;

		if (!g_file_test (items[i].name, G_FILE_TEST_EXISTS)) {
			g_warning (_("Unable to load printman stock icon '%s'\n"), items[i].name);
			icon_set = gtk_icon_factory_lookup_default (GTK_STOCK_MISSING_IMAGE);
			gtk_icon_factory_add (factory, items[i].stock_id, icon_set);
			continue;
		}
		gtk_icon_source_set_filename (source, items[i].name);

		icon_set = gtk_icon_set_new ();
		gtk_icon_set_add_source (icon_set, source);
		gtk_icon_factory_add (factory, items[i].stock_id, icon_set);
		gtk_icon_set_unref (icon_set);
	}
	gtk_icon_source_free (source);
}

static void
print_manager_init_stock_icons (void)
{
	GtkIconFactory *factory;
	static gboolean initialized = FALSE;

	if (initialized)
		return;
	initialized = TRUE;

	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);
	print_manager_register_stock_icons (factory);

	print_manager_icon_size = gtk_icon_size_register ("print-manager",
							   PRINT_MANAGER_DEFAULT_ICON_SIZE,
							   PRINT_MANAGER_DEFAULT_ICON_SIZE);

	g_object_unref (factory);
}

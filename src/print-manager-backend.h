/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_BACKEND_H
#define PRINT_MANAGER_BACKEND_H 1

#include <glib-object.h>
#include "print-manager-device.h"

#define PRINT_MANAGER_TYPE_BACKEND            (print_manager_backend_get_type ())
#define PRINT_MANAGER_BACKEND(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PRINT_MANAGER_TYPE_BACKEND, PrintManagerBackend))
#define PRINT_MANAGER_BACKEND_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), PRINT_MANAGER_TYPE_BACKEND, PrintManagerBackendClass))
#define PRINT_MANAGER_IS_BACKEND(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PRINT_MANAGER_TYPE_BACKEND))
#define PRINT_MANAGER_IS_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PRINT_MANAGER_TYPE_BACKEND))

typedef struct _PrintManagerBackend PrintManagerBackend;
typedef struct _PrintManagerBackendClass PrintManagerBackendClass;
typedef struct _PrintManagerBackendPrivate PrintManagerBackendPrivate;

struct _PrintManagerBackend {
	GObject object;
};

struct _PrintManagerBackendClass {
	GObjectClass parent_class;

	/* Virtual Methods */
	gint                 (*get_count)                 (PrintManagerBackend *backend);
	PrintManagerDevice  *(*get_device)                (PrintManagerBackend *backend,
							   int                  n);
	gint                 (*get_device_index)          (PrintManagerBackend *backend,
							   PrintManagerDevice  *device);

	/* Update functions */
	void                 (*reload)                    (PrintManagerBackend *backend);

	/* Preferences functions */
	gboolean             (*get_show_all_jobs)         (PrintManagerBackend *backend);
	void                 (*set_show_all_jobs)         (PrintManagerBackend *backend,
							   gboolean             show_all_jobs);
	gboolean             (*get_show_printer_details)  (PrintManagerBackend *backend);
	void                 (*set_show_printer_details)  (PrintManagerBackend *backend,
							   gboolean             show_printer_details);
	gboolean             (*get_show_status_bar)       (PrintManagerBackend *backend);
	void                 (*set_show_status_bar)       (PrintManagerBackend *backend,
							   gboolean             show_status_bar);
	gint                 (*get_update_interval)       (PrintManagerBackend *backend);
	void                 (*set_update_interval)       (PrintManagerBackend *backend,
							   gint                 update_interval);
	gint                 (*get_default_printer)       (PrintManagerBackend *backend);
	void                 (*set_default_printer)       (PrintManagerBackend *backend,
							   gint                 default_printer);

	/* Signals */
	void                 (*changed)                   (PrintManagerBackend *backend);
	void                 (*changes_done)              (PrintManagerBackend *backend);
	void                 (*prefs_changed)             (PrintManagerBackend *backend);
};


/* Standard gobject functions */
GType                print_manager_backend_get_type                  (void);
PrintManagerBackend *print_manager_backend_new                       (void);

/* access functions */
gint                 print_manager_backend_get_count                 (PrintManagerBackend *backend);
PrintManagerDevice  *print_manager_backend_get_device                (PrintManagerBackend *backend,
								      int                  n);
int                  print_manager_backend_get_device_index          (PrintManagerBackend *backend,
								      PrintManagerDevice  *device);

/* Update functions */
void                 print_manager_backend_reload                    (PrintManagerBackend *backend);

/* Preferences functions */
gboolean             print_manager_backend_get_show_all_jobs         (PrintManagerBackend *backend);
void                 print_manager_backend_set_show_all_jobs         (PrintManagerBackend *backend,
								      gboolean             show_all_jobs);
gboolean             print_manager_backend_get_show_printer_details  (PrintManagerBackend *backend);
void                 print_manager_backend_set_show_printer_details  (PrintManagerBackend *backend,
								      gboolean             show_printer_details);
gboolean             print_manager_backend_get_show_status_bar       (PrintManagerBackend *backend);
void                 print_manager_backend_set_show_status_bar       (PrintManagerBackend *backend,
								      gboolean             show_status_bar);
gint                 print_manager_backend_get_update_interval       (PrintManagerBackend *backend);
void                 print_manager_backend_set_update_interval       (PrintManagerBackend *backend,
								      gint                 update_interval);
gint                 print_manager_backend_get_default_printer       (PrintManagerBackend *backend);
void                 print_manager_backend_set_default_printer       (PrintManagerBackend *backend,
								      gint                 default_printer);

/* Send Signal */
void                 print_manager_backend_changed                   (PrintManagerBackend *backend);
void                 print_manager_backend_changes_done              (PrintManagerBackend *backend);
void                 print_manager_backend_prefs_changed             (PrintManagerBackend *backend);

#endif /* ! __PRINT_MANAGER_BACKEND_H__ */

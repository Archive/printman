/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_PREFS_H
#define PRINT_PREFS_H 1

#include <glib.h>

#define PRINTMAN_GCONF_DEFAULTS_PATH SYSCONFDIR "/gconf/gconf.xml.defaults"
#define PRINTMAN_GCONF_DIR           "/apps/gnome-print-manager"
#define PRINTMAN_GCONF_PRINTER_DIR   "/desktop/gnome/peripherals"

typedef enum {
	PRINT_PREF_TYPE_STRING,
	PRINT_PREF_TYPE_BOOLEAN,
	PRINT_PREF_TYPE_INT
} PrintPrefType;

gboolean  print_manager_prefs_is_gconf_admin_mode   (void);
gboolean  print_manager_prefs_set_gconf_admin_path  (gchar         *edit_path);
gboolean  print_manager_prefs_set_gconf             (gchar         *name,
						     gboolean       admin,
						     PrintPrefType  type,
						     gconstpointer  value);
gpointer  print_manager_prefs_get_gconf             (gchar         *name,
						     gboolean       admin,
						     gboolean       use_default,
						     gboolean      *set_ret);

gchar    *print_manager_prefs_get_cde_dt     (const gchar   *objname,
					      gboolean       admin,
					      const gchar   *action,
					      const gchar   *key,
					      gboolean      *set_ret);
gchar    *print_manager_prefs_get_cde_simple (gchar         *filename,
					      gchar         *keyname,
					      gboolean      *set_ret);

#endif /*PRINT_PREFS_H*/

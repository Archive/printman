/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-queue.h"

#include "print-manager-marshal.h"
#include "print-manager-commands.h"
#include "print-prefs.h"

#include <string.h>
#include <stdlib.h>
#include <gnome.h>

enum {
	CHANGED,
	CHANGES_DONE,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0, };
#define CS_CLASS(queue) (G_TYPE_INSTANCE_GET_CLASS \
                           ((queue), \
			    PRINT_MANAGER_TYPE_QUEUE, \
			    PrintManagerQueueClass))

#define PARENT_TYPE (G_TYPE_OBJECT)

/* Static functions */

void
print_manager_queue_changed (PrintManagerQueue *queue)
{
	g_signal_emit (queue, signals[CHANGED], 0);
}

void
print_manager_queue_changes_done (PrintManagerQueue *queue)
{
	g_signal_emit (queue, signals[CHANGES_DONE], 0);
}

static void
print_manager_queue_class_init (PrintManagerQueueClass *klass)
{
	signals [CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerQueueClass,
					       changed),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals [CHANGES_DONE] =
		g_signal_new ("changes_done",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerQueueClass,
					       changes_done),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	klass->get_count         = NULL;
	klass->get_job           = NULL;

	klass->get_queue_name    = NULL;
	klass->get_status        = NULL;
	klass->get_available     = NULL;
	klass->get_device_name   = NULL;
	klass->get_device_status = NULL;
	klass->get_host_name     = NULL;
	klass->get_host_queue    = NULL;
	klass->get_is_remote     = NULL;

	klass->cancel_job        = NULL;
	klass->cancel_jobs       = NULL;

	klass->reload            = NULL;
	klass->get_is_loaded     = NULL;

	klass->changed           = NULL;
	klass->changes_done      = NULL;
}

/**
 * print_manager_queue_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerQueue class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerQueue class.
 **/
GType
print_manager_queue_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerQueueClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) print_manager_queue_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerQueue),
			0,
			(GInstanceInitFunc) NULL,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerQueue",
					       &info, 0);
	}

	return type;
}

/* access functions */
gint
print_manager_queue_get_count (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_count)
		return CS_CLASS (queue)->get_count (queue);
	else
		return 0;
}

PrintJob *
print_manager_queue_get_job (PrintManagerQueue *queue, int n)
{
	if (CS_CLASS (queue)->get_job)
		return CS_CLASS (queue)->get_job (queue, n);
	else
		return NULL;
}

const gchar *
print_manager_queue_get_queue_name (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_queue_name)
		return CS_CLASS (queue)->get_queue_name (queue);
	else
		return NULL;
}

const gchar *
print_manager_queue_get_status (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_status)
		return CS_CLASS (queue)->get_status (queue);
	else
		return NULL;
}

gboolean
print_manager_queue_get_available (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_available)
		return CS_CLASS (queue)->get_available (queue);
	else
		return TRUE;
}

const gchar *
print_manager_queue_get_device_name (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_device_name)
		return CS_CLASS (queue)->get_device_name (queue);
	else
		return NULL;
}

const gchar *
print_manager_queue_get_device_status (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_device_status)
		return CS_CLASS (queue)->get_device_status (queue);
	else
		return NULL;
}

const gchar *
print_manager_queue_get_host_name (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_host_name)
		return CS_CLASS (queue)->get_host_name (queue);
	else
		return NULL;
}

const gchar *
print_manager_queue_get_host_queue (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_host_queue)
		return CS_CLASS (queue)->get_host_queue (queue);
	else
		return NULL;
}

gboolean
print_manager_queue_get_is_remote (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_is_remote)
		return CS_CLASS (queue)->get_is_remote (queue);
	else
		return FALSE;
}

void
print_manager_queue_cancel_job (PrintManagerQueue  *queue,
				PrintJob           *job,
				PrintJobCanceledFn  callback,
				gpointer            user_data)
{
	if (CS_CLASS (queue)->cancel_job)
		CS_CLASS (queue)->cancel_job (queue, 
					      job,
					      callback, 
					      user_data);
}

void
print_manager_queue_cancel_jobs (PrintManagerQueue  *queue,
				 GSList             *jobs,
				 PrintJobCanceledFn  callback,
				 gpointer            user_data)
{
	if (CS_CLASS (queue)->cancel_jobs)
		CS_CLASS (queue)->cancel_jobs (queue, 
					       jobs,
					       callback, 
					       user_data);
}

/* Change functions */
void
print_manager_queue_reload (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->reload)
		CS_CLASS (queue)->reload (queue);
}

gboolean
print_manager_queue_get_is_loaded (PrintManagerQueue *queue)
{
	if (CS_CLASS (queue)->get_is_loaded)
		return CS_CLASS (queue)->get_is_loaded (queue);
	else
		return FALSE;
}

gboolean
print_job_equal (const PrintJob *job1,
		 const PrintJob *job2)
{
	return (!strcmp (job1->job_num, job2->job_num) &&
		job1->user_owned == job2->user_owned &&
		!strcmp (job1->owner, job2->owner) &&
		!strcmp (job1->doc_name, job2->doc_name) &&
		!strcmp (job1->file_size, job2->file_size) &&
		!strcmp (job1->submit_date, job2->submit_date) &&
		!strcmp (job1->queue_name, job2->queue_name));
}

void
print_job_free (PrintJob *job)
{
	g_free (job->queue_name);
	g_free (job->job_num);
	g_free (job->owner);
	g_free (job->doc_name);
	g_free (job->file_size);
	g_free (job->submit_date);
	g_free (job);
}

PrintJob *
print_job_copy (const PrintJob *job)
{
	PrintJob *new_job;

	new_job = g_new (PrintJob, 1);
	new_job->queue_name  = g_strdup (job->queue_name);
	new_job->job_num     = g_strdup (job->job_num);
	new_job->user_owned  = job->user_owned;
	new_job->owner       = g_strdup (job->owner);
	new_job->doc_name    = g_strdup (job->doc_name);
	new_job->file_size   = g_strdup (job->file_size);
	new_job->submit_date = g_strdup (job->submit_date);

	return new_job;
}

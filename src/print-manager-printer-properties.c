/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-printer-properties.h"
#include "print-manager-access.h"

#include <gnome.h>

typedef struct {
	PrintManagerDevice *device;
	PrintManagerQueue  *queue;
	GtkWidget          *dialog;
	GtkWidget          *label; /* GtkEntry */
	GtkWidget          *desc; /* GtkTextView */
	GtkWidget          *icon_path; /* GtkEntry */

	GtkWidget          *queue_name;
	GtkWidget          *status;
	GtkWidget          *device_name;
	GtkWidget          *device_status;
	GtkWidget          *pixmapentry;	
} PrinterProperties;

/*
 * Printer properties dialog.
 */
static void
printer_properties_response_cb (GtkWidget * dialog,
				gint response_id,
				PrinterProperties *properties)
{
	char *chars;
	GtkTextBuffer *buffer;
	GtkTextIter start, end;
	char *text;
	const char *tmp;
	const char *temp_icon_path;

	switch (response_id) {
	case GTK_RESPONSE_OK:
		print_manager_device_set_label (
			properties->device,
			gtk_entry_get_text (GTK_ENTRY (properties->label)));
			text = gtk_editable_get_chars (GTK_EDITABLE (
							properties->icon_path),
							0, -1);
			if (gdk_pixbuf_new_from_file (text, NULL)) {
				print_manager_device_set_icon_path (
							properties->device,
							(const char *)text);
			} 
			else {
				GtkWidget *dialog;
		

				temp_icon_path =  print_manager_device_get_icon_path (properties->device);
				if (temp_icon_path) {
					print_manager_device_set_icon_path (properties->device,
									    temp_icon_path);
					gnome_file_entry_set_filename (
							GNOME_FILE_ENTRY (properties->pixmapentry),
							temp_icon_path);
				}

				dialog = gtk_message_dialog_new (GTK_WINDOW (
						properties->dialog), 
						GTK_DIALOG_MODAL,
						GTK_MESSAGE_WARNING,
						GTK_BUTTONS_CLOSE,
						_("Unsupported image format or corrupted file."));
				g_signal_connect_swapped (dialog, "response",
							  G_CALLBACK (
							  gtk_widget_destroy),
							  dialog);
				gtk_widget_show (dialog);

			}
				
		buffer = 
			gtk_text_view_get_buffer (
				GTK_TEXT_VIEW (properties->desc));

		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter (buffer, &end);

		chars = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
		print_manager_device_set_desc (properties->device, chars);
		g_free (chars);
		g_free (text);

		gtk_widget_hide (properties->dialog);
		break;
	case GTK_RESPONSE_CANCEL:
	case GTK_RESPONSE_DELETE_EVENT:
		gtk_widget_hide (properties->dialog);
		tmp = print_manager_device_get_icon_path (properties->device);
		if (tmp)
			gnome_file_entry_set_filename (
				GNOME_FILE_ENTRY (properties->pixmapentry),
				tmp);
		tmp = NULL;
		tmp = print_manager_device_get_label (properties->device);
		if (tmp)
			gtk_entry_set_text (GTK_ENTRY (properties->label),
							tmp);
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (
							properties->desc));
		gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer),
					  print_manager_device_get_desc (
						properties->device), -1);
		break;
	case GTK_RESPONSE_HELP:
		gnome_help_display ("gnome-print-manager.xml", 
				    "per-printer", 
				    NULL);
		break;
	}
}

static void
queue_changed (PrintManagerQueue *queue, PrinterProperties *properties)
{
	const char *value;

	value = print_manager_queue_get_queue_name (queue);
	if (value)
		gtk_label_set_text (GTK_LABEL (properties->queue_name), value);
	else
		gtk_label_set_text (GTK_LABEL (properties->queue_name), "");

	value = print_manager_queue_get_status (queue);
	if (value)
		gtk_label_set_text (GTK_LABEL (properties->status), value);
	else
		gtk_label_set_text (GTK_LABEL (properties->status), "");

	value = print_manager_queue_get_device_name (queue);
	if (value)
		gtk_label_set_text (GTK_LABEL (properties->device_name), 
				    value);
	else
		gtk_label_set_text (GTK_LABEL (properties->device_name), "");

	value = print_manager_queue_get_device_status (queue);
	if (value)
		gtk_label_set_text (GTK_LABEL (properties->device_status),
				    value);
	else
		gtk_label_set_text (GTK_LABEL (properties->device_status), "");
}

static GtkWidget *
create_printer_properties_dialog (PrintManagerDevice *device,
				  GtkWindow * parent)
{
	GtkWidget *dialog;
	GtkWidget *table1;
	GtkWidget *label1;
	GtkWidget *label2;
	GtkWidget *label3;
	GtkWidget *label5;
	GtkWidget *label6;
	GtkWidget *label7;
	GtkWidget *label8;
	GtkWidget *entry1;
	GtkWidget *scrolledwindow1;
	GtkWidget *text1;
	GtkWidget *pixmapentry1;
	GtkWidget *label9;
	GtkWidget *label10;
	GtkWidget *label11;
	GtkWidget *label12;
	GtkWidget *hseparator1;
	GtkWidget *entry;
	GtkTextBuffer *buffer;

	const char *text;

	PrinterProperties *properties;

	properties = g_new (PrinterProperties, 1);
	properties->device = device;
	g_object_ref (properties->device);

	dialog = gtk_dialog_new_with_buttons (_("Printer Properties"), 
					      parent, 
					      0,
					      GTK_STOCK_CANCEL,
					      GTK_RESPONSE_CANCEL, 
					      GTK_STOCK_OK,
					      GTK_RESPONSE_OK, 
					      GTK_STOCK_HELP,
					      GTK_RESPONSE_HELP, 
					      NULL);

	properties->dialog = dialog;

	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (printer_properties_response_cb),
			  properties);
	/* To prevent dialog destruction when pressing ESC */
	g_signal_connect (dialog,
			  "delete_event",
			  G_CALLBACK (gtk_true), 
			  NULL);

	table1 = gtk_table_new (8, 2, FALSE);
	gtk_widget_show (table1);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), 
			    table1, 
			    TRUE,
			    TRUE, 
			    0);
	gtk_container_set_border_width (GTK_CONTAINER (table1), 8);
	gtk_table_set_row_spacings (GTK_TABLE (table1), 4);
	gtk_table_set_col_spacings (GTK_TABLE (table1), 8);

	label1 = gtk_label_new_with_mnemonic (_("_Label:"));
	gtk_widget_show (label1);
	gtk_table_attach (GTK_TABLE (table1), 
			  label1, 
			  0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label1), 0, 0.5);

	label2 = gtk_label_new_with_mnemonic (_("_Description:"));
	gtk_widget_show (label2);
	gtk_table_attach (GTK_TABLE (table1), 
			  label2, 
			  0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label2), 0, 0.5);

	label3 = gtk_label_new_with_mnemonic (_("_Icon:"));
	gtk_widget_show (label3);
	gtk_table_attach (GTK_TABLE (table1), 
			  label3, 
			  0, 1, 2, 3,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label3), 0, 0.5);

	label5 = gtk_label_new (_("Queue Status:"));
	gtk_widget_show (label5);
	gtk_table_attach (GTK_TABLE (table1), 
			  label5, 
			  0, 1, 5, 6,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label5), 0, 0.5);

	label6 = gtk_label_new (_("Device Name:"));
	gtk_widget_show (label6);
	gtk_table_attach (GTK_TABLE (table1), 
			  label6, 
			  0, 1, 6, 7,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label6), 0, 0.5);

	label7 = gtk_label_new (_("Device Status:"));
	gtk_widget_show (label7);
	gtk_table_attach (GTK_TABLE (table1), 
			  label7, 
			  0, 1, 7, 8,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label7), 0, 0.5);

	label8 = gtk_label_new (_("Printer Queue:"));
	gtk_widget_show (label8);
	gtk_table_attach (GTK_TABLE (table1), 
			  label8, 
			  0, 1, 4, 5,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label8), 0, 0.5);

	entry1 = gtk_entry_new ();
	text = print_manager_device_get_label(device);
	if (text)
		gtk_entry_set_text (GTK_ENTRY (entry1),
				    text);
	gtk_widget_show (entry1);
	gtk_table_attach (GTK_TABLE (table1),
			  entry1,
			  1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0),
			  0, 0);

	properties->label = entry1;

	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow1);
	gtk_table_attach (GTK_TABLE (table1),
			  scrolledwindow1,
			  1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 
			  0, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (
		GTK_SCROLLED_WINDOW (scrolledwindow1),
		GTK_SHADOW_IN);

	text1 = gtk_text_view_new ();
	gtk_widget_show (text1);
	gtk_container_add (GTK_CONTAINER (scrolledwindow1), text1);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text1));
	gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer),
				  print_manager_device_get_desc(device), 
				  -1);

	properties->desc = text1;

	pixmapentry1 = gnome_pixmap_entry_new (NULL, NULL, TRUE);
	gnome_pixmap_entry_set_pixmap_subdir (GNOME_PIXMAP_ENTRY (pixmapentry1),
					      "gnome-print-manager/");
	text = print_manager_device_get_icon_path (device);
	if (text)
		gnome_file_entry_set_filename (GNOME_FILE_ENTRY (pixmapentry1),
					       text);
	gtk_widget_show (pixmapentry1);
	properties->pixmapentry = pixmapentry1;
	gtk_table_attach (GTK_TABLE (table1), 
			  pixmapentry1, 
			  1, 2, 2, 3,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 
			  0, 0);

	label9 = gtk_label_new ("");
	gtk_widget_show (label9);
	gtk_table_attach (GTK_TABLE (table1), 
			  label9, 
			  1, 2, 4, 5,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label9), 0, 0.5);
	properties->queue_name = label9;

	label10 = gtk_label_new ("");
	gtk_widget_show (label10);
	gtk_table_attach (GTK_TABLE (table1), 
			  label10, 
			  1, 2, 5, 6,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label10), 0, 0.5);
	properties->status = label10;

	label11 = gtk_label_new ("");
	gtk_widget_show (label11);
	gtk_table_attach (GTK_TABLE (table1), 
			  label11, 
			  1, 2, 6, 7,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label11), 0, 0.5);
	properties->device_name = label11;

	label12 = gtk_label_new ("");
	gtk_widget_show (label12);
	gtk_table_attach (GTK_TABLE (table1), 
			  label12, 
			  1, 2, 7, 8,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label12), 0, 0.5);
	properties->device_status = label12;

	hseparator1 = gtk_hseparator_new ();
	gtk_widget_show (hseparator1);
	gtk_table_attach (GTK_TABLE (table1), 
			  hseparator1, 
			  0, 2, 3, 4,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
			  0, 4);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label1), entry1);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label2), text1);
	entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (pixmapentry1));
	gtk_label_set_mnemonic_widget (GTK_LABEL (label3), entry);
	properties->icon_path = entry;

	properties->queue = print_manager_device_get_queue (properties->device);
	g_object_ref (properties->queue);
	g_signal_connect (properties->queue, "changed",
			  G_CALLBACK (queue_changed), properties);
	queue_changed (properties->queue, properties);

	_add_atk_relation (entry, 
			   pixmapentry1, 
			   ATK_RELATION_CONTROLLER_FOR,
			   ATK_RELATION_CONTROLLED_BY);

	_add_atk_description (entry, 
			      _("Enter path of the icon to display for "
				"this printer."));

	_add_atk_relation (label5, 
			   properties->queue_name, 
			   ATK_RELATION_LABEL_FOR,
			   ATK_RELATION_LABELLED_BY);

	_add_atk_relation (label6, 
			   properties->status, 
			   ATK_RELATION_LABEL_FOR,
			   ATK_RELATION_LABELLED_BY);

	_add_atk_relation (label7, 
			   properties->device_name, 
			   ATK_RELATION_LABEL_FOR,
			   ATK_RELATION_LABELLED_BY);

	_add_atk_relation (label8, 
			   properties->device_status, 
			   ATK_RELATION_LABEL_FOR,
			   ATK_RELATION_LABELLED_BY);

	return dialog;
}

static GHashTable *
get_dialog_cache (void) 
{
	static GHashTable *dialog_cache = NULL;

	if (dialog_cache == NULL)
		dialog_cache = g_hash_table_new (g_str_hash, g_str_equal);

	return dialog_cache;
}

void
print_manager_show_printer_properties (PrintManagerDevice *device, 
				       GtkWindow          *parent_window)
{
	GtkWidget *dialog;
	GHashTable *dialog_cache;

	dialog_cache = get_dialog_cache();

	dialog = 
		g_hash_table_lookup (
			dialog_cache, 
			print_manager_device_get_queue_name (device));

	if (!dialog) {
		dialog = create_printer_properties_dialog (device, 
							   parent_window);
		g_hash_table_insert (
			dialog_cache, 
			g_strdup (print_manager_device_get_queue_name (device)),
			dialog);
	}

	gtk_widget_show (dialog);
	if (dialog->window)
		gdk_window_raise (dialog->window);
}

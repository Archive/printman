/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_QUEUE_H
#define PRINT_MANAGER_QUEUE_H 1

#include <glib-object.h>

#define PRINT_MANAGER_TYPE_QUEUE            (print_manager_queue_get_type ())
#define PRINT_MANAGER_QUEUE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PRINT_MANAGER_TYPE_QUEUE, PrintManagerQueue))
#define PRINT_MANAGER_QUEUE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), PRINT_MANAGER_TYPE_QUEUE, PrintManagerQueueClass))
#define PRINT_MANAGER_IS_QUEUE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PRINT_MANAGER_TYPE_QUEUE))
#define PRINT_MANAGER_IS_QUEUE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PRINT_MANAGER_TYPE_QUEUE))

typedef struct _PrintManagerQueue PrintManagerQueue;
typedef struct _PrintManagerQueueClass PrintManagerQueueClass;

typedef enum {
	PRINT_JOB_STATUS_NORMAL,
	PRINT_JOB_STATUS_CANCELLING
} PrintJobStatus;

typedef struct {
	gchar          *queue_name;
	gchar          *job_num;
	gboolean       	user_owned;
	gchar          *owner;

	gchar          *doc_name;
	gchar          *file_size;
	gchar          *submit_date;
	PrintJobStatus 	status;
} PrintJob;

typedef void (*PrintJobCanceledFn) (PrintManagerQueue *queue,
				    PrintJob    *job,
				    gboolean     success,
				    gchar       *errstr,
				    gpointer     user_data);

struct _PrintManagerQueue {
	GObject object;
};

struct _PrintManagerQueueClass {
	GObjectClass parent_class;

	/* Virtual Methods */
	gint         (*get_count)          (PrintManagerQueue *queue);
	PrintJob    *(*get_job)            (PrintManagerQueue *queue,
						  int                n);

	const gchar *(*get_queue_name)     (PrintManagerQueue *queue);
	gboolean     (*get_available)      (PrintManagerQueue *queue);
	const gchar *(*get_status)         (PrintManagerQueue *queue);
	const gchar *(*get_device_name)    (PrintManagerQueue *queue);
	const gchar *(*get_device_status)  (PrintManagerQueue *queue);
	const gchar *(*get_host_name)      (PrintManagerQueue *queue);
	const gchar *(*get_host_queue)     (PrintManagerQueue *queue);
	gboolean     (*get_is_remote)      (PrintManagerQueue *queue);

	/* Update functions */
	void         (*reload)             (PrintManagerQueue *queue);
	gboolean     (*get_is_loaded)      (PrintManagerQueue *queue);

	void         (*cancel_job)         (PrintManagerQueue  *queue,
					    PrintJob           *job,
					    PrintJobCanceledFn  callback,
					    gpointer            user_data);
	void         (*cancel_jobs)        (PrintManagerQueue  *queue,
					    GSList             *jobs,
					    PrintJobCanceledFn  callback,
					    gpointer            user_data);

	/* Signals */
	void         (*changed)            (PrintManagerQueue *queue);
	void         (*changes_done)       (PrintManagerQueue *queue);
};

/* Standard gobject functions */
GType        print_manager_queue_get_type           (void);

/* access functions */
gint         print_manager_queue_get_count          (PrintManagerQueue  *queue);
PrintJob    *print_manager_queue_get_job            (PrintManagerQueue  *queue,
						     int                 n);

const gchar *print_manager_queue_get_queue_name     (PrintManagerQueue  *queue);
gboolean     print_manager_queue_get_available      (PrintManagerQueue  *queue);
const gchar *print_manager_queue_get_status         (PrintManagerQueue  *queue);
const gchar *print_manager_queue_get_device_name    (PrintManagerQueue  *queue);
const gchar *print_manager_queue_get_device_status  (PrintManagerQueue  *queue);
const gchar *print_manager_queue_get_host_name      (PrintManagerQueue  *queue);
const gchar *print_manager_queue_get_host_queue     (PrintManagerQueue  *queue);
gboolean     print_manager_queue_get_is_remote      (PrintManagerQueue  *queue);

/* Update functions */
void         print_manager_queue_reload             (PrintManagerQueue  *queue);
gboolean     print_manager_queue_get_is_loaded      (PrintManagerQueue  *queue);

/* Cancel jobs */
void         print_manager_queue_cancel_job         (PrintManagerQueue  *queue,
						     PrintJob           *job,
						     PrintJobCanceledFn  callback,
						     gpointer            user_data);
void         print_manager_queue_cancel_jobs        (PrintManagerQueue  *queue,
						     GSList             *jobs,
						     PrintJobCanceledFn  callback,
						     gpointer            user_data);

/* Send Signal */
void         print_manager_queue_changed            (PrintManagerQueue  *queue);
void         print_manager_queue_changes_done       (PrintManagerQueue  *queue);


/* Job functions */
gboolean     print_job_equal                        (const PrintJob     *job1,
						     const PrintJob     *job2);
void         print_job_free                         (PrintJob           *job);
PrintJob    *print_job_copy                         (const PrintJob     *job);


#endif /* ! __PRINT_MANAGER_QUEUE_H__ */

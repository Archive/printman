/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-queue-test.h"

#include "print-manager-commands.h"
#include "print-prefs.h"

#include <string.h>
#include <stdlib.h>
#include <gnome.h>
#include <libxml/parser.h>

#define PARENT_TYPE (PRINT_MANAGER_TYPE_QUEUE)

/* Static functions */

struct _PrintManagerQueueTestPrivate {
	gchar       *directory;

	gchar       *queue_name;

	gchar       *status;
	gboolean     available;
	gchar       *device_name;
	gchar       *device_status;
	gchar       *host_name;
	gchar       *host_queue;

	gboolean     is_remote;

	gint         count;
	PrintJob   **jobs;
};

/* access functions */
static gint
pmqt_get_count (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	g_return_val_if_fail (pmqt != NULL, -1);

	return pmqt->priv->count;
}

static PrintJob *
pmqt_get_job (PrintManagerQueue *pmq, int n)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	g_return_val_if_fail (pmqt != NULL, NULL);
	g_return_val_if_fail (n >= 0, NULL);
	g_return_val_if_fail (n < pmqt->priv->count, NULL);

	return pmqt->priv->jobs[n];
}


static const gchar *
pmqt_get_queue_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->queue_name;
}

static const gchar *
pmqt_get_status (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->status;
}

static gboolean
pmqt_get_available (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->available;
}

static const gchar *
pmqt_get_device_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->device_name;
}

static const gchar *
pmqt_get_device_status (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->device_status;
}

static const gchar *
pmqt_get_host_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->host_name;
}

static const gchar *
pmqt_get_host_queue (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->host_queue;
}

static gboolean
pmqt_get_is_remote (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);

	return pmqt->priv->is_remote;
}

static char *
xml_get_prop (xmlNode *node, char *prop)
{
	char *temp, *ret_val;

	temp = xmlGetProp (node, prop);
	ret_val = g_strdup (temp);
	xmlFree (temp);

	return ret_val;
}

/* Change functions */
static void
pmqt_reload (PrintManagerQueue *pmq)
{
	PrintManagerQueueTest *pmqt = PRINT_MANAGER_QUEUE_TEST (pmq);
	xmlDoc *doc;
	xmlNode *root, *node;
	char *filename;
	char *temp;
	int i, j, count, new_count;
	PrintJob **new_jobs;

	if (!pmqt->priv->directory || !pmqt->priv->queue_name)
		return;

	filename = g_strconcat (pmqt->priv->directory,
				"queues/",
				pmqt->priv->queue_name,
				".xml",
				NULL);

	doc = xmlParseFile (filename);

	if (!doc)
		return;

	root = xmlDocGetRootElement (doc);

	if (!root)
		return;

	g_free (pmqt->priv->status);
	g_free (pmqt->priv->device_name);
	g_free (pmqt->priv->device_status);
	g_free (pmqt->priv->host_name);

	pmqt->priv->status = xml_get_prop (root, "status");
	if (pmqt->priv->status == NULL)
		pmqt->priv->status = g_strdup ("");

	temp = xmlGetProp (root, "available");
	if (temp)
		pmqt->priv->available = !strcasecmp (temp, "true") ||
			!strcasecmp (temp, "t");
	else
		pmqt->priv->available = TRUE;
	xmlFree (temp);

	pmqt->priv->device_name = xml_get_prop (root, "device-name");
	if (pmqt->priv->device_name == NULL)
		pmqt->priv->device_name = g_strdup ("");

	pmqt->priv->device_status = xml_get_prop (root, "device-status");
	if (pmqt->priv->device_status == NULL)
		pmqt->priv->device_status = g_strdup ("");

	pmqt->priv->host_name = xml_get_prop (root, "host-name");
	if (pmqt->priv->host_name == NULL)
		pmqt->priv->host_name = g_strdup ("");

	pmqt->priv->host_queue = temp = xmlGetProp (root, "host-queue");
	if (pmqt->priv->host_queue == NULL)
		pmqt->priv->host_name = g_strdup ("");

	temp = xmlGetProp (root, "is-remote");
	if (temp)
		pmqt->priv->is_remote = !strcasecmp (temp, "true") ||
			!strcasecmp (temp, "t");
	else
		pmqt->priv->is_remote = FALSE;
	xmlFree (temp);

	new_count = 0;

	for (node = root->xmlChildrenNode; node; node = node->next) {
		if (!xmlNodeIsText (node)) {
			new_count ++;
		}
	}

	i = 0;

	count = pmqt->priv->count;

	new_jobs = g_new (PrintJob *, new_count);

	for (node = root->xmlChildrenNode; node; node = node->next) {
		if (!xmlNodeIsText (node)) {
			PrintJob *job = g_new (PrintJob, 1);

			job->queue_name = g_strdup (pmqt->priv->queue_name);
			job->job_num = xml_get_prop (node, "job-num");
			job->owner = xml_get_prop (node, "owner");
			temp = xml_get_prop (node, "user-owned");
			if (temp)
				job->user_owned =
					!strcasecmp (temp, "true") ||
					!strcasecmp (temp, "t");
			else
				job->user_owned = FALSE;

			job->doc_name = xml_get_prop (node, "doc-name");
			job->file_size = xml_get_prop (node, "file-size");
			job->submit_date = 
				g_strconcat (xmlGetProp (node, "submit-time"),
					     ", ",
					     xmlGetProp (node, "submit-date"),
					     NULL);

			for (j = 0; j < count; j++) {
				if (pmqt->priv->jobs[j] &&
				    print_job_equal (job,
						     pmqt->priv->jobs[j])) {
					print_job_free (job);
					job = pmqt->priv->jobs[j];
					pmqt->priv->jobs[j] = NULL;
					break;
				}
			}

			new_jobs[i] = job;

			i++;
		}
	}

	for (j = 0; j < count; j++) {
		if (pmqt->priv->jobs[j])
			print_job_free (pmqt->priv->jobs[j]);
	}
	g_free (pmqt->priv->jobs);

	pmqt->priv->count = new_count;
	pmqt->priv->jobs = new_jobs;

	print_manager_queue_changed (pmq);
	print_manager_queue_changes_done (pmq);
}

static gboolean
pmqt_get_is_loaded (PrintManagerQueue *pmq)
{
	return TRUE;
}

static void
pmqt_class_init (PrintManagerQueueTestClass *klass)
{
	PrintManagerQueueClass *queue_class = PRINT_MANAGER_QUEUE_CLASS (klass);

	queue_class->get_count         = pmqt_get_count;
	queue_class->get_job           = pmqt_get_job;

	queue_class->get_queue_name    = pmqt_get_queue_name;
	queue_class->get_status        = pmqt_get_status;
	queue_class->get_available     = pmqt_get_available;
	queue_class->get_device_name   = pmqt_get_device_name;
	queue_class->get_device_status = pmqt_get_device_status;
	queue_class->get_host_name     = pmqt_get_host_name;
	queue_class->get_host_queue    = pmqt_get_host_queue;
	queue_class->get_is_remote     = pmqt_get_is_remote;

	queue_class->reload            = pmqt_reload;
	queue_class->get_is_loaded     = pmqt_get_is_loaded;
}

static void
pmqt_init (PrintManagerQueueTest *queue)
{
	queue->priv = g_new (PrintManagerQueueTestPrivate, 1);
 
	queue->priv->queue_name        = NULL;
	queue->priv->status            = NULL;
	queue->priv->device_name       = NULL;
	queue->priv->device_status     = NULL;
	queue->priv->host_name         = NULL;
	queue->priv->host_queue        = NULL;

	queue->priv->is_remote         = FALSE;

	queue->priv->count             = 0;
	queue->priv->jobs              = NULL;
}

/**
 * print_manager_queue_test_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerQueueTest class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerQueueTest class.
 **/
GType
print_manager_queue_test_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerQueueTestClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pmqt_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerQueueTest),
			0,
			(GInstanceInitFunc) pmqt_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerQueueTest",
					       &info, 0);
	}

	return type;
}

PrintManagerQueue *
print_manager_queue_test_new (const char *directory, const char *queue_name)
{
	PrintManagerQueueTest *queue = PRINT_MANAGER_QUEUE_TEST
		(g_type_create_instance (PRINT_MANAGER_TYPE_QUEUE_TEST));

	queue->priv->directory = g_strdup (directory);
	queue->priv->queue_name = g_strdup (queue_name);

	print_manager_queue_reload (PRINT_MANAGER_QUEUE (queue));

	return PRINT_MANAGER_QUEUE (queue);
}


void
print_manager_queue_test_print_file (PrintManagerQueueTest *pmqt, 
				     const char            *filename)
{
	static int job_num = 1;
	char string[100];
	time_t time_as_timet;
	struct tm *current_time;
	PrintJob *job;

	pmqt->priv->count ++;
	pmqt->priv->jobs = g_renew (PrintJob *, 
				    pmqt->priv->jobs, 
				    pmqt->priv->count);
	job = g_new (PrintJob, 1);
	pmqt->priv->jobs[pmqt->priv->count - 1] = job;

	job->queue_name = g_strdup (pmqt->priv->queue_name);
	job->job_num = g_strdup_printf ("%d", job_num++);
	job->owner = g_strdup (g_get_user_name ());
	job->user_owned = TRUE;
	job->doc_name = g_strdup (filename);
	if (g_file_test (filename, G_FILE_TEST_EXISTS))
		job->file_size = g_strdup (">0");
	else
		job->file_size = g_strdup ("0");

	time_as_timet = time(NULL);

	current_time = localtime (& time_as_timet);

	strftime (string, 100, "%T, %b %e", current_time);
	job->submit_date = g_strdup (string);
	print_manager_queue_changed (PRINT_MANAGER_QUEUE (pmqt));
}

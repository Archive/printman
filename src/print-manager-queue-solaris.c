/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-queue-solaris.h"

#include "print-manager-commands.h"
#include "print-prefs.h"

#include <string.h>
#include <stdlib.h>
#include <gnome.h>

#define PARENT_TYPE (PRINT_MANAGER_TYPE_QUEUE)

/* Static functions */

typedef struct _PrintStamp {
	gchar *req_id;
	gchar *timestamp;
} PrintStamp;

struct _PrintManagerQueueSolarisPrivate {
	gchar       *queue_name;
	gboolean     available;
	gchar       *status;
	gchar       *device_name;
	gchar       *device_status;
	gchar       *host_name;
	gchar       *host_queue;

	gboolean     new_available;
	gchar       *new_status;
	gchar       *new_device_name;
	gchar       *new_device_status;
	gchar       *new_host_name;
	gchar       *new_host_queue;

	gint         steps;

	gint         count;
	PrintJob   **jobs;

	gint         new_count;
	gint         time_count;
	PrintJob   **new_jobs;
	PrintStamp **new_stamp;

	guint	     is_remote : 1;
	guint	     new_is_remote : 1;

	guint        in_reload : 1;
	guint        is_loaded : 1;
	int          reload_cnt;
	GSList       *src_buf;
};

typedef enum {
        BSD_FORMAT,
        LPRNG_FORMAT,
        UNKNOWN_FORMAT,
} JobFormat;

static JobFormat
get_server_output_format (PrintManagerQueueSolaris *queue)
{
	int i = 0;
	GSList *iter;
	gchar *token;
	GSList *src_buf = queue->priv->src_buf;

	if (!src_buf)
		return UNKNOWN_FORMAT;

	iter = src_buf;
	for (i = 0; i < 10; i++) {

		if (!iter)
			break;	

		token = iter->data;

		/* 
		 * First check if its a BSD format. 
		 * On linux we get "Printer:" for any output 
		 */
		if ((token = strchr (token, ':')) 
		     && (token = strchr (token, '[')) 
		     && (token = strchr (token, ']'))) { 
			/* BSD printing protocol output */
      			return BSD_FORMAT;
    		}
		iter = iter->next;
	}

	iter = src_buf;
	token = iter->data;

	/* Determine if its LPRng format */
	if (strncmp (token, "Printer:", 8) == 0) {
		return LPRNG_FORMAT;
	} 

	return UNKNOWN_FORMAT;
}

/* Ok here we fill up the jobs information for the queue */
static void
fill_jobs_BSD (PrintManagerQueueSolaris *queue)
{
	char *token, *buf;
	char owner[100];
	char job_num [50]; 
	char file_size[50];
	char doc_name[200];
	char *tmp;
	char *tmp1;
	PrintJob *job;
	GSList *iter;
	GSList *src_buf = queue->priv->src_buf;

	if (!src_buf)
		return;

	for (iter = src_buf; iter; iter = iter->next) {

		token = iter->data;

		if ((token = strchr (token, ':')) 
			&& (token = strchr (token, '[')) 
			&& (token = strchr (token, ']'))) { 
			/* BSD printing protocol output */

			buf = iter->data;
			tmp = strchr (buf, ':');
			if (!tmp)
				continue;
			*tmp = '\0';
			g_strlcpy (owner, buf, sizeof(owner));
			g_strchug (owner); /* Remove leading spaces */
			
			/* Get the job number */	
			tmp++;
			tmp1 = strchr (tmp, '[');
			if (!tmp1)
				continue;
			tmp = strchr (tmp1, ' ');
			if (!tmp)
				continue;
			tmp++;
			tmp1 = strrchr (tmp, ' ');
			if (!tmp1)
				continue;
			*tmp1 = '\0'; 
			g_strlcpy (job_num, tmp, sizeof(job_num));

			/* Read the next line */
			iter = iter->next;
			if (!iter)
				break;

			buf = iter->data;

			/* Make sure we dont have any traling spaces */
			g_strchomp (buf);

			tmp = strrchr (buf, ' ');
			if (!tmp)
				continue;
			*tmp = '\0';

			/* Now remove any trailing spaces*/
			tmp = g_strchomp (buf);
			tmp = strrchr (buf, ' ');

			if (!tmp)
				continue;

			g_strlcpy (file_size, tmp, sizeof(file_size));
			g_strchug (file_size); /* Remove leading spaces */
			*tmp = '\0';

			/* Ok the rest of the string is assumed to be the doc*/
			buf = g_strstrip (buf);
			g_strlcpy (doc_name, buf, sizeof(doc_name));

			job = g_new0 (PrintJob, 1);
			job->queue_name = g_strdup (queue->priv->queue_name);
			job->doc_name = g_strdup (doc_name);
			job->owner = g_strdup (owner);
			job->user_owned = !strcmp (job->owner, g_get_user_name ());
			job->submit_date = g_strdup ("Unknown");
			job->job_num = g_strdup (job_num);
			job->status = PRINT_JOB_STATUS_NORMAL;
			job->file_size = g_strdup (file_size);
			queue->priv->new_jobs = g_renew (PrintJob *,
							 queue->priv->new_jobs,
							 queue->priv->new_count + 1);

			queue->priv->new_jobs [queue->priv->new_count] = job;
			queue->priv->new_count++;
	
		}

	}
	return;
}

static void
fill_jobs_LPRng (PrintManagerQueueSolaris *queue)
{
	
	char owner[100];
	char job_num [50]; 
	char file_size [50];
	char doc_name [200];
	char submit_date [50];
	PrintJob *job;
	GSList *iter;
	GSList *src_buf = queue->priv->src_buf;
	char *buf;

	if (!queue->priv->src_buf)
		return;

	for (iter = src_buf; iter; iter = iter->next) {
		char *tmp1, *tmp2;

		buf = iter->data;
		if (strncmp (buf, "Printer:", 8) == 0)
			continue;

		if (*buf == ' ') /* Header information ignore it */
			continue;

		/* active snm@rh80-build+829 A 829 lprng.out  388 17:40:23 */

		buf = strchr (buf, ' ');
		if (!buf)
			continue;

		tmp1 = strchr (buf, '+');
		if (!tmp1)
			continue;
		*tmp1 = '\0';

		g_strlcpy (owner, buf, sizeof(owner));
		g_strchug (owner); /* Remove leading spaces */

		/* Now come from behind to get the size and time */
		/* Remember that buf points to the char after '+' */
		tmp1++;
		buf = tmp1;

		/* Make sure we dont have any trailing spaces */
		g_strchomp (buf);
		tmp2 = strrchr (buf, ' ');
		if (!tmp2)
			continue;

		g_strlcpy (submit_date, tmp2, sizeof(submit_date));
		g_strchug (submit_date); /* Remove any leading spaces */

		*tmp2 = '\0';
		/* Remove any trailing spaces */
		g_strchomp (buf);

		tmp2 = strrchr (buf, ' ');
		if (!tmp2)
			continue;

		g_strlcpy (file_size, tmp2, sizeof(file_size));
		g_strchug (file_size); /* Remove any leading spaces */
		*tmp2 = '\0';

		/* Get the job number and document name */
		tmp1 = strchr (buf, ' ');
		if (!tmp1)
			continue;

		g_strchug(tmp1);
		tmp2 = strchr (tmp1, ' ');
		if (!tmp2)
			continue;
		g_strchug(tmp2);
		tmp1 = strchr (tmp2, ' ');
		*tmp1 = '\0';
		g_strlcpy (job_num, tmp2, sizeof(job_num));

		tmp1++;

		g_strlcpy (doc_name, tmp1, sizeof(doc_name));

		g_strstrip (doc_name); /* Remove leading/trailing spaces */

		job = g_new0 (PrintJob, 1);
		job->queue_name = g_strdup (queue->priv->queue_name);
		job->doc_name = g_strdup (doc_name);
		job->owner = g_strdup (owner);
		job->user_owned = !strcmp (job->owner, g_get_user_name ());
		job->submit_date = g_strdup (submit_date);
		job->job_num = g_strdup (job_num);
		job->status = PRINT_JOB_STATUS_NORMAL;
		job->file_size = g_strdup (file_size);
		
		queue->priv->new_jobs = g_renew (PrintJob *,
						 queue->priv->new_jobs,
						 queue->priv->new_count + 1);

		queue->priv->new_jobs [queue->priv->new_count] = job;
		queue->priv->new_count++;

	}

}

static void free_timestamp (PrintManagerQueueSolaris *queue)
{
	int i;
	int count = queue->priv->time_count;
		
	for (i = 0; i < count; i++) {
		g_free (queue->priv->new_stamp[i]->req_id);	
		g_free (queue->priv->new_stamp[i]->timestamp);	
		g_free (queue->priv->new_stamp[i]);
	}
	queue->priv->time_count = 0;
	queue->priv->new_stamp = NULL;	
}

static void
merge_timestamp (PrintManagerQueueSolaris *queue)
{
	int count = queue->priv->count;
	int time_count = queue->priv->time_count;
	int i;
	int j;

	for (i = 0; i < count; i++) {
		for (j = 0; j < time_count; j++) {
			if (strcmp (queue->priv->jobs[i]->job_num, queue->priv->new_stamp[j]->req_id) == 0) {
			g_free (queue->priv->jobs[i]->submit_date);	
			queue->priv->jobs[i]->submit_date = g_strdup (queue->priv->new_stamp[j]->timestamp);
			break;
			}
		}
	}
}

static void
merge_jobs (PrintManagerQueueSolaris *queue)
{
	int new_count = queue->priv->new_count;
	int count = queue->priv->count;
	int i;
	int j;
	for (i = 0; i < new_count; i++) {
		for (j = 0; j < count; j++) {
			if (queue->priv->jobs[j] &&
			    print_job_equal (queue->priv->new_jobs[i],
					     queue->priv->jobs[j])) {
				print_job_free (queue->priv->new_jobs[i]);
				queue->priv->new_jobs[i] = queue->priv->jobs[j];
				queue->priv->jobs[j] = NULL;
				break;
			}
		}
	}
	for (j = 0; j < count; j++) {
		if (queue->priv->jobs[j])
			print_job_free (queue->priv->jobs[j]);
	}
	g_free (queue->priv->jobs);
	queue->priv->jobs = queue->priv->new_jobs;
	queue->priv->count = queue->priv->new_count;
	queue->priv->new_jobs = NULL;
	queue->priv->new_count = 0;
}

static void
one_step_finished (PrintManagerQueueSolaris *queue)
{
	queue->priv->steps --;
	if (queue->priv->steps == 0) {
		g_free (queue->priv->status);
		g_free (queue->priv->device_name);
		g_free (queue->priv->device_status);
		g_free (queue->priv->host_name);
		g_free (queue->priv->host_queue);

		queue->priv->status        = queue->priv->new_status;
		queue->priv->available     = queue->priv->new_available;
		queue->priv->device_name   = queue->priv->new_device_name;
		queue->priv->device_status = queue->priv->new_device_status;
		queue->priv->host_name     = queue->priv->new_host_name;
		queue->priv->host_queue    = queue->priv->new_host_queue;

		queue->priv->is_remote     = queue->priv->new_is_remote;

		queue->priv->new_status        = NULL;
		queue->priv->new_available     = TRUE;
		queue->priv->new_device_name   = NULL;
		queue->priv->new_device_status = NULL;
		queue->priv->new_host_name     = NULL;
		queue->priv->new_host_queue    = NULL;

		queue->priv->new_is_remote     = FALSE;

		merge_jobs (queue);
		merge_timestamp (queue);
		free_timestamp (queue);

		queue->priv->is_loaded = TRUE;

		print_manager_queue_changed (PRINT_MANAGER_QUEUE (queue));
		print_manager_queue_changes_done (PRINT_MANAGER_QUEUE (queue));

		queue->priv->in_reload = FALSE;
		if (queue->priv->reload_cnt > 0) {
			queue->priv->reload_cnt--;
			print_manager_queue_reload (
				PRINT_MANAGER_QUEUE (queue));
		}
	}
}

static void 
list_timestamp_output (gint       lpstat_id,
			gchar     *line,
			gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;
	PrintStamp *stamp;
	char timestamp[100], reqid[20];
	char month[20], date[20], time[20];
	char *temp;
	char *temp1;

	/* Get the timestamp traversing from behind */ 	
	temp1 = strrchr(line, ':');
	if (!temp1)
		return;
	temp = temp1+3;
	*temp = '\0';

	g_strlcpy (timestamp, temp1-9, sizeof(timestamp)); 	

	temp1 = strchr (line, ' ');
	if (!temp1)
		return;
	*temp1 = '\0';

	temp = strrchr (line, '-');
	if (!temp)
		return;
	temp++;
	g_strlcpy(reqid, temp, sizeof(reqid));
  	
	stamp = g_new0 (PrintStamp, 1);
	stamp->req_id = g_strdup (reqid);
	stamp->timestamp = g_strdup (timestamp);

	queue->priv->new_stamp = g_renew (PrintStamp *,
					 queue->priv->new_stamp,
					 queue->priv->time_count + 1);

	queue->priv->new_stamp [queue->priv->time_count] = stamp;
	queue->priv->time_count++;
}

static void 
list_jobs_output (gint       lpstat_id,
		  gchar      *src_buf,
		  gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	if (src_buf) {
		queue->priv->src_buf = g_slist_prepend (queue->priv->src_buf, g_strdup (src_buf));
	}
}

static void
free_src_buffer (PrintManagerQueueSolaris *queue)
{
	GSList *iter;

	for (iter = queue->priv->src_buf; iter; iter = iter->next) {
		gchar *str = iter->data;

		g_free (str);
	}

	g_slist_free (queue->priv->src_buf);
	queue->priv->src_buf = NULL;
}

static void
list_jobs_end (gint       lpstat_id,
	       gboolean   cancelled,
	       gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	queue->priv->src_buf = g_slist_reverse (queue->priv->src_buf);

	switch (get_server_output_format (queue)) { 
		case BSD_FORMAT:
			fill_jobs_BSD (queue);
			break;
		case LPRNG_FORMAT:
			fill_jobs_LPRng (queue);
			break;
	        case UNKNOWN_FORMAT:
		default:
			break;	
        }

	free_src_buffer (queue);
	one_step_finished (queue);

}

static void
list_timestamp_end (gint       lpstat_id,
	       gboolean   cancelled,
	       gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	one_step_finished (queue);

}

static void
list_jobs (PrintManagerQueueSolaris *queue)
{

	if (queue->priv->src_buf)
		free_src_buffer (queue);

	if (queue->priv->new_is_remote) {
		gchar *command_args [] = { queue->priv->new_host_name, 
					   queue->priv->new_host_queue, 
					   NULL };
		lpstat_run (list_jobs_output,
			    NULL,
			    list_jobs_end,
			    queue,
			    LPSTAT_LIST_REMOTE_JOBS,
			    command_args);
	} else {
		gchar *command_args [] = { queue->priv->queue_name, NULL };

		lpstat_run (list_jobs_output,
			    NULL,
			    list_jobs_end,
			    queue,
			    LPSTAT_LIST_LOCAL_JOBS,
			    command_args);

		lpstat_run (list_timestamp_output,
			    NULL,
			    list_timestamp_end,
			    queue,
			    LPSTAT_LIST_TIMESTAMP,
			    command_args);
	}
}

static void
get_queue_status_output (gint       lpstat_id,
			 gchar     *line,
			 gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	if (strlen (line)) {
		queue->priv->new_status = g_strdup (_("Not Active"));
		queue->priv->new_available = FALSE;
	}
}

static void
get_device_status_output (gint       lpstat_id,
			  gchar     *line,
			  gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	if (strlen (line)) {
		queue->priv->new_device_status = g_strdup (_("Not Active"));
		queue->priv->new_available = FALSE;
	}
}

static void
get_queue_or_device_status_end (gint       lpstat_id,
				gboolean   cancelled,
				gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	one_step_finished (queue);
}

static void
get_queue_status (PrintManagerQueueSolaris *queue)
{
	gchar *command_args[] = { queue->priv->queue_name, NULL };

	lpstat_run (get_queue_status_output,
		    NULL,
		    get_queue_or_device_status_end,
		    queue,
		    LPSTAT_GET_QUEUE_STATUS,
		    command_args);

	queue->priv->new_status = g_strdup (_("Active"));

	if (!queue->priv->new_is_remote) {
		lpstat_run (get_device_status_output, 
			    NULL, 
			    get_queue_or_device_status_end,
			    queue, 
			    LPSTAT_GET_DEVICE_STATUS,
			    command_args);
		queue->priv->new_device_status = g_strdup (_("Active"));
	}
}

static void 
get_queue_output (gint       lpstat_id,
		  gchar     *line,
		  gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;
	gchar **split;

	split = g_strsplit (line, ":", 0);
	if (!split || !split [0])
		return;

	if (split [0] && strlen (split [0])) {
		queue->priv->new_device_name = g_strdup (split [0]);
		queue->priv->new_is_remote = FALSE;
	} 
	else if (split [1] && strlen (split [1])) {
		queue->priv->new_host_name = g_strdup (split [1]);
		queue->priv->new_host_queue = 
			g_strdup (strlen (split [2]) ? 
				  	split [2] : 
				  	queue->priv->queue_name);

		queue->priv->new_is_remote = TRUE;
	}

	g_strfreev (split);
}

static void
get_queue_end (gint       lpstat_id,
	       gboolean   cancelled,
	       gpointer   user_data)
{
	PrintManagerQueueSolaris *queue = user_data;

	if (queue->priv->new_is_remote)
		queue->priv->steps = 2;
	else
		queue->priv->steps = 4;

	get_queue_status (queue);
	list_jobs (queue);
}

typedef struct {
	PrintManagerQueue  *queue;
	PrintJob           *job;
	PrintJobCanceledFn  callback;
	gpointer           *user_data;
	gchar              *errstr;
} JobCancelData;

static void
job_cancel_error_output (gint      lpstat_id,
			 gchar    *line,
			 gpointer  user_data)
{
	JobCancelData *data = user_data;

	if (data->errstr)
		data->errstr = g_strconcat (data->errstr, line, NULL);
	else
		data->errstr = g_strdup (line);
}

static void
job_cancel_output (gint      lpstat_id,
		   gchar    *line,
		   gpointer  user_data)
{
	gchar *colon;

	colon = strrchr (line, ':');
	if (colon)
		line = colon + 1;

	if (strstr (line, "cancelled"))
		return;

	job_cancel_error_output (lpstat_id, line, user_data);
}

static void
job_cancel_end (gint      lpstat_id,
		gboolean  cancelled,
		gpointer  user_data)
{
	JobCancelData *data = user_data;
	PrintManagerQueueSolaris *pmqs;
	gboolean success = data->errstr ? FALSE : TRUE;

	pmqs = PRINT_MANAGER_QUEUE_SOLARIS (data->queue);

	if (data->callback)
		(*data->callback) (data->queue,
				   data->job, 
				   success,
				   data->errstr, 
				   data->user_data);

	if (success) {
		PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (data->queue);
		int count = pmqs->priv->count;
		int i;

		for (i = 0; i < count; i++) {
			if (print_job_equal (data->job, pmqs->priv->jobs[i])) {
				print_job_free (pmqs->priv->jobs[i]);
				memmove (pmqs->priv->jobs + i, pmqs->priv->jobs + i + 1, (count - i - 1) * sizeof (PrintJob *));
				pmqs->priv->count --;
				break;
			}
		}
	} else {
		data->job->status = PRINT_JOB_STATUS_NORMAL;
	}

	if (pmqs->priv->is_loaded) {
		print_manager_queue_changed (data->queue);
		print_manager_queue_changes_done (data->queue);
	}

	g_free (data->errstr);
	g_free (data);
}

/* access functions */
static gint
pmqs_get_count (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	g_return_val_if_fail (pmqs != NULL, -1);

	return pmqs->priv->count;
}

static PrintJob *
pmqs_get_job (PrintManagerQueue *pmq, int n)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	g_return_val_if_fail (pmqs != NULL, NULL);
	g_return_val_if_fail (n >= 0, NULL);
	g_return_val_if_fail (n < pmqs->priv->count, NULL);

	return pmqs->priv->jobs[n];
}


static const gchar *
pmqs_get_queue_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->queue_name;
}

static const gchar *
pmqs_get_status (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->status;
}

static gboolean
pmqs_get_available (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->available;
}

static const gchar *
pmqs_get_device_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->device_name;
}

static const gchar *
pmqs_get_device_status (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->device_status;
}

static const gchar *
pmqs_get_host_name (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->host_name;
}

static const gchar *
pmqs_get_host_queue (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->host_queue;
}

static gboolean
pmqs_get_is_remote (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->is_remote;
}

static void
pmqs_cancel_job (PrintManagerQueue  *pmq,
		 PrintJob           *job,
		 PrintJobCanceledFn  callback,
		 gpointer            user_data)
{
	JobCancelData *data;

	g_return_if_fail (job != NULL);

	if (job->status == PRINT_JOB_STATUS_CANCELLING) {
		if (callback)
			(*callback) (pmq,
				     job,
				     FALSE,
				     _("Already Cancelling"),
				     user_data);
		return;
	}

	data = g_new0 (JobCancelData, 1);
	data->queue = pmq;
	data->job = job;
	data->callback = callback;
	data->user_data = user_data;

	{ 
		gchar *command_args[] = { job->queue_name, job->job_num };

		lpstat_run (job_cancel_output,
			    job_cancel_error_output,
			    job_cancel_end,
			    data,
			    LPSTAT_CANCEL_JOB,
			    command_args);

		job->status = PRINT_JOB_STATUS_CANCELLING;
		if (PRINT_MANAGER_QUEUE_SOLARIS (pmq)->priv->is_loaded) {
			print_manager_queue_changed (pmq);
			print_manager_queue_changes_done (pmq);
		}
	}
}

typedef struct {
	GSList             *list;
	GSList             *current;
	PrintJobCanceledFn  callback;
	gpointer            user_data;
} JobCancelListData;

static void
cancel_jobs_internal (PrintManagerQueue *queue,
		      PrintJob    *job,
		      gboolean     success,
		      gchar       *errstr,
		      gpointer     user_data)
{
	JobCancelListData *data = user_data;

	(*data->callback) (queue, job, success, errstr, data->user_data);

	data->current = data->current->next;

	if (!data->current) {
		g_slist_free (data->list);
		g_free (data);
	} 
	else {
		PrintJob *next_job = data->current->data;

		pmqs_cancel_job (queue,
				 next_job,
				 cancel_jobs_internal,
				 data);
	}
}

static void
pmqs_cancel_jobs (PrintManagerQueue  *pmq,
		  GSList             *jobs,
		  PrintJobCanceledFn  callback,
		  gpointer            user_data)
{
	JobCancelListData *data;
	PrintJob *first_job;

	g_return_if_fail (jobs != NULL);

	data = g_new0 (JobCancelListData, 1);
	data->list      = g_slist_copy (jobs);
	data->current   = data->list;
	data->callback  = callback;
	data->user_data = user_data;

	first_job = data->current->data;

	pmqs_cancel_job (pmq, 
			 first_job, 
			 cancel_jobs_internal, 
			 data);
}

/* Change functions */
static void
pmqs_reload (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	if (!pmqs->priv->in_reload) {
		gchar *command_args[] = { pmqs->priv->queue_name, NULL };

		pmqs->priv->new_count = 0;
		pmqs->priv->new_jobs = NULL;

		lpstat_run (get_queue_output,
			    NULL,
			    get_queue_end,
			    pmqs,
			    LPSTAT_GET_QUEUE_ATTRS,
			    command_args);
		pmqs->priv->in_reload = TRUE;
	} else
		pmqs->priv->reload_cnt++;
}

static gboolean
pmqs_get_is_loaded (PrintManagerQueue *pmq)
{
	PrintManagerQueueSolaris *pmqs = PRINT_MANAGER_QUEUE_SOLARIS (pmq);

	return pmqs->priv->is_loaded;
}

static void
pmqs_class_init (PrintManagerQueueSolarisClass *klass)
{
	PrintManagerQueueClass *queue_class = PRINT_MANAGER_QUEUE_CLASS (klass);

	queue_class->get_count         = pmqs_get_count;
	queue_class->get_job           = pmqs_get_job;

	queue_class->get_queue_name    = pmqs_get_queue_name;
	queue_class->get_status        = pmqs_get_status;
	queue_class->get_available     = pmqs_get_available;
	queue_class->get_device_name   = pmqs_get_device_name;
	queue_class->get_device_status = pmqs_get_device_status;
	queue_class->get_host_name     = pmqs_get_host_name;
	queue_class->get_host_queue    = pmqs_get_host_queue;
	queue_class->get_is_remote     = pmqs_get_is_remote;

	queue_class->cancel_job        = pmqs_cancel_job;
	queue_class->cancel_jobs       = pmqs_cancel_jobs;

	queue_class->reload            = pmqs_reload;
	queue_class->get_is_loaded     = pmqs_get_is_loaded;
}

static void
pmqs_init (PrintManagerQueueSolaris *queue)
{
	queue->priv = g_new (PrintManagerQueueSolarisPrivate, 1);
 
	queue->priv->queue_name        = NULL;
	queue->priv->available         = TRUE;
	queue->priv->status            = NULL;
	queue->priv->device_name       = NULL;
	queue->priv->device_status     = NULL;
	queue->priv->host_name         = NULL;
	queue->priv->host_queue        = NULL;

	queue->priv->is_remote         = FALSE;
	
	queue->priv->new_available     = TRUE;
	queue->priv->new_status        = NULL;
	queue->priv->new_device_name   = NULL;
	queue->priv->new_device_status = NULL;
	queue->priv->new_host_name     = NULL;
	queue->priv->new_host_queue    = NULL;

	queue->priv->new_is_remote     = FALSE;

	queue->priv->count             = 0;
	queue->priv->jobs              = NULL;

	queue->priv->new_count         = 0;
	queue->priv->new_jobs          = NULL;

	queue->priv->time_count        = 0;
	queue->priv->new_stamp         = NULL;

	queue->priv->in_reload         = FALSE;
	queue->priv->is_loaded         = FALSE;
	queue->priv->reload_cnt        = 0;

	queue->priv->src_buf           = NULL;
}

/**
 * print_manager_queue_solaris_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerQueueSolaris class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerQueueSolaris class.
 **/
GType
print_manager_queue_solaris_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerQueueSolarisClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pmqs_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerQueueSolaris),
			0,
			(GInstanceInitFunc) pmqs_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerQueueSolaris",
					       &info, 0);
	}

	return type;
}

PrintManagerQueue *
print_manager_queue_solaris_new (char *name)
{
	PrintManagerQueueSolaris *queue = 
		PRINT_MANAGER_QUEUE_SOLARIS (
			g_type_create_instance (
				PRINT_MANAGER_TYPE_QUEUE_SOLARIS));

	queue->priv->queue_name = g_strdup (name);

	return PRINT_MANAGER_QUEUE (queue);
}

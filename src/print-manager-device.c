/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-device.h"
#include "print-manager-marshal.h"

enum {
	CHANGED,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0, };
#define CS_CLASS(device) (G_TYPE_INSTANCE_GET_CLASS \
                           ((device), \
			    PRINT_MANAGER_TYPE_DEVICE, \
			    PrintManagerDeviceClass))

#define PARENT_TYPE (G_TYPE_OBJECT)

static void
print_manager_device_class_init (PrintManagerDeviceClass *klass)
{
	signals [CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerDeviceClass,
					       changed),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	klass->get_is_visible = NULL;
	klass->set_is_visible = NULL;

	klass->get_is_default = NULL;

	klass->get_label = NULL;
	klass->set_label = NULL;

	klass->get_desc = NULL;
	klass->set_desc = NULL;

	klass->get_queue_name = NULL;
	klass->set_queue_name = NULL;

	klass->get_icon_path = NULL;
	klass->set_icon_path = NULL;

	klass->get_queue = NULL;

	klass->print_file = NULL;

	klass->reload = NULL;
	klass->save = NULL;

	klass->changed = NULL;
}

/**
 * print_manager_device_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerDevice class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerDevice class.
 **/
GType
print_manager_device_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerDeviceClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) print_manager_device_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerDevice),
			0,
			(GInstanceInitFunc) NULL,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerDevice",
					       &info, 0);
	}

	return type;
}

/* access functions */
gboolean
print_manager_device_get_is_visible (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_is_visible)
		return CS_CLASS (printer)->get_is_visible (printer);
	else
		return FALSE;
}

void
print_manager_device_set_is_visible (PrintManagerDevice *printer,
				     gboolean is_visible)
{
	if (CS_CLASS (printer)->set_is_visible)
		CS_CLASS (printer)->set_is_visible (printer, is_visible);
}

gboolean
print_manager_device_get_is_default (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_is_default)
		return CS_CLASS (printer)->get_is_default (printer);
	else
		return FALSE;
}

const char *
print_manager_device_get_label (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_label)
		return CS_CLASS (printer)->get_label (printer);
	else
		return NULL;
}

void
print_manager_device_set_label (PrintManagerDevice *printer,
				const char *label)
{
	if (CS_CLASS (printer)->set_label)
		CS_CLASS (printer)->set_label (printer, label);
}

const char *
print_manager_device_get_desc (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_desc)
		return CS_CLASS (printer)->get_desc (printer);
	else
		return NULL;
}

void print_manager_device_set_desc (PrintManagerDevice *printer,
				    const char *desc)
{
	if (CS_CLASS (printer)->set_desc)
		CS_CLASS (printer)->set_desc (printer, desc);
}

const char *
print_manager_device_get_queue_name (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_queue_name)
		return CS_CLASS (printer)->get_queue_name (printer);
	else
		return NULL;
}

void
print_manager_device_set_queue_name (PrintManagerDevice *printer,
				     const char *queue_name)
{
	if (CS_CLASS (printer)->set_queue_name)
		CS_CLASS (printer)->set_queue_name (printer, queue_name);
}


const char *
print_manager_device_get_icon_path (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_icon_path)
		return CS_CLASS (printer)->get_icon_path (printer);
	else
		return NULL;
}

void
print_manager_device_set_icon_path (PrintManagerDevice *printer,
			    const char *icon_path)
{
	if (CS_CLASS (printer)->set_icon_path)
		CS_CLASS (printer)->set_icon_path (printer, icon_path);
}

/* Change functions */
void
print_manager_device_reload (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->reload)
		CS_CLASS (printer)->reload (printer);
}


void
print_manager_device_save (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->save)
		CS_CLASS (printer)->save (printer);
}

PrintManagerQueue *
print_manager_device_get_queue (PrintManagerDevice *printer)
{
	if (CS_CLASS (printer)->get_queue)
		return CS_CLASS (printer)->get_queue (printer);
	else
		return NULL;
}

void
print_manager_device_print_file (PrintManagerDevice *printer,
				 const gchar        *filename,
				 PrintFileDoneFn     done_cb,
				 gpointer            user_data)
{
	if (CS_CLASS (printer)->print_file)
		CS_CLASS (printer)->print_file (printer, 
						filename, 
						done_cb, 
						user_data);
}


void
print_manager_device_changed (PrintManagerDevice *device)
{
	g_signal_emit (device, signals[CHANGED], 0);
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_MAIN_WINDOW_H
#define PRINT_MANAGER_MAIN_WINDOW_H 1

#define PRINT_MANAGER_DEFAULT_ICON_SIZE	63

#define	PRINT_MANAGER_DEFAULT		"printer-default"
#define	PRINT_MANAGER_BROKEN		"printer-broken"
#define	PRINT_MANAGER_INKJET		"printer-inkjet"
#define	PRINT_MANAGER_PERSONAL_LASER	"printer-personal-laser"
#define	PRINT_MANAGER_WORKGROUP_LASER	"printer-workgroup-laser"

#include <gnome.h>
#include <gconf/gconf-client.h>
#include "print-manager-backend.h"

void print_manager_show_main_window (PrintManagerBackend *backend);

GtkWidget *append_stock_menuitem  (GtkWidget       *menu,
				   const char       *text,
				   GtkAccelGroup    *accel_group,
				   GCallback        callback,
				   GConfClient      *gconf_client,
				   gpointer         data);

#endif /* ! __PRINT_MANAGER_MAIN_WINDOW_H__ */

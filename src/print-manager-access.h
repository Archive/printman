/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PRINT_MANAGER_ACCESS_H__
#define __PRINT_MANAGER_ACCESS_H__

#include <glib-object.h>
#include <atk/atkobject.h>
#include <atk/atkregistry.h>
#include <atk/atkrelationset.h>
#include <atk/atkobjectfactory.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtklabel.h>

void _add_atk_name_desc   (GtkWidget       *widget,
			   gchar           *name,
			   gchar           *desc);
void _add_atk_description (GtkWidget       *widget,
			   gchar           *desc);
void _add_atk_relation    (GtkWidget       *widget1,
			   GtkWidget       *widget2,
			   AtkRelationType  w1_to_w2,
			   AtkRelationType  w2_to_w1);

#endif /* ! __PRINT_MANAGER_ACCESS_H__ */

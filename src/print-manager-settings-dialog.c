/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-settings-dialog.h"

#include <gnome.h>

typedef struct {
	PrintManagerBackend *backend;

	GtkWidget           *dialog;

	GtkAdjustment       *adjustment_interval;
	GtkWidget           *onlymine;
	GtkWidget           *everyone;
} Settings;

/*
 * Settings dialog.
 */
static void
settings_response_cb (GtkWidget *dialog,
		      gint       response_id,
		      Settings  *settings)
{
	switch (response_id) {
	case GTK_RESPONSE_HELP:
		gnome_help_display ("gnome-print-manager.xml", 
				    "general-prefs", 
				    NULL);
		break;
	case GTK_RESPONSE_CLOSE:
		gtk_widget_hide (settings->dialog);
		break;
	}
}

static void
radio_button_toggled_cb (GtkWidget *radio, gpointer data)
{
	Settings  *settings = data;
	print_manager_backend_set_show_all_jobs (
			settings->backend,
			gtk_toggle_button_get_active (
			GTK_TOGGLE_BUTTON (settings->everyone)));
}

static void
adjustment_value_changed_cb (GtkWidget *radio, gpointer data)
{
	Settings  *settings = data;
	print_manager_backend_set_update_interval (
			settings->backend,
			settings->adjustment_interval->value);
}
	
static GtkWidget *
create_settings_dialog (Settings *settings, GtkWindow * parent)
{
	GtkWidget *dialog;
	GtkWidget *table2;
	GtkWidget *label13;
	GtkWidget *label14;
	GtkWidget *hscale1;
	GtkWidget *alignment1;
	GtkWidget *hbox1;
	GtkWidget *radiobutton1;
	GtkWidget *radiobutton2;
	GtkAdjustment *adjustment_interval;

	dialog = gtk_dialog_new_with_buttons (_("Preferences"), 
					      parent, 
					      0,
					      GTK_STOCK_CLOSE,
					      GTK_RESPONSE_CLOSE,
					      GTK_STOCK_HELP,
					      GTK_RESPONSE_HELP,
					      NULL);

	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (settings_response_cb), 
			  settings);
	g_signal_connect (dialog, 
			  "delete-event",
			  G_CALLBACK (gtk_widget_hide_on_delete), 
			  NULL);

	table2 = gtk_table_new (2, 2, FALSE);
	gtk_widget_show (table2);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), 
			    table2, 
			    TRUE,
			    TRUE, 
			    8);
	gtk_container_set_border_width (GTK_CONTAINER (table2), 8);
	gtk_table_set_row_spacings (GTK_TABLE (table2), 4);
	gtk_table_set_col_spacings (GTK_TABLE (table2), 8);

	label13 = gtk_label_new_with_mnemonic (_("Documents to Show:"));
	gtk_widget_show (label13);
	gtk_table_attach (GTK_TABLE (table2), 
			  label13, 
			  0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label13), 0, 0.5);

	label14 =
		gtk_label_new_with_mnemonic (_("_Update Interval in Seconds:"));
	gtk_widget_show (label14);
	gtk_table_attach (GTK_TABLE (table2), 
			  label14, 
			  0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL), 
			  (GtkAttachOptions) (0),
			  0, 0);
	gtk_misc_set_alignment (GTK_MISC (label14), 0, 0.5);

	adjustment_interval = 
		GTK_ADJUSTMENT (gtk_adjustment_new (30, 5, 300, 1, 10, 10));
	hscale1 = gtk_hscale_new (adjustment_interval);
	settings->adjustment_interval = adjustment_interval;

	gtk_scale_set_digits (GTK_SCALE (hscale1), 0);
	gtk_widget_show (hscale1);
	gtk_table_attach (GTK_TABLE (table2), 
			  hscale1, 
			  1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 
			  0, 0);

	alignment1 = gtk_alignment_new (0, 0.5, 0, 1);
	gtk_widget_show (alignment1);
	gtk_table_attach (GTK_TABLE (table2), 
			  alignment1, 
			  1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 
			  0, 0);

	hbox1 = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (hbox1);
	gtk_container_add (GTK_CONTAINER (alignment1), hbox1);

	radiobutton1 = gtk_radio_button_new_with_mnemonic (NULL,
							   _("Only _Mine"));
	gtk_widget_show (radiobutton1);
	gtk_box_pack_start (GTK_BOX (hbox1), radiobutton1, FALSE, FALSE, 0);

	radiobutton2 = 
		gtk_radio_button_new_with_mnemonic_from_widget (
			GTK_RADIO_BUTTON (radiobutton1),
			_("_Everyone's"));
	gtk_widget_show (radiobutton2);
	gtk_box_pack_start (GTK_BOX (hbox1), radiobutton2, FALSE, FALSE, 0);

	settings->onlymine = radiobutton1;
	settings->everyone = radiobutton2;
	
	g_signal_connect (G_OBJECT (settings->everyone),
		          "toggled",
			  (GCallback) radio_button_toggled_cb,
			   settings);
	
	g_signal_connect (G_OBJECT (settings->adjustment_interval),
		          "value_changed",
			  (GCallback) adjustment_value_changed_cb,
			   settings);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label13), radiobutton1);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label14), hscale1);

	return dialog;
}

static void
get_settings (Settings *settings)
{
	settings->adjustment_interval->value =
		print_manager_backend_get_update_interval (settings->backend);
	gtk_adjustment_value_changed (settings->adjustment_interval);

	if (print_manager_backend_get_show_all_jobs (settings->backend))
		gtk_toggle_button_set_active (
			GTK_TOGGLE_BUTTON (settings->everyone), 
			TRUE);
	else
		gtk_toggle_button_set_active (
			GTK_TOGGLE_BUTTON (settings->onlymine), 
			TRUE);
}

void
print_manager_show_settings_dialog (PrintManagerBackend *backend,
				    GtkWindow           *parent_window)
{
	static Settings *settings = NULL;

	if (!settings) {
		settings = g_new (Settings, 1);

		settings->backend = backend;
		g_object_ref (backend);

		settings->adjustment_interval = NULL;
		settings->onlymine            = NULL;
		settings->everyone            = NULL;

		settings->dialog = 
			create_settings_dialog (settings, parent_window);
	}

	get_settings (settings);

	gtk_widget_show (settings->dialog);
	if (settings->dialog->window)
		gdk_window_raise (settings->dialog->window);
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <gnome.h>

#include "print-manager-printer-selection.h"
#include "print-manager-backend.h"
#include "print-manager-access.h"

#define d(x)

static GtkWidget *printer_selection_dialog = NULL;

typedef struct {
	PrintManagerBackend *backend;
	GtkListStore        *store;
	gboolean             changes_made;
} PrinterSelection;

static PrinterSelection *printer_selection = NULL;


/*
 * Printer selection dialog.
 */
static void
printer_selection_row_activated_cb (GtkWidget   *widget, 
				    GtkTreePath *path,
				    int          column, 
				    gpointer     data)
{
#if 0
	GtkTreeModel *model;
	GtkTreeIter iter;
	GValue value = { 0, };
	gboolean active;

	g_print ("In printer_selection_row_activated_cb\n");

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	if (!gtk_tree_model_get_iter (model, &iter, path)) {
		g_warning ("Tree path not found!");
		return;
	}

	gtk_tree_model_get_value (model, &iter, 0, &value);
	if (!G_VALUE_HOLDS_BOOLEAN (&value)) {
		g_warning ("Value isn't BOOLEAN!");
		return;
	}

	active = g_value_get_boolean (&value);

	gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, !active, -1);
	printer_selection->changes_made = TRUE;

	g_value_unset (&value);
#endif
}

static void
printer_selection_toggled_cb (GtkWidget * widget, gchar * path, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GValue value = { 0, };
	gboolean active;

	d(g_print ("In printer_selection_toggled_cb\n"));

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (data));

	if (!gtk_tree_model_get_iter_from_string (model, &iter, path)) {
		g_warning ("Tree path not found!");
		return;
	}

	gtk_tree_model_get_value (model, &iter, 0, &value);
	if (!G_VALUE_HOLDS_BOOLEAN (&value)) {
		g_warning ("Value isn't BOOLEAN!");
		return;
	}

	active = g_value_get_boolean (&value);

	gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, !active, -1);
	printer_selection->changes_made = TRUE;

	g_value_unset (&value);
}


static void
printer_selection_response_cb (GtkWidget * dialog,
			       gint response_id,
			       PrinterSelection *printer_selection)
{
	GtkTreeIter iter;
	switch (response_id) {
	case GTK_RESPONSE_OK:
		gtk_widget_hide (printer_selection_dialog);

		if (!printer_selection->changes_made)
			break;

		if (gtk_tree_model_get_iter_first (
			    GTK_TREE_MODEL (printer_selection->store), 
			    &iter)) {
			while (1) {
				gboolean active;
				PrintManagerDevice *pd;
				gtk_tree_model_get (
					GTK_TREE_MODEL (
						printer_selection->store),
					&iter,
					0, &active,
					2, &pd,
					-1);

				if (print_manager_device_get_is_visible (pd) != active)
				{
					print_manager_device_set_is_visible (pd, 
									     active);
				}

				if (!gtk_tree_model_iter_next (
				             GTK_TREE_MODEL (
					            printer_selection->store), 
					     &iter))
					break;
			}
		}
		break;
	case GTK_RESPONSE_CANCEL:
		gtk_widget_hide (printer_selection_dialog);
		break;
	case GTK_RESPONSE_HELP:
		gnome_help_display ("gnome-print-manager.xml", 
				    "printman-display", 
				    NULL);
		break;
	}
}

static void
show_all (GtkWidget *button, PrinterSelection *printer_selection)
{
	GtkTreeIter iter;
	if (gtk_tree_model_get_iter_first (
		    GTK_TREE_MODEL (printer_selection->store), 
		    &iter)) {
		while (1) {
			gtk_list_store_set (printer_selection->store,
					    &iter,
					    0, TRUE,
					    -1);

			if (!gtk_tree_model_iter_next (
			             GTK_TREE_MODEL (printer_selection->store),
				     &iter))
				break;
		}
		printer_selection->changes_made = TRUE;
	}
}

static void
hide_all (GtkWidget *button, PrinterSelection *printer_selection)
{
	GtkTreeIter iter;
	if (gtk_tree_model_get_iter_first (
	           GTK_TREE_MODEL (printer_selection->store), 
		   &iter)) {
		while (1) {
			gtk_list_store_set (printer_selection->store,
					    &iter,
					    0, FALSE,
					    -1);

			if (!gtk_tree_model_iter_next (
				     GTK_TREE_MODEL (printer_selection->store),
				     &iter))
				break;
		}
		printer_selection->changes_made = TRUE;
	}
}


static GtkWidget *
create_printer_selection_dialog (GtkWindow           *parent,
				 PrintManagerBackend *backend)
{
	GtkWidget *dialog, *treeview;
	GtkWidget *scrolledwin, *hbox, *button;
	GtkListStore *store;
	GtkTreeViewColumn *col;
	GtkCellRenderer *rend;

	dialog = gtk_dialog_new_with_buttons (_("Select Printers to Show"),
					      parent, 
					      0, 
					      GTK_STOCK_HELP,
					      GTK_RESPONSE_HELP, 
					      GTK_STOCK_CANCEL,
					      GTK_RESPONSE_CANCEL, 
					      GTK_STOCK_OK,
					      GTK_RESPONSE_OK, 
					      NULL);
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (printer_selection_response_cb),
			  printer_selection);
	g_signal_connect (dialog,
			  "delete_event",
			  G_CALLBACK (gtk_widget_hide_on_delete), 
			  NULL);

	scrolledwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwin);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwin),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwin),
					     GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), scrolledwin,
			    TRUE, TRUE, 0);

	store = gtk_list_store_new (3, 
				    G_TYPE_BOOLEAN, 
				    G_TYPE_STRING, 
				    G_TYPE_POINTER);
	printer_selection->store = store;

	treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	g_object_unref (G_OBJECT (store));
	gtk_widget_show (treeview);
	gtk_container_add (GTK_CONTAINER (scrolledwin), treeview);
	g_signal_connect (treeview, 
			  "row_activated",
			  G_CALLBACK (printer_selection_row_activated_cb),
			  NULL);
#if 0
	g_signal_connect (treeview,
			  "button_press_event",
			  G_CALLBACK (list_button_press_event_cb), 
			  backend);
#endif

	_add_atk_name_desc (treeview, 
			    _("Select Printer Visibility"),
			    _("Lists all printers and lets you choose "
			      "which are shown"));

	rend = gtk_cell_renderer_toggle_new ();
	col = gtk_tree_view_column_new_with_attributes ("", 
							rend, 
							"active", 
							0,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), col);
	g_signal_connect (rend,
			  "toggled",
			  G_CALLBACK (printer_selection_toggled_cb),
			  treeview);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Printer Name"),
							rend, 
							"text", 
							1,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), col);


	hbox = gtk_hbox_new (TRUE, 4);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), 
			    hbox, 
			    FALSE,
			    FALSE, 
			    4);

	button = gtk_button_new_with_mnemonic (_("Show _All"));
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 10);
	g_signal_connect (button, "clicked",
			  G_CALLBACK (show_all), printer_selection);

	_add_atk_relation (button, 
			   treeview,
			   ATK_RELATION_CONTROLLER_FOR,
			   ATK_RELATION_CONTROLLED_BY);

	button = gtk_button_new_with_mnemonic (_("_Hide All"));
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 10);
	g_signal_connect (button, "clicked",
			  G_CALLBACK (hide_all), printer_selection);

	_add_atk_relation (button, 
			   treeview,
			   ATK_RELATION_CONTROLLER_FOR,
			   ATK_RELATION_CONTROLLED_BY);

	gtk_window_set_default_size (GTK_WINDOW (dialog), 300, 300);

	return dialog;
}

static gint
printer_name_compare (gconstpointer p1, gconstpointer p2) 
{
	PrintManagerDevice *printer1 = (PrintManagerDevice *) p1;
	PrintManagerDevice *printer2 = (PrintManagerDevice *) p2;

	return strcmp (print_manager_device_get_queue_name (printer1),
			       print_manager_device_get_queue_name (printer2));
}

static void
get_settings (void)
{
	GtkTreeIter iter;
	int i, count;
	GSList *sort_list = NULL, *sort_iter;

	gtk_list_store_clear (printer_selection->store);

	count = print_manager_backend_get_count (printer_selection->backend);
	for (i = 0; i < count; i++) {
		PrintManagerDevice *pd;
		pd =
			print_manager_backend_get_device (
				printer_selection->backend, i);
		sort_list = g_slist_insert_sorted (sort_list, 
						   pd, 
						   printer_name_compare);
	}
	for (sort_iter = sort_list; sort_iter; sort_iter = sort_iter->next) {
		PrintManagerDevice *pd = sort_iter->data;
		gchar *label;

		gtk_list_store_append (printer_selection->store, &iter);

		label = g_strconcat (print_manager_device_get_label (pd),
				     " - ",
				     print_manager_device_get_desc (pd),
				     NULL);
		gtk_list_store_set (printer_selection->store, 
				    &iter,
				    0, print_manager_device_get_is_visible (pd),
				    1, label,
				    2, pd,
				    -1);
		g_free (label);
	}
	g_slist_free (sort_list);
}

void
print_manager_show_printer_selection (PrintManagerBackend *backend, 
				      GtkWindow           *parent_window)
{
	if (!printer_selection_dialog) {
		printer_selection = g_new0 (PrinterSelection, 1);
		printer_selection->backend = backend;
		printer_selection->store = NULL;
		printer_selection->changes_made = FALSE;

		printer_selection_dialog =
			create_printer_selection_dialog (parent_window,
							 backend);
	}

	get_settings ();

	gtk_widget_show (printer_selection_dialog);
	if (printer_selection_dialog->window)
		gdk_window_raise (printer_selection_dialog->window);
}

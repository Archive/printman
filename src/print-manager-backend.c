/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * FIXME:
 *  - Don't use indexes for devices
 *  - Handle multiple backends (print_manager_backend_new)
 */

#include <config.h>

#include "print-manager-backend.h"
#include "print-manager-backend-solaris.h"
#include "print-manager-backend-test.h"
#include "print-manager-marshal.h"
#include "print-manager-commands.h"
#include "print-prefs.h"

#include <stdlib.h>

enum {
	CHANGED,
	CHANGES_DONE,
	PREFS_CHANGED,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0, };
#define CS_CLASS(backend) (G_TYPE_INSTANCE_GET_CLASS \
                           ((backend), \
			    PRINT_MANAGER_BACKEND_TYPE, \
			    PrintManagerBackendClass))

#define PARENT_TYPE (G_TYPE_OBJECT)

void
print_manager_backend_changed (PrintManagerBackend *backend)
{
	g_signal_emit (backend, signals[CHANGED], 0);
}

void
print_manager_backend_changes_done (PrintManagerBackend *backend)
{
	g_signal_emit (backend, signals[CHANGES_DONE], 0);
}

void
print_manager_backend_prefs_changed (PrintManagerBackend *backend)
{
	g_signal_emit (backend, signals[PREFS_CHANGED], 0);
}

static void
print_manager_backend_class_init (PrintManagerBackendClass *klass)
{
	signals [CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerBackendClass,
					       changed),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals [CHANGES_DONE] =
		g_signal_new ("changes_done",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerBackendClass,
					       changes_done),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals [PREFS_CHANGED] =
		g_signal_new ("prefs_changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (PrintManagerBackendClass,
					       prefs_changed),
			      NULL, NULL,
			      print_manager_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	klass->get_count = NULL;
	klass->get_device = NULL;
	klass->get_device_index = NULL;

        klass->reload = NULL;

        klass->get_show_all_jobs = NULL;
        klass->set_show_all_jobs = NULL;

        klass->get_show_printer_details = NULL;
        klass->set_show_printer_details = NULL;

        klass->get_show_status_bar = NULL;
        klass->set_show_status_bar = NULL;

        klass->get_update_interval = NULL;
        klass->set_update_interval = NULL;

        klass->get_default_printer = NULL;
        klass->set_default_printer = NULL;

	klass->changed = NULL;
	klass->changes_done = NULL;
	klass->prefs_changed = NULL;
}

/**
 * print_manager_backend_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerBackend class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerBackend class.
 **/
GType
print_manager_backend_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerBackendClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) print_manager_backend_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerBackend),
			0,
			(GInstanceInitFunc) NULL,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerBackend",
					       &info, 0);
	}

	return type;
}

PrintManagerBackend *
print_manager_backend_new (void)
{
	char *filename = getenv ("GNOME_PRINT_MANAGER_TEST");

	if (filename)
		return print_manager_backend_test_new (filename);
	else
		return print_manager_backend_solaris_new ();
}

/* access functions */
gint
print_manager_backend_get_count (PrintManagerBackend *backend)
{
	g_return_val_if_fail (backend != NULL, -1);

	if (CS_CLASS (backend)->get_count)
		return CS_CLASS (backend)->get_count (backend);
	else
		return -1;
}

PrintManagerDevice *
print_manager_backend_get_device (PrintManagerBackend *backend, int n)
{
	g_return_val_if_fail (backend != NULL, NULL);

	if (CS_CLASS (backend)->get_device)
		return CS_CLASS (backend)->get_device (backend, n);
	else
		return NULL;
}

int
print_manager_backend_get_device_index (PrintManagerBackend *backend, 
					PrintManagerDevice  *device)
{
	g_return_val_if_fail (backend != NULL, -1);

	if (CS_CLASS (backend)->get_device_index)
		return CS_CLASS (backend)->get_device_index (backend, device);
	else {
		int n;
		for (n = print_manager_backend_get_count (backend) - 1;
		     n >= 0; n--) {
			PrintManagerDevice *backend_device;
			
			backend_device = 
				print_manager_backend_get_device (backend, n);
			
			if (backend_device == device) {
				return n;
			}
		}
		return -1;
	}
}

/* Change functions */
void
print_manager_backend_reload (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->reload)
		CS_CLASS (backend)->reload (backend);
}

gboolean
print_manager_backend_get_show_all_jobs (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->get_show_all_jobs)
		return CS_CLASS (backend)->get_show_all_jobs (backend);
	else
		return FALSE;
}

void
print_manager_backend_set_show_all_jobs (PrintManagerBackend *backend,
					 gboolean show_all_jobs)
{
	if (CS_CLASS (backend)->set_show_all_jobs)
		CS_CLASS (backend)->set_show_all_jobs (backend, show_all_jobs);
}

gboolean
print_manager_backend_get_show_printer_details (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->get_show_printer_details)
		return CS_CLASS (backend)->get_show_printer_details (backend);
	else
		return FALSE;
}

void
print_manager_backend_set_show_printer_details  (PrintManagerBackend *backend,
						 gboolean show_printer_details)
{
	if (CS_CLASS (backend)->set_show_printer_details)
		CS_CLASS (backend)->set_show_printer_details (
					    backend, 
					    show_printer_details);
}

gboolean
print_manager_backend_get_show_status_bar (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->get_show_status_bar)
		return CS_CLASS (backend)->get_show_status_bar (backend);
	else
		return FALSE;	
}

void
print_manager_backend_set_show_status_bar (PrintManagerBackend *backend,
					   gboolean show_status_bar)
{
	if (CS_CLASS (backend)->set_show_status_bar)
		CS_CLASS (backend)->set_show_status_bar (backend, 
							 show_status_bar);
}

gint
print_manager_backend_get_update_interval (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->get_update_interval)
		return CS_CLASS (backend)->get_update_interval (backend);
	else
		return -1;
}

void
print_manager_backend_set_update_interval (PrintManagerBackend *backend,
					   gint update_interval)
{
	if (CS_CLASS (backend)->set_update_interval)
		CS_CLASS (backend)->set_update_interval (backend, 
							 update_interval);
}

gint
print_manager_backend_get_default_printer (PrintManagerBackend *backend)
{
	if (CS_CLASS (backend)->get_default_printer)
		return CS_CLASS (backend)->get_default_printer (backend);
	else
		return -1;
}

void
print_manager_backend_set_default_printer (PrintManagerBackend *backend,
					   gint default_printer)
{
	if (CS_CLASS (backend)->set_default_printer)
		CS_CLASS (backend)->set_default_printer (backend, 
							 default_printer);
}

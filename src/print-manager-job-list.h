/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_JOB_LIST_H
#define PRINT_MANAGER_JOB_LIST_H 1

#include <gnome.h>
#include "print-manager-device.h"
#include "print-manager-queue.h"
#include "print-manager-backend.h"

void print_manager_show_job_list (PrintManagerBackend *backend,
				  PrintManagerDevice *device,
				  gboolean is_main,
				  PrintJob *selected_job);

/* Returns a list of queue names of job list windows that are currently open.
   The list should be freed, but not the queue names. */
GList* print_manager_get_open_job_lists (void);

#endif /* ! __PRINT_MANAGER_JOB_LIST_H__ */

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-device-test.h"
#include "print-manager-queue-test.h"
#include "print-prefs.h"

enum {
	CHANGED,
	LAST_SIGNAL
};

#define PARENT_TYPE (PRINT_MANAGER_TYPE_DEVICE)

struct _PrintManagerDeviceTestPrivate {
	gchar      *directory;

	gchar      *label;
	gchar      *desc;
	gchar      *icon_path;

	gchar      *queue_name;

	PrintManagerQueue *queue;

	guint       is_visible : 1;
	guint       is_default : 1;
};

/* Static functions */

static gchar *
pmdt_get_gconf_key_name (PrintManagerDeviceTest *pmdt,
			 gchar *lastkey)
{
	return g_strjoin ("/",
			  "printers",
			  pmdt->priv->queue_name,
			  lastkey,
			  NULL);
}

static gpointer
pmdt_get_gconf (PrintManagerDeviceTest *pmdt,
		char                   *key,
		gboolean                admin,
		gboolean                def,
		gboolean               *set_ret)
{
	char *fullkey;
	gpointer ret_val;

	fullkey = pmdt_get_gconf_key_name (pmdt, key);
	ret_val = print_manager_prefs_get_gconf (fullkey, admin, def, set_ret);
	g_free (fullkey);

	return ret_val;
}

static gboolean
pmdt_set_gconf (PrintManagerDeviceTest *pmdt,
		char                   *key,
		PrintPrefType           type,
		gconstpointer           value)
{
	char *fullkey;
	gboolean ret_val;
	gboolean admin = print_manager_prefs_is_gconf_admin_mode ();

	fullkey = pmdt_get_gconf_key_name (pmdt, key);
	ret_val = print_manager_prefs_set_gconf (fullkey, admin, type, value);
	g_free (fullkey);

	return ret_val;
}

static gboolean
reload_settings_from_gconf (PrintManagerDeviceTest *pmdt, gboolean admin, gboolean def)
{
	gboolean set;
	gboolean is_visible =
		GPOINTER_TO_INT (pmdt_get_gconf
				 (pmdt, "visible", admin, def, &set));
	if (set) {
		pmdt->priv->is_visible = is_visible;
		g_free (pmdt->priv->label);
		g_free (pmdt->priv->desc);
		g_free (pmdt->priv->icon_path);

		pmdt->priv->label = 
			pmdt_get_gconf (pmdt,
					"label",
					admin, def, NULL);
		pmdt->priv->desc = 
			pmdt_get_gconf (pmdt,
					"description",
					admin, def, NULL);
		pmdt->priv->icon_path = 
			pmdt_get_gconf (pmdt,
					"icon_path",
					admin, def, NULL);
	}
	return set;
}

static void
reload_settings (PrintManagerDeviceTest *pmdt)
{
	if (print_manager_prefs_is_gconf_admin_mode ()) {
		if (reload_settings_from_gconf (pmdt, TRUE, TRUE))
			return;
		reload_settings_from_gconf (pmdt, FALSE, TRUE);
	} else {
		if (reload_settings_from_gconf (pmdt, FALSE, FALSE))
			return;
		if (reload_settings_from_gconf (pmdt, TRUE, TRUE))
			return;
		reload_settings_from_gconf (pmdt, FALSE, TRUE);
	}
}

static void
change_settings (PrintManagerDeviceTest *pmdt)
{
	print_manager_device_changed (PRINT_MANAGER_DEVICE (pmdt));
}

/* access functions */
static gboolean
pmdt_get_is_visible (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->is_visible;
}

static void
pmdt_set_is_visible (PrintManagerDevice *pmd,
		     gboolean is_visible)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	pmdt->priv->is_visible = is_visible;

	pmdt_set_gconf (pmdt, "visible",
			PRINT_PREF_TYPE_BOOLEAN,
			GINT_TO_POINTER (pmdt->priv->is_visible));

	change_settings (pmdt);
}

static gboolean
pmdt_get_is_default (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->is_default;
}

static const char *
pmdt_get_label (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->label;
}

static void
pmdt_set_label (PrintManagerDevice *pmd,
		const char *label)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	g_free (pmdt->priv->label);
	pmdt->priv->label = g_strdup (label);

	pmdt_set_gconf (pmdt, "label",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->label);

	change_settings (pmdt);
}

static const char *
pmdt_get_desc (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->desc;
}

static void
pmdt_set_desc (PrintManagerDevice *pmd,
	       const char *desc)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	g_free (pmdt->priv->desc);
	pmdt->priv->desc = g_strdup (desc);

	pmdt_set_gconf (pmdt, "description",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->desc);

	change_settings (pmdt);
}

static const char *
pmdt_get_queue_name (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->queue_name;
}

static void
pmdt_set_queue_name (PrintManagerDevice *pmd,
		     const char *queue_name)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	g_free (pmdt->priv->queue_name);
	pmdt->priv->queue_name = g_strdup (queue_name);
	change_settings (pmdt);
}


static const char *
pmdt_get_icon_path (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	return pmdt->priv->icon_path;
}

static void
pmdt_set_icon_path (PrintManagerDevice *pmd,
		    const char *icon_path)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	g_free (pmdt->priv->icon_path);
	pmdt->priv->icon_path = g_strdup (icon_path);

	pmdt_set_gconf (pmdt, "icon_path",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->icon_path);

	change_settings (pmdt);
}

/* Change functions */
static void
pmdt_reload (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);
	reload_settings (pmdt);
	if (pmdt->priv->queue)
		print_manager_queue_reload (pmdt->priv->queue);
}


static void
pmdt_save (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	pmdt_set_gconf (pmdt, "visible",
			PRINT_PREF_TYPE_BOOLEAN,
			GINT_TO_POINTER (pmdt->priv->is_visible));
	pmdt_set_gconf (pmdt, "label",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->label);
	pmdt_set_gconf (pmdt, "description",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->desc);
	pmdt_set_gconf (pmdt, "icon_path",
			PRINT_PREF_TYPE_STRING,
			pmdt->priv->icon_path);
}

static PrintManagerQueue *
pmdt_get_queue (PrintManagerDevice *pmd)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST (pmd);

	g_return_val_if_fail (pmdt != NULL, NULL);

	if (pmdt->priv->queue == NULL) {
		pmdt->priv->queue =
			print_manager_queue_test_new (pmdt->priv->directory,
						      pmdt->priv->queue_name);
	}

	return pmdt->priv->queue;
}

static void
pmdt_print_file (PrintManagerDevice *pmd,
		 const char         *filename, 
		 PrintFileDoneFn     done_cb,
		 gpointer            user_data)
{
	print_manager_queue_test_print_file (PRINT_MANAGER_QUEUE_TEST (pmdt_get_queue (pmd)), filename);

	if (done_cb) 
		(*done_cb) (pmd, filename, user_data);
}

static void
pmdt_class_init (PrintManagerDeviceTestClass *klass)
{
	PrintManagerDeviceClass *device_class =
		PRINT_MANAGER_DEVICE_CLASS (klass);

	device_class->get_is_visible = pmdt_get_is_visible;
	device_class->set_is_visible = pmdt_set_is_visible;

	device_class->get_is_default = pmdt_get_is_default;

	device_class->get_label      = pmdt_get_label;
	device_class->set_label      = pmdt_set_label;

	device_class->get_desc       = pmdt_get_desc;
	device_class->set_desc       = pmdt_set_desc;

	device_class->get_queue_name = pmdt_get_queue_name;
	device_class->set_queue_name = pmdt_set_queue_name;

	device_class->get_icon_path  = pmdt_get_icon_path;
	device_class->set_icon_path  = pmdt_set_icon_path;

	device_class->get_queue      = pmdt_get_queue;
	device_class->print_file     = pmdt_print_file;

	device_class->reload         = pmdt_reload;
	device_class->save           = pmdt_save;
}

static void
pmdt_init (PrintManagerDeviceTest *pmdt)
{
	pmdt->priv = g_new (PrintManagerDeviceTestPrivate, 1);
	pmdt->priv->label = NULL;
	pmdt->priv->desc = NULL;
	pmdt->priv->icon_path = g_strdup (PRINT_MANAGER_ICONDIR "/inkjet.png");

	pmdt->priv->queue_name = NULL;

	pmdt->priv->queue = NULL;

	pmdt->priv->is_visible = TRUE;
	pmdt->priv->is_default = FALSE;
}

/**
 * print_manager_device_test_get_type:
 * @void: 
 * 
 * Registers the &PrintManagerDeviceTest class if necessary, and returns
 * the type ID associated to it.
 * 
 * Return value: The type ID of the &PrintManagerDeviceTest class.
 **/
GType
print_manager_device_test_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (PrintManagerDeviceTestClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) pmdt_class_init,
			(GClassFinalizeFunc) NULL,
			NULL, /* class_data */
			sizeof (PrintManagerDeviceTest),
			0,
			(GInstanceInitFunc) pmdt_init,
			NULL /* value_table */
		};

		type = g_type_register_static (PARENT_TYPE,
					       "PrintManagerDeviceTest",
					       &info, 0);
	}

	return type;
}

PrintManagerDevice *
print_manager_device_test_new (char *directory, char *queue_name)
{
	PrintManagerDeviceTest *pmdt = PRINT_MANAGER_DEVICE_TEST
		(g_type_create_instance (PRINT_MANAGER_TYPE_DEVICE_TEST));

	pmdt->priv->directory = g_strdup (directory);

	g_free (pmdt->priv->queue_name);
	g_free (pmdt->priv->label);
	g_free (pmdt->priv->desc);
	pmdt->priv->queue_name = g_strdup (queue_name);
	pmdt->priv->label      = g_strdup (queue_name);
	pmdt->priv->desc       = g_strdup (queue_name);

	reload_settings (pmdt);

	return PRINT_MANAGER_DEVICE (pmdt);
}

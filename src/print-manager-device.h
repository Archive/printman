/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_DEVICE_H
#define PRINT_MANAGER_DEVICE_H 1

#include <glib-object.h>
#include "print-manager-queue.h"

#define PRINT_MANAGER_TYPE_DEVICE            (print_manager_device_get_type ())
#define PRINT_MANAGER_DEVICE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), PRINT_MANAGER_TYPE_DEVICE, PrintManagerDevice))
#define PRINT_MANAGER_DEVICE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), PRINT_MANAGER_TYPE_DEVICE, PrintManagerDeviceClass))
#define PRINT_MANAGER_IS_DEVICE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), PRINT_MANAGER_TYPE_DEVICE))
#define PRINT_MANAGER_IS_DEVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), PRINT_MANAGER_TYPE_DEVICE))

typedef struct _PrintManagerDevice PrintManagerDevice;
typedef struct _PrintManagerDeviceClass PrintManagerDeviceClass;

struct _PrintManagerDevice {
	GObject object;
};

typedef void (*PrintFileDoneFn) (PrintManagerDevice *pmd,
				 const gchar        *filename,
				 gpointer            user_data);

struct _PrintManagerDeviceClass {
	GObjectClass parent_class;

	/* Virtual Methods */
	gboolean            (*get_is_visible)  (PrintManagerDevice *printer);
	void                (*set_is_visible)  (PrintManagerDevice *printer,
						gboolean            visible);
	gboolean            (*get_is_default)  (PrintManagerDevice *printer);

	const char         *(*get_label)       (PrintManagerDevice *printer);
	void                (*set_label)       (PrintManagerDevice *printer,
						const char         *label);

	const char         *(*get_desc)        (PrintManagerDevice *printer);
	void                (*set_desc)        (PrintManagerDevice *printer,
						const char         *desc);

	const char         *(*get_queue_name)  (PrintManagerDevice *printer);
	void                (*set_queue_name)  (PrintManagerDevice *printer,
						const char         *desc);

	const char         *(*get_icon_path)   (PrintManagerDevice *printer);
	void                (*set_icon_path)   (PrintManagerDevice *printer,
						const char         *icon_path);

	PrintManagerQueue  *(*get_queue)       (PrintManagerDevice *printer);
	void                (*print_file)      (PrintManagerDevice *printer,
						const char         *filename,
						PrintFileDoneFn     done_cb,
						gpointer            user_data);

	void                (*reload)          (PrintManagerDevice *printer);
	void                (*save)            (PrintManagerDevice *printer);

	/* Signals */
	void (*changed) (PrintManagerDevice *device);
};

/* Standard gobject functions */
GType               print_manager_device_get_type        (void);

/* access functions */
gboolean            print_manager_device_get_is_visible  (PrintManagerDevice *printer);
void                print_manager_device_set_is_visible  (PrintManagerDevice *printer,
							  gboolean            visible);
gboolean            print_manager_device_get_is_default  (PrintManagerDevice *printer);

const char         *print_manager_device_get_label       (PrintManagerDevice *printer);
void                print_manager_device_set_label       (PrintManagerDevice *printer,
							  const char         *label);

const char         *print_manager_device_get_desc        (PrintManagerDevice *printer);
void                print_manager_device_set_desc        (PrintManagerDevice *printer,
							  const char         *desc);

const char         *print_manager_device_get_queue_name  (PrintManagerDevice *printer);
void                print_manager_device_set_queue_name  (PrintManagerDevice *printer,
							  const char         *desc);


const char         *print_manager_device_get_icon_path   (PrintManagerDevice *printer);
void                print_manager_device_set_icon_path   (PrintManagerDevice *printer,
							  const char         *icon_path);

PrintManagerQueue  *print_manager_device_get_queue       (PrintManagerDevice *printer);

void                print_manager_device_print_file      (PrintManagerDevice *printer,
							  const gchar        *filename,
							  PrintFileDoneFn     done_cb,
							  gpointer            user_data);

/* Reload function */
void                print_manager_device_reload          (PrintManagerDevice *printer);
void                print_manager_device_save            (PrintManagerDevice *printer);

/* Send Signal */
void                print_manager_device_changed         (PrintManagerDevice *printer);

#endif /* ! __PRINT_MANAGER_DEVICE_H__ */

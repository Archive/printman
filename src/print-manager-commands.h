/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PRINT_MANAGER_COMMANDS_H
#define PRINT_MANAGER_COMMANDS_H 1

#include <glib.h>

typedef void (*LPStatDataFn) (gint       lpstat_id,
			      gchar     *line,
			      gpointer   user_data);

typedef void (*LPStatEndFn) (gint       lpstat_id,
			     gboolean   cancelled,
			     gpointer   user_data);

typedef enum {
	/* 
	 * Output format: "queue_name\nqueue_name"
	 * Command args: none
	 */
	LPSTAT_LIST_PRINTERS,

	/* 
	 * Output format: "queue_name"
	 * Command args: none
	 */
	LPSTAT_LIST_DEFAULT_PRINTER,

	/*
	 * Output format: "printer|jobname|jobnum|owner|datetime|bytesize"
	 * Command args: queuename
	 */
	LPSTAT_LIST_LOCAL_JOBS,

	/*
	 * Output format: "printer|jobname|jobnum|owner|datetime|bytesize"
	 * Command args: hostname queuename
	 */
	LPSTAT_LIST_REMOTE_JOBS,

	/*
	 * Output format: "device:remotehost:remoteport"
	 * Command args: queuename
	 */
	LPSTAT_GET_QUEUE_ATTRS,

	/*
	 * Output format: exit 1, disabled
	 * Command args: queuename
	 */
	LPSTAT_GET_QUEUE_STATUS,

	/*
	 * Output format: exit 1, disabled
	 * Command args: queuename
	 */
	LPSTAT_GET_DEVICE_STATUS,

	/*
	 * Output format: exit 1, failure
	 * Command args: queuename jobnum
	 */
	LPSTAT_CANCEL_JOB,

	/*
	 * Output format: exit 1, failure
	 * Command args: queuename filename
	 */
	LPSTAT_PRINT_FILE,

	LPSTAT_LIST_TIMESTAMP

} LPStatCommand;

gint lpstat_run        (LPStatDataFn   stdout_cb,
			LPStatDataFn   stderr_cb,
			LPStatEndFn    end_cb,
			gpointer       user_data,
			LPStatCommand  command,
			gchar        **command_args);

void lpstat_cancel     (gint lpstat_id);

void lpstat_cancel_all (void);

#endif /*PRINT_MANAGER_COMMANDS_H*/

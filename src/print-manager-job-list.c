/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  GNOME Print Manager
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include "print-manager-job-list.h"
#include "print-manager-printer-properties.h"
#include "print-manager-settings-dialog.h"
#include "print-manager-about.h"
#include "print-manager-queue.h"
#include "print-manager-dnd.h"
#include "print-manager-access.h"
#include "print-manager-main-window.h"

#include <gnome.h>
#include <gconf/gconf-client.h>

typedef struct {
	PrintManagerBackend *backend;
	PrintManagerDevice  *device;
	PrintManagerQueue   *queue;

	char                *queue_name;

	GtkWidget           *window;
	GtkWidget           *statusbar;
	GtkWidget           *tree_view;
	GtkListStore        *store;

	GtkWidget           *everyone;
	GtkWidget           *onlymine;

	GtkWidget           *cancel;

	guint                is_main : 1;
	guint                update_interval;
	guint                refresh_timeout_id;

	GSList              *print_jobs;
} JobList;

enum {
	COL_NAME,
	COL_OWNER,
	COL_NUM,
	COL_SIZE,
	COL_SUBMIT,
	COL_JOB,
	LAST_COL
};

static GHashTable *
get_dialog_cache (void) 
{
	static GHashTable *dialog_cache = NULL;

	if (dialog_cache == NULL)
		dialog_cache = g_hash_table_new (g_str_hash, g_str_equal);

	return dialog_cache;
}

/*
 * Job List Window.
 */
static void 
selection_foreach_cb (GtkTreeModel *model,
		      GtkTreePath *path,
		      GtkTreeIter *iter,
		      gpointer data)
{

	GSList **list = data;
	PrintJob *job;
	
	gtk_tree_model_get (model, iter, COL_JOB, &job, -1);

	*list = g_slist_prepend (*list, job);
}

static GSList *
get_selected_jobs (JobList *job_list)
{
	GtkTreeSelection *selection;
	GSList *ret = NULL;

	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (job_list->tree_view));
	
	gtk_tree_selection_selected_foreach (selection, 
					     selection_foreach_cb,
					     &ret);
	
	return ret;
}

static void
set_menu_sensitivities (JobList *job_list)
{
	GSList *selected = get_selected_jobs (job_list);

	gtk_widget_set_sensitive (job_list->cancel, (selected != NULL));

	g_slist_free (selected);
}

static gboolean
jobs_delete_event_cb (GtkWidget * widget, GdkEvent * event, JobList *job_list)
{
	if (job_list->is_main)
		gtk_main_quit ();
	else 
		gtk_widget_hide (widget);

	if (job_list->refresh_timeout_id != 0)
		g_source_remove (job_list->refresh_timeout_id);

	return TRUE;
}

static void
jobs_close_cb (GtkWidget * widget, JobList *job_list)
{
	if (job_list->is_main)
		gtk_main_quit ();
	else
		gtk_widget_hide (job_list->window);

	if (job_list->refresh_timeout_id != 0)
		g_source_remove (job_list->refresh_timeout_id);
}

static void
show_jobs_status_bar_cb (GtkWidget * widget, JobList *job_list)
{
	if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (widget)))
		gtk_widget_show (job_list->statusbar);
	else
		gtk_widget_hide (job_list->statusbar);
}

static void
default_cb (GtkWidget * widget, JobList *job_list)
{
	int i, count;
	count = print_manager_backend_get_count (job_list->backend);

	for (i = 0; i < count; i++) {
		const gchar *qname1, *qname2;

		qname1 = print_manager_device_get_queue_name (job_list->device);
		qname2 = 
			print_manager_device_get_queue_name (
				print_manager_backend_get_device (
					job_list->backend, 
					i));

		if (!strcmp (qname1, qname2)) {
			print_manager_backend_set_default_printer (
				job_list->backend, 
				i);
			break;
		}
	}
}

static void
printer_properties_cb (GtkWidget * widget, JobList *job_list)
{
	print_manager_show_printer_properties (job_list->device, 
					       GTK_WINDOW (job_list->window));
}


static void
show_everyone_cb (GtkWidget *widget, JobList *job_list)
{
	if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (widget)))
		if (!print_manager_backend_get_show_all_jobs (
			     job_list->backend))
			print_manager_backend_set_show_all_jobs (
				job_list->backend, 
				TRUE);
}

static void
show_onlymine_cb (GtkWidget *widget, JobList *job_list)
{
	if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (widget)))
		if (print_manager_backend_get_show_all_jobs (job_list->backend))
			print_manager_backend_set_show_all_jobs (
				job_list->backend, 
				FALSE);
}

static void
refresh_jobs_cb (GtkWidget *widget, JobList *job_list)
{
	print_manager_queue_reload (job_list->queue);
}

static void
job_cancel_output_fn (PrintManagerQueue *queue,
		      PrintJob    *job,
		      gboolean     success,
		      gchar       *errstr,
		      gpointer     user_data)
{
	JobList *job_list = user_data;
	const gchar *label;
	GtkWidget *dialog;

	if (!errstr || !strlen (errstr))
		return;

	label = print_manager_device_get_label (job_list->device);
	dialog = 
		gtk_message_dialog_new (
			GTK_WINDOW (job_list->window), 
			GTK_DIALOG_MODAL, 
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_OK,
			_("Error cancelling \"%s\" on printer \"%s\".\n"
			  "Reason given by server: %s.\n"),
			job->doc_name,
			label,
			errstr);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static void
cancel_cb (GtkWidget *widget, JobList *job_list)
{
	GSList *selected;
	const gchar *label;
	GtkWidget *dialog;
	gint length, resp;

	selected = get_selected_jobs (job_list);
	label = print_manager_device_get_label (job_list->device);
	length = g_slist_length (selected);

	if (length == 1) {
		PrintJob *job = selected->data;
		dialog = 
			gtk_message_dialog_new (
				GTK_WINDOW (job_list->window), 
				GTK_DIALOG_MODAL, 
				GTK_MESSAGE_QUESTION,
				GTK_BUTTONS_YES_NO,
				_("Cancel printing of \"%s\" "
				  "on printer \"%s\"?"),
				job->doc_name,
				label);
	} else
		dialog = 
			gtk_message_dialog_new (
				GTK_WINDOW (job_list->window), 
				GTK_DIALOG_MODAL, 
				GTK_MESSAGE_QUESTION,
				GTK_BUTTONS_YES_NO,
				_("Cancel printing of %d documents "
				  "on printer \"%s\"?"),
				length,
				label);

	resp = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	if (resp != GTK_RESPONSE_YES) {
		g_slist_free (selected);
		return;
	}

	print_manager_queue_cancel_jobs (job_list->queue,
					 selected,
					 job_cancel_output_fn,
					 job_list);
	g_slist_free (selected);
}

static void
preferences_cb (GtkWidget * widget, JobList *job_list)
{
	print_manager_show_settings_dialog (job_list->backend, 
					    GTK_WINDOW (job_list->window));
}

static void
about_cb (GtkWidget *widget, gpointer data)
{
	print_manager_about ();
}

static void
update_window_title (JobList *job_list)
{
	const char *status, *label;
	char *title;

	status = print_manager_queue_get_status (job_list->queue);
	label = print_manager_device_get_label (job_list->device);

	title = g_strdup_printf ("%s - %s", 
				 label ? label : "",
				 status ? status : _("Unknown"));
	gtk_window_set_title (GTK_WINDOW (job_list->window), title);
	g_free (title);
}

static gboolean
check_if_job_already_in_store (JobList *job_list, PrintJob *new_job)
{
	GSList *iter;

	for (iter = job_list->print_jobs; iter; iter = iter->next) {
		PrintJob *job = iter->data;

		if (print_job_equal (job, new_job))
			return TRUE;
	}

	return FALSE;
}

static gboolean 
check_if_job_exists (PrintManagerQueue *queue, JobList *job_list, PrintJob *job) {

	int job_count, i;

	if (!print_manager_backend_get_show_all_jobs (job_list->backend) && !job->user_owned)
		return FALSE;

	job_count = print_manager_queue_get_count (queue);

	for (i = 0; i < job_count; i++) {
		PrintJob *new_job = print_manager_queue_get_job (queue, i);

		if (print_job_equal (job, new_job))
			return TRUE;
	}

	return FALSE;
}

static void
remove_nonexistant_jobs (PrintManagerQueue *queue, JobList *job_list)
{
	GtkTreeIter iter;
	gboolean ret_val;

	if (!job_list->print_jobs)
		return;

	ret_val = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (job_list->store), &iter);

	while (ret_val) {
		PrintJob *job;

		gtk_tree_model_get (GTK_TREE_MODEL (job_list->store),
				    &iter,
				    COL_JOB, &job,
				    -1);

		if (!check_if_job_exists (queue, job_list, job)) {
			job_list->print_jobs = g_slist_remove (job_list->print_jobs, job);
			print_job_free (job);
			ret_val = gtk_list_store_remove (job_list->store, &iter);
			continue;
		}

		ret_val = gtk_tree_model_iter_next (GTK_TREE_MODEL (job_list->store), &iter);
	}
}

static void
free_print_jobs (GSList *list)
{
	GSList *iter;

	for (iter = list; iter; iter = iter->next) {
		PrintJob *job = iter->data;

		print_job_free (job);
	}

	g_slist_free (list);
}

static void
queue_changed (PrintManagerQueue *queue, JobList *job_list)
{
	int i, count;
	GtkTreeIter iter;
	int job_count = 0;
	const gchar *queuename;
	char *status_str;

	update_window_title (job_list);

	remove_nonexistant_jobs (queue, job_list);

	count = print_manager_queue_get_count (queue);
	for (i = 0; i < count; i++) {
		PrintJob *job = print_manager_queue_get_job (queue, i);

		if (print_manager_backend_get_show_all_jobs (job_list->backend)
		    || job->user_owned) {

			if (!check_if_job_already_in_store (job_list, job)) {
				PrintJob *new_job = print_job_copy (job);
				job_list->print_jobs = g_slist_prepend (job_list->print_jobs, new_job);
				gtk_list_store_insert (job_list->store, &iter, i);
				gtk_list_store_set (job_list->store, &iter, 
						    COL_NAME, new_job->doc_name,
						    COL_OWNER, new_job->owner,
						    COL_NUM, new_job->job_num,
						    COL_SIZE, new_job->file_size,
						    COL_SUBMIT, new_job->submit_date,
						    COL_JOB, new_job,
						    -1);
			}
			job_count ++;
		}
	}

	queuename = print_manager_device_get_label (job_list->device);

	if (!job_count)
		status_str = g_strdup_printf (_("No jobs in queue \"%s\""), 
					      queuename);
	else if (job_count == 1)
		status_str = g_strdup_printf (_("1 job in queue \"%s\""), 
					      queuename);
	else
		status_str = g_strdup_printf (_("%d jobs in queue \"%s\""), 
					      job_count,
					      queuename);

	gtk_statusbar_push (GTK_STATUSBAR (job_list->statusbar),
			    0,
			    status_str);
	g_free (status_str);
}

static void
tree_selection_changed_cb (GtkTreeSelection *selection, JobList *job_list)
{
	set_menu_sensitivities (job_list);
}

static gboolean
device_refresh_timeout_cb (gpointer data) 
{
	PrintManagerDevice *device = data;
	print_manager_device_reload (device);
	return TRUE;
}

static void
backend_prefs_changed (PrintManagerBackend *backend, JobList *job_list)
{
	gboolean show_all_jobs;
	gint interval;

	show_all_jobs = print_manager_backend_get_show_all_jobs (backend);
	if (show_all_jobs)
		gtk_check_menu_item_set_active (
			GTK_CHECK_MENU_ITEM (job_list->everyone),
			TRUE);
	else
		gtk_check_menu_item_set_active (
			GTK_CHECK_MENU_ITEM (job_list->onlymine),
			TRUE);

	interval = print_manager_backend_get_update_interval (backend);
	if (job_list->update_interval != interval * 1000) {
		job_list->update_interval = interval * 1000;

		if (job_list->refresh_timeout_id != 0)
			g_source_remove (job_list->refresh_timeout_id);

		job_list->refresh_timeout_id =
			g_timeout_add (job_list->update_interval,
				       device_refresh_timeout_cb,
				       job_list->device);
	}

	queue_changed (job_list->queue, job_list);
}

static GtkWidget *
create_jobs_menubar (GtkAccelGroup * accel_group,
		     GtkTooltips * tooltips,
		     JobList *job_list)
{
	GtkWidget *menubar, *menuitem, *menu;
	GSList *group;
	GConfClient *gconf_client;

	gconf_client = gconf_client_get_default ();

	menubar = gtk_menu_bar_new ();
	gtk_widget_show (menubar);

	/* Printer menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Printer"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = gtk_menu_item_new_with_mnemonic (_("Set as _Default"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem,
			      _("Set the printer as the default printer"),
			      NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (default_cb), 
			  job_list);
	menuitem = append_stock_menuitem (menu, GTK_STOCK_PROPERTIES,
					  accel_group, G_CALLBACK (printer_properties_cb),
					  gconf_client, job_list);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	if (job_list->is_main) {
		menuitem = append_stock_menuitem (menu, GTK_STOCK_QUIT,
						  accel_group, G_CALLBACK (jobs_close_cb),
						  gconf_client, job_list);
	}
	else {
		menuitem = append_stock_menuitem (menu, GTK_STOCK_CLOSE,
						  accel_group, G_CALLBACK (jobs_close_cb),
						  gconf_client, job_list);
	}
	/* Edit menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Edit"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = gtk_menu_item_new_with_mnemonic (_("_Cancel Documents"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem,
			      _("Cancel printing of the selected documents"),
			      NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (cancel_cb),
			  job_list);
	job_list->cancel = menuitem;

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_PREFERENCES,
					  accel_group, G_CALLBACK (preferences_cb),
					  gconf_client, job_list);
	/* View menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_View"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	menuitem = 
		gtk_radio_menu_item_new_with_mnemonic (NULL, 
						       _("Show _My Documents"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (show_onlymine_cb), 
			  job_list);
	job_list->onlymine = menuitem;

	group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (menuitem));
	menuitem =
		gtk_radio_menu_item_new_with_mnemonic (
			group,
			_("Show _All Documents"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (show_everyone_cb), 
			  job_list);
	job_list->everyone = menuitem;

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem =
		gtk_check_menu_item_new_with_mnemonic (_("Show _Status Bar"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menuitem), TRUE);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_tooltips_set_tip (tooltips, 
			      menuitem,
			      _("Whether to show the status bar"
				" at the bottom of the window"),
			      NULL);
	gtk_widget_show (menuitem);
	g_signal_connect (menuitem, "activate",
			  G_CALLBACK (show_jobs_status_bar_cb), 
			  job_list);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = append_stock_menuitem (menu, GTK_STOCK_REFRESH,
					  accel_group, G_CALLBACK (refresh_jobs_cb),
					  gconf_client, job_list);
	/* Help menu. */
	menuitem = gtk_menu_item_new_with_mnemonic (_("_Help"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	gtk_widget_show (menuitem);

	menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	{
		GnomeUIInfo uiinfo [] = {
			GNOMEUIINFO_HELP ((gchar *) "gnome-print-manager"),
			GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
			GNOMEUIINFO_END
		};
		gnome_app_fill_menu (GTK_MENU_SHELL (menu), 
				     uiinfo,
				     accel_group,
				     TRUE,
				     0);
	}

	return menubar;
}


static gboolean
tree_drag_motion_cb (GtkWidget *tree_view, 
		     GdkDragContext *context, 
		     int x, int y, 
		     guint time, 
		     gpointer user_data)
{
	gdk_drag_status (context, GDK_ACTION_COPY, time);
	return TRUE;
}

static gboolean 
tree_drag_drop_cb (GtkWidget *tree_view, 
		   GdkDragContext *context,
		   int x, int y, 
		   guint time)
{
	GdkAtom target;

	target = 
		gtk_drag_dest_find_target (
			tree_view, 
			context, 
			gtk_drag_dest_get_target_list (tree_view));

	if (target != GDK_NONE)
		gtk_drag_get_data (tree_view, context, target, time);

	return TRUE;
}

static void
tree_drag_data_received_cb (GtkWidget *tree_view, 
			    GdkDragContext *context, 
			    int x, int y, 
			    GtkSelectionData *selection_data,
			    guint info, 
			    guint time, 
			    JobList *job_list)
{
	print_manager_dnd_print_uri_list (job_list->device, 
					  (char *) selection_data->data);
	gtk_drag_finish (context, TRUE, FALSE, time);
}

static void
create_job_list (JobList *job_list)
{
	GtkWidget *window, *menubar, *scrolledwin;
	GtkAccelGroup *accel_group;
	GtkTooltips *tooltips;
	GtkTreeViewColumn *col;
	GtkCellRenderer *rend;
	GtkTreeSelection *selection;
	const gchar *queuename;

	window = gnome_app_new ("printman", "GNOME Print Manager");

	accel_group = gtk_accel_group_new ();
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	tooltips = gtk_tooltips_new ();

	menubar = create_jobs_menubar (accel_group, tooltips, job_list);
	gnome_app_set_menus (GNOME_APP (window), GTK_MENU_BAR (menubar));

	/* List view - Job Name, Owner, Number, Size, Submitted Date/Time. */
	scrolledwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwin);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwin),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwin),
					     GTK_SHADOW_IN);
	gnome_app_set_contents (GNOME_APP (window), scrolledwin);

	job_list->store = gtk_list_store_new (LAST_COL,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_POINTER);

	job_list->tree_view = 
		gtk_tree_view_new_with_model (GTK_TREE_MODEL (job_list->store));

	g_object_unref (G_OBJECT (job_list->store));
	selection = 
		gtk_tree_view_get_selection (
			GTK_TREE_VIEW (job_list->tree_view));
	gtk_tree_selection_set_mode (GTK_TREE_SELECTION (selection),
				     GTK_SELECTION_MULTIPLE);
	g_signal_connect (selection,
			  "changed",
			  G_CALLBACK (tree_selection_changed_cb), 
			  job_list);

	_add_atk_name_desc (GTK_WIDGET (job_list->tree_view),
			    _("Job List"),
			    _("Lists print jobs in this printer's queue"));

	gtk_widget_show (job_list->tree_view);
	gtk_container_add (GTK_CONTAINER (scrolledwin), job_list->tree_view);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Document"), 
							rend,
							"text", 
							COL_NAME, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (job_list->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Owner"), 
							rend,
							"text",
							COL_OWNER, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (job_list->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Job Number"), 
							rend,
							"text", 
							COL_NUM, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (job_list->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Size"), 
							rend,
							"text", 
							COL_SIZE, 
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (job_list->tree_view), col);

	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Time Submitted"),
							rend, 
							"text", 
							COL_SUBMIT,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (job_list->tree_view), col);

	print_manager_dnd_setup (job_list->tree_view,
				 G_CALLBACK (tree_drag_motion_cb),
				 G_CALLBACK (tree_drag_drop_cb),
				 G_CALLBACK (tree_drag_data_received_cb),
				 job_list);

	job_list->statusbar = gtk_statusbar_new ();
	gnome_app_set_statusbar (GNOME_APP (window), job_list->statusbar);

	gtk_window_set_default_size (GTK_WINDOW (window), 500, 300);

	g_signal_connect (window,
			  "delete-event",
			  G_CALLBACK (jobs_delete_event_cb), 
			  job_list);

	job_list->window = window;

	job_list->queue = print_manager_device_get_queue (job_list->device);
	g_object_ref (job_list->queue);

	update_window_title (job_list);

	/* We set the role to the queue name, to identify the window for
	   session management. The title isn't good enough as it includes
	   the print queue status, which may be different when it is restarted.
	*/
	queuename = print_manager_device_get_queue_name (job_list->device);
	gtk_window_set_role (GTK_WINDOW (job_list->window), queuename);

	g_signal_connect (job_list->queue, "changed",
			  G_CALLBACK (queue_changed), job_list);

	g_signal_connect (job_list->backend, "prefs_changed",
			  G_CALLBACK (backend_prefs_changed), job_list);

	backend_prefs_changed (job_list->backend, job_list);

	set_menu_sensitivities (job_list);
}

static void
device_gone (gpointer data, GObject *object)
{
	JobList *job_list = data;
	GHashTable *dialog_cache;

	dialog_cache = get_dialog_cache();
	g_hash_table_remove (dialog_cache, job_list->queue_name);

	g_free (job_list->queue_name);
	g_object_unref (job_list->backend);
	job_list->device = NULL;
	if (job_list->queue)
		g_object_unref (job_list->queue);
	gtk_widget_destroy (job_list->window);

	if (job_list->print_jobs) {
		free_print_jobs (job_list->print_jobs);
		job_list->print_jobs = NULL;
	}

	g_free (job_list);
}

static void
highlight_selected_job (GtkTreeView   *view,
			GtkTreeModel  *model,
			PrintJob      *selected_job)
{
	GtkTreeIter iter;

	if (!gtk_tree_model_get_iter_first (model, &iter))
		return;

	while (1) {
		PrintJob *job;

		gtk_tree_model_get (model, 
				    &iter, 
				    COL_JOB, &job, 
				    -1);

		if (print_job_equal (job, selected_job)) {
			GtkTreeSelection *selection;

			selection = gtk_tree_view_get_selection (view);
			gtk_tree_selection_unselect_all (selection);
			gtk_tree_selection_select_iter (selection, &iter);
			break;
		}

		if (!gtk_tree_model_iter_next (model, &iter))
			break;
	}
}

void
print_manager_show_job_list (PrintManagerBackend *backend, 
			     PrintManagerDevice  *device,
			     gboolean             is_main,
			     PrintJob            *selected_job)
{
	JobList *job_list;
	GHashTable *dialog_cache;
	const gchar *queuename;

	dialog_cache = get_dialog_cache();

	queuename = print_manager_device_get_queue_name (device);
	job_list = g_hash_table_lookup (dialog_cache, queuename);

	if (!job_list) {
		job_list = g_new0 (JobList, 1);

		job_list->backend = backend;
		g_object_ref (backend);

		job_list->device = device;
		g_object_weak_ref (G_OBJECT (device),
				   device_gone,
				   job_list);

		job_list->queue_name = g_strdup (queuename);
		job_list->is_main = is_main;
		job_list->print_jobs = NULL;

		create_job_list (job_list);

		g_hash_table_insert (dialog_cache,
				     g_strdup (queuename),
				     job_list);

		gtk_widget_show (job_list->window);
	}
	else
		gtk_window_present (GTK_WINDOW (job_list->window));

	if (selected_job)
		highlight_selected_job (GTK_TREE_VIEW (job_list->tree_view),
					GTK_TREE_MODEL (job_list->store),
					selected_job);

	set_menu_sensitivities (job_list);
}

static void
get_open_job_lists_cb (char     *queue_name, 
		       JobList  *job_list,
		       GList   **open_job_lists)
{
	/* If the job list window is open add it to the list. */
	if (GTK_WIDGET_DRAWABLE (job_list->window))
		*open_job_lists = g_list_prepend (*open_job_lists, queue_name);
}

/* Returns a list of queue names of job list windows that are currently open.
   The list should be freed, but not the queue names. */
GList*
print_manager_get_open_job_lists (void)
{
	GList *open_job_lists = NULL;

	g_hash_table_foreach (get_dialog_cache (),
			      (GHFunc) get_open_job_lists_cb,
			      &open_job_lists);

	return open_job_lists;
}

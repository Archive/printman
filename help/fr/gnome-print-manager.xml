<?xml version="1.0"?><!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "0.0.1">
  <!ENTITY manrevision "2.2">
  <!ENTITY date "November 2002">
  <!ENTITY app "Gestionnaire d'impression GNOME">
]>

<!--
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Apr 11, 2002
  
-->

<!-- =============Document Header ============================= -->
<article id="index" lang="fr">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
    <title>Manuel &app;  V&manrevision;</title>

    <copyright> 
      <year>2003</year> 
      <holder>Sun Microsystems</holder> 
    </copyright> 

<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
    <publisher> 
      <publishername> Projet de documentation GNOME </publishername> 
    </publisher> 

   &legal;

    <authorgroup>
      <author> 
	<firstname>Sun</firstname> 
	<surname>Equipe de documentation GNOME</surname> 
	<affiliation> 
	  <orgname>Sun Microsystems</orgname> 
	  <address> <email>gdocteam@sun.com</email> </address>
	</affiliation>
      </author>
      <author> 
	<firstname>Aaron</firstname> 
	<surname>Weber</surname> 
	<affiliation> 
	  <orgname>Projet de documentation GNOME</orgname> 
	  <address> <email>aaron@ximian.com</email> </address>
	</affiliation>
      </author>
<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>

    <revhistory>
      <revision> 
	<revnumber>&app; Manuel V&manrevision;</revnumber> 
	<date>&date;</date> 
	<revdescription> 
	  <para role="author">Equipe de documentation GNOME de Sun
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuel du gestionnaire d'impression GNOME V2.1</revnumber> 
	<date>Ao&ucirc;t 2002</date> 
	<revdescription> 
	  <para role="author">Equipe de documentation GNOME de Sun
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manuel du gestionnaire d'impression GNOME V2.1</revnumber> 
	<date>Mai 2002</date> 
	<revdescription> 
	  <para role="author">Equipe de documentation GNOME de Sun
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="author">Aaron Weber
	    <email>aaron@ximian.com</email>
	  </para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
    </revhistory>

    <releaseinfo>Le pr&eacute;sent manuel d&eacute;crit la version &appversion; de &app;.
    </releaseinfo>
    <legalnotice> 
        <title>Feedback</title>
      <para>Pour signaler un probl&egrave;me ou &eacute;mettre une suggestion concernant l'application &app; ou le pr&eacute;sent manuel, proc&eacute;dez comme indiqu&eacute; &agrave; la 
	<ulink url="ghelp:gnome-feedback" type="help">GNOME Feedback Page</ulink>. 
      </para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice>
  </articleinfo>

  <indexterm> 
    <primary>Gestionnaire d'impression</primary> 
  </indexterm> 


 <!-- ============= Document Body ============================= -->

 <!-- ============= Introduction ============================== -->
 <sect1 id="printman-intro">
  <title>Introduction</title>

   <para>
    L'application <application>&app;</application> permet de contr&ocirc;ler et g&eacute;rer les imprimantes et les
    travaux d'impression. Vous pouvez utiliser <application>&app;</application> pour ex&eacute;cuter les t&acirc;ches suivantes :
     <itemizedlist>
        <listitem><para>d&eacute;tection des imprimantes existantes ;</para></listitem>
        <listitem><para>choix de l'imprimante &agrave; utiliser pour imprimer diff&eacute;rents types de documents ;</para></listitem>
        <listitem><para>affichage d'une liste des travaux d'impression mis en file d'attente sur chaque imprimante ;</para></listitem>
	<listitem><para>annulation des travaux d'impression mis en file d'attente.</para></listitem>
     </itemizedlist>
   </para>
   
   <para>
    <application>&app;</application> est optimalis&eacute; pour les syst&egrave;mes Sun Solaris, mais l'application se
    compile et s'ex&eacute;cute sur la plupart des syst&egrave;mes UNIX ou Linux.
   </para>
   <note>
	<para>
  	<application>&app;</application> est une application de gestion des imprimantes au niveau des utilisateurs. 
  	L'application ne remplace pas <application>printtool</application> ni aucun autre utilitaire de configuration au niveau racine. <application>&app;</application> ne vous permet pas de d&eacute;tecter de nouvelles imprimantes sur votre r&eacute;seau ni de s&eacute;lectionner les options des pilotes.
 	</para>
   </note>
    
 </sect1>
   
    
 <sect1 id="printman-getting-started"> 
     <title>D&eacute;marrage</title>

    <para>Vous pouvez d&eacute;marrer <application>&app;</application> en recourant &agrave; l'une des m&eacute;thodes suivantes :
    </para>
    <variablelist>
    	<varlistentry>
    		<term>Menu <guimenu>Applications</guimenu></term>
    		<listitem>
    		<para>S&eacute;lectionnez <menuchoice><guisubmenu>Outils syst&egrave;me</guisubmenu><guimenuitem>Gestionnaire d'impression</guimenuitem></menuchoice>.</para>
    		</listitem>
    	</varlistentry>
    	<varlistentry>
    		<term>Ligne de commandes</term>
    		<listitem>
    		<para>Entrez <command>gnome-print-manager</command>, puis appuyez sur <keycap>Entr&eacute;e</keycap>.</para>
    		<para>		 
    		</para>
    		</listitem>
    	</varlistentry>
    </variablelist>
    
    <para>Lorsque vous lancez <application>&app;</application>, la fen&ecirc;tre suivante appara&icirc;t.</para>
    
    <figure id="main-window">
    <title>Fen&ecirc;tre du gestionnaire d'impression GNOME</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/main-window.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>Affiche la fen&ecirc;tre principale du gestionnaire d'impression GNOME.</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
    </figure>
   
  </sect1>
  
  <sect1 id="printman-usage"> 
    <title>Utilisation</title>

  <sect2 id="printman-display">
   <title>Affichage des imprimantes disponibles</title>
   <para>
     La fen&ecirc;tre principale de <application>&app;</application> affiche une liste des imprimantes
     auxquelles vous avez acc&egrave;s. Par d&eacute;faut, <application>&app;</application>
     affiche les imprimantes sous la forme d'ic&ocirc;nes accompagn&eacute;es du nom de l'imprimante. Pour afficher 
     une liste des imprimantes disponibles avec le nombre de documents mis en file d'attente sur chacune 
     et le statut de l'imprimante, choisissez
     <menuchoice><guimenu>Afficher</guimenu><guimenuitem>Liste</guimenuitem></menuchoice>.
   </para>
   <para>
     Pour modifier la liste d'imprimantes affich&eacute;e, choisissez <menuchoice><guimenu>Afficher</guimenu>
     <guimenuitem>S&eacute;lectionner les imprimantes &agrave; afficher</guimenuitem></menuchoice>. La bo&icirc;te de dialogue <guilabel>S&eacute;lectionner les imprimantes à afficher</guilabel> affiche toutes les imprimantes disponibles sur le r&eacute;seau. 
     Choisissez une des options suivantes, puis cliquez sur <guibutton>OK</guibutton>:
   </para>
     <itemizedlist>
       <listitem><para>Pour afficher toutes les imprimantes disponibles sur le r&eacute;seau, cliquez sur <guibutton>
       Afficher tout</guibutton>. Si vous choisissez cette option et qu'une nouvelle imprimante est ajout&eacute;e au r&eacute;seau, choisissez
       <menuchoice><guimenu>Afficher</guimenu><guimenuitem>Rafra&icirc;chir</guimenuitem></menuchoice> pour afficher l'imprimante dans la 
       fen&ecirc;tre principale de <application>&app;</application>.</para></listitem>
       <listitem><para>Pour n'afficher aucune imprimante, cliquez sur <guibutton>Masquer tout </guibutton>.</para></listitem>
       <listitem><para>Pour afficher une partie des imprimantes disponibles sur le r&eacute;seau, s&eacute;lectionnez la case 
       &agrave; cocher face &agrave; chaque imprimante &agrave; afficher.</para></listitem>
     </itemizedlist>
	
  </sect2>
  
  <sect2 id="printman-choose">
   <title>S&eacute;lection d'une imprimante par d&eacute;faut</title>
   <para>
     Pour s&eacute;lectionner une imprimante par d&eacute;faut, cliquez avec le bouton 
     droit sur l'ic&ocirc;ne repr&eacute;sentant l'imprimante dans la fen&ecirc;tre <guilabel>Gestionnaire d'impression GNOME</guilabel>, puis
     s&eacute;lectionnez <guimenuitem>D&eacute;finir par d&eacute;faut</guimenuitem>. 
     Lorsque vous s&eacute;lectionnez une imprimante par d&eacute;faut, toutes vos 
     demandes d'impression sont envoy&eacute;es &agrave; l'imprimante s&eacute;lectionn&eacute;e, 
     &agrave; moins que vous n'en indiquiez une autre lors de l'impression d'un document.</para>
  <para>    
     Pour imprimer un document sur une imprimante sp&eacute;cifique,
     vous pouvez faire glisser le document du gestionnaire de fichiers vers l'ic&ocirc;ne ou l'entr&eacute;e de liste repr&eacute;sentant 
     l'imprimante dans <application>&app;</application>. <application>&app;</application>
     met automatiquement le document en file d'attente sur l'imprimante sp&eacute;cifi&eacute;e.
  </para>
  
  <note id="filetypes">
    <para>
       Ne glissez-d&eacute;placez les travaux d'impression que pour les types 
       de documents que vos imprimantes acceptent. Si cette m&eacute;thode ne fonctionne pas pour un 
      document particulier, essayez de l'imprimer depuis l'application qui 
      traite normalement ce type de document. L'application imprime le document 
       sur l'imprimante s&eacute;lectionn&eacute;e avec <application>&app;</application>.
    </para>
  </note>
  </sect2>

  <sect2 id="view-printjobs">
   <title>Affichage et annulation de travaux d'impression</title>
   <para>
     Pour consulter les documents se trouvant dans la file d'attente d'une imprimante, cliquez deux fois sur l'imprimante dans la fen&ecirc;tre 
     <guilabel>Gestionnaire d'impression GNOME</guilabel> principale. Ou bien, cliquez avec le bouton droit sur l'imprimante, puis s&eacute;lectionnez
     <guimenuitem>Ouvrir</guimenuitem>. <application>&app;</application> met &agrave; jour les informations de la file d'impression 
     et le statut de l'imprimante &agrave; l'intervalle sp&eacute;cifi&eacute; dans la bo&icirc;te de dialogue <guilabel>Pr&eacute;f&eacute;rences</guilabel>.
     Pour actualiser imm&eacute;diatement la file d'attente et le statut, 
     choisissez <menuchoice><guimenu>Afficher</guimenu><guimenuitem>Rafra&icirc;chir</guimenuitem></menuchoice>.
   </para>
   <para>
     Pour annuler un travail d'impression, localisez un document mis en file d'attente sur une imprimante ou affichez une liste des
     documents mis en file d'attente sur toutes les imprimantes, et proc&eacute;dez comme suit :
     <orderedlist>
     <listitem><para>Choisissez <menuchoice><guimenu>&Eacute;dition</guimenu><guimenuitem>Rechercher un document</guimenuitem></menuchoice>. <application>&app;</application> ouvre la bo&icirc;te de dialogue
     <guilabel>Rechercher un document en cours d'impression</guilabel>. </para></listitem>
     <listitem><para>Tapez le nom du document &agrave; rechercher dans la zone de texte <guilabel>Nom du document</guilabel>. </para></listitem>
     <listitem><para>S&eacute;lectionnez les options de recherche voulues comme suit :
     	<itemizedlist>
     	<listitem><para><guilabel>Correspondance exacte</guilabel></para>
     	<para>Cherche les documents dont le nom correspond exactement au contenu de la 
     	zone de texte <guilabel>Nom du document</guilabel>.</para></listitem>
     	<listitem><para><guilabel>Ignorer Maj/Min</guilabel></para>
     	<para>Ignore la casse du nom de fichier recherch&eacute;.</para></listitem>
     	<listitem><para><guilabel>Mes documents uniquement</guilabel></para>
     	<para>Cherche uniquement les documents que vous avez mis en file d'attente sur l'imprimante.</para></listitem>
     	</itemizedlist>
     </para></listitem>
     <listitem><para>Cliquez sur <guibutton>Lancer la recherche</guibutton> pour lancer la recherche. Les r&eacute;sultats de la
     recherche s'affichent dans la liste <guilabel>R&eacute;sultats de la recherche</guilabel>. 
     <xref linkend="search-screenshot" /> montre une recherche sans r&eacute;sultats.</para></listitem>
     <listitem><para>Pour acc&eacute;der &agrave; un document se trouvant dans une file d'attente, s&eacute;lectionnez le document, puis cliquez sur <guibutton>
     Aller au document</guibutton>.</para></listitem>
     <listitem><para>Pour annuler un travail d'impression, s&eacute;lectionnez le document, puis cliquez sur <guibutton>
     Annuler les documents</guibutton>. Pour s&eacute;lectionner plusieurs documents, appuyez sur <keycap>Ctrl</keycap> 
     tout en s&eacute;lectionnant les documents.</para></listitem>
     <listitem><para>Pour fermer la bo&icirc;te de dialogue<guilabel>Rechercher un document en cours d'impression</guilabel>, 
     cliquez sur <guibutton>Fermer</guibutton>.</para></listitem>
     </orderedlist>
   </para>

	<figure id="search-screenshot">
	<title>Bo&icirc;te de dialogue Rechercher un document en cours d'impression</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/search.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>Affiche la fen&ecirc;tre de recherche du gestionnaire d'impression GNOME</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
         </figure>

  </sect2>
 </sect1>
 

  <sect1 id="general-prefs"> 
    <title>Pr&eacute;f&eacute;rences</title>
    
    <sect2 id="general-config">
    <title>Configuration de &app;</title>

    <para>Pour configurer <application>&app;</application>, choisissez <menuchoice><guimenu>&Eacute;diter</guimenu>
    <guimenuitem>Pr&eacute;f&eacute;rences</guimenuitem>
    </menuchoice>. </para>
      <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>Documents &agrave; afficher</guilabel> </term> 
	    <listitem>
	      <para>S&eacute;lectionnez l'une des options suivantes :
	      <itemizedlist>
	      <listitem><para><guilabel>Seulement les miens</guilabel></para>
	      <para>Affiche uniquement les documents que vous avez mis en file d'attente dans la fen&ecirc;tre de <application>&app;
	      </application>.</para></listitem>
	      <listitem><para><guilabel>Tous</guilabel></para>
	      <para>Affiche les documents mis en file d'attente par tous les utilisateurs dans la fen&ecirc;tre de <application>&app;
	      </application>.</para></listitem>
	      </itemizedlist> </para>
	      <para>Valeur par d&eacute;faut : <guilabel>Tous</guilabel>.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Intervalle de mise &agrave; jour en secondes</guilabel> </term> 
	    <listitem>
	      <para>Utilisez ce curseur pour sp&eacute;cifier la fr&eacute;quence &agrave; laquelle <application>&app;</application>
      		met &agrave; jour les informations relatives &agrave; la file d'attente et au statut de chaque imprimante. </para>
	      <para>Valeur par d&eacute;faut : 30 secondes.</para>
	    </listitem>
	  </varlistentry>
	</variablelist>
  </sect2>	

  <sect2 id="per-printer">
   <title>Affichage des propri&eacute;t&eacute;s pour une imprimante sp&eacute;cifique</title>
   <para>Pour afficher ou d&eacute;finir les propri&eacute;t&eacute;s d'une seule imprimante, cliquez avec le bouton droit, puis s&eacute;lectionnez <guimenuitem>Propri&eacute;t&eacute;s</guimenuitem>.
     <application>&app;</application> affiche la bo&icirc;te de dialogue <guilabel>Propri&eacute;t&eacute;s de l'imprimante</guilabel>.  
   </para>
   <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>&Eacute;tiquette</guilabel> </term> 
	    <listitem>
	      <para>Utilisez cette zone de texte pour sp&eacute;cifier un texte d&eacute;signant l'imprimante. Le libell&eacute; s'affiche
	      dans les vues Ic&ocirc;ne et de liste des imprimantes.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Description</guilabel> </term> 
	    <listitem>
	      <para>Utilisez cette zone de texte pour fournir une br&egrave;ve description de l'imprimante, par exemple, le type	 
	      d'imprimante et son emplacement. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Ic&ocirc;ne</guilabel> </term> 
	    <listitem>
	      <para>Utilisez ce champ pour s&eacute;lectionner l'ic&ocirc;ne &agrave; employer pour repr&eacute;senter l'imprimante. S&eacute;lectionnez une ic&ocirc;ne dans
	      la liste d&eacute;roulante ou cliquez sur <guibutton>Parcourir</guibutton> pour s&eacute;lectionner une ic&ocirc;ne d'imprimante dans
	      le syst&egrave;me de fichiers.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>File d'attente de l'imprimante</guilabel> </term> 
	    <listitem>
	      <para>Ce champ en lecture seule affiche le nom de la file d'attente d'impression associ&eacute;e &agrave; l'imprimante.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>&Eacute;tat</guilabel> </term> 
	    <listitem>
	      <para>Ce champ en lecture seule affiche le statut actuel de l'imprimante.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Nom du p&eacute;riph&eacute;rique</guilabel> </term> 
	    <listitem>
	      <para>Ce champ en lecture seule affiche le nom de p&eacute;riph&eacute;rique mat&eacute;riel de l'imprimante.</para>
	    </listitem>
	  </varlistentry>
	</variablelist>
   
  </sect2>
 </sect1>

</article>

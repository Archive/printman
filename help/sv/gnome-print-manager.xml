<?xml version="1.0"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "0.0.1">
  <!ENTITY manrevision "2.2">
  <!ENTITY date "November 2002">
  <!ENTITY app "Utskriftshanteraren i GNOME">
]>

<!--
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Apr 11, 2002
  
-->

<!-- =============Document Header ============================= -->
<article id="index" lang="sv">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
    <title>Manual f&ouml;r &app;, version&nbsp;&manrevision;</title>

    <copyright> 
      <year>2003</year> 
      <holder>Sun Microsystems</holder> 
    </copyright> 

<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
    <publisher> 
      <publishername> Dokumentationsprojekt f&ouml;r GNOME </publishername> 
    </publisher> 

   &legal;

    <authorgroup>
      <author> 
	<firstname>Suns</firstname> 
	<surname>GNOME-dokumentationsteam</surname> 
	<affiliation> 
	  <orgname>Sun Microsystems</orgname> 
	  <address> <email>gdocteam@sun.com</email> </address>
	</affiliation>
      </author>
      <author> 
	<firstname>Aaron</firstname> 
	<surname>Weber</surname> 
	<affiliation> 
	  <orgname>Dokumentationsprojekt f&ouml;r GNOME</orgname> 
	  <address> <email>aaron@ximian.com</email> </address>
	</affiliation>
      </author>
<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>

    <revhistory>
      <revision> 
	<revnumber>Manual f&ouml;r &app;, version&nbsp;&manrevision;</revnumber> 
	<date>&date;</date> 
	<revdescription> 
	  <para role="author">Suns GNOME-dokumentationsteam
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">Dokumentationsprojekt f&ouml;r GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manual f&ouml;r Utskriftshanteraren i GNOME, V2.1</revnumber> 
	<date>Augusti 2002</date> 
	<revdescription> 
	  <para role="author">Suns GNOME-dokumentationsteam
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">Dokumentationsprojekt f&ouml;r GNOME</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>Manual f&ouml;r Utskriftshanteraren i GNOME, V2.0</revnumber> 
	<date>Maj 2002</date> 
	<revdescription> 
	  <para role="author">Suns GNOME-dokumentationsteam
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="author">Aaron Weber
	    <email>aaron@ximian.com</email>
	  </para>
	  <para role="publisher">Dokumentationsprojekt f&ouml;r GNOME</para>
	</revdescription>
      </revision>
    </revhistory>

    <releaseinfo> I den h&auml;r manualen beskrivs version &appversion; av &app;.
    </releaseinfo>
    <legalnotice> 
      <title>Feedback</title>
      <para>F&ouml;lj instruktionerna p&aring; <ulink url="ghelp:gnome-feedback" type="help">GNOME:s feedbacksida</ulink> om du vill rapportera ett fel eller ge f&ouml;rslag betr&auml;ffande det h&auml;r programmet eller den h&auml;r manualen. 
      </para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice>
  </articleinfo>

  <indexterm> 
    <primary>Utskriftshanterare</primary> 
  </indexterm> 


 <!-- ============= Document Body ============================= -->

 <!-- ============= Introduction ============================== -->
 <sect1 id="printman-intro">
 <title>Introduktion</title>

   <para>
    Med programmet <application>&app;</application> kan du kontrollera och hantera skrivare och skrivarjobb. Du kan utf&ouml;ra f&ouml;ljande &aring;tg&auml;rder med hj&auml;lp av <application>&app;</application>:
     <itemizedlist>
        <listitem><para>Hitta befintliga skrivare.</para></listitem>
        <listitem><para>V&auml;lja den skrivare som du vill anv&auml;nda f&ouml;r olika typer av dokument.</para></listitem>
        <listitem><para>Visa en lista &ouml;ver de skrivarjobb som st&aring;r i k&ouml; vid varje skrivare.</para></listitem>
	<listitem><para>Avbryta utskriftsjobb som placerats i k&ouml;.</para></listitem>
     </itemizedlist>
   </para>
   
   <para>
    <application>&app;</application> &auml;r optimerad f&ouml;r att anv&auml;ndas p&aring; Sun Solaris-system men programmet b&ouml;r g&aring; att k&ouml;ra p&aring; de flesta UNIX- och Linux-system.
   </para>
   <note>
	<para>
  	<application>&app;</application> &auml;r ett program p&aring; anv&auml;ndarniv&aring; f&ouml;r hantering av skrivare. 
  	Programmet ers&auml;tter inte <application>utskriftsverktyg</application> eller andra konfigurationsverktyg p&aring; root-niv&aring;. <application>&app;</application> uppt&auml;cker inte nya skrivare
  	i n&auml;tverket eller l&aring;ter dig inte v&auml;lja drivrutinsalternativ.
 	</para>
   </note>
    
 </sect1>
   
    
 <sect1 id="printman-getting-started">
    <title>Komma ig&aring;ng</title>

    <para>Du kan starta <application>&app;</application> p&aring; f&ouml;ljande s&auml;tt:
    </para>
    <variablelist>
    	<varlistentry>
    		<term><guimenu>Program</guimenu>-menyn</term>
    		<listitem>
    		<para>V&auml;lj <menuchoice><guisubmenu>Systemverktyg</guisubmenu><guimenuitem>Utskriftshanterare</guimenuitem></menuchoice>.</para>
    		</listitem>
    	</varlistentry>
    	<varlistentry>
    		<term>Kommandoraden</term>
    		<listitem>
    		<para>Skriv <command>gnome-print-manager</command> och tryck sedan p&aring; <keycap>Retur</keycap>.</para>
    		<para>		 
    		</para>
    		</listitem>
    	</varlistentry>
    </variablelist>
    
    <para>N&auml;r du startar <application>&app;</application> visas f&ouml;ljande f&ouml;nster.</para>
    
    <figure id="main-window">
    <title>F&ouml;nstret i GNOME:s utskriftshanterare</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/main-window.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>Visar huvudf&ouml;nstret f&ouml;r Utskriftshanteraren i GNOME.</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
    </figure>
   
  </sect1>
  
  <sect1 id="printman-usage"> 
    <title>Anv&auml;ndning</title>

  <sect2 id="printman-display">
   <title>S&aring; h&auml;r visar du tillg&auml;ngliga skrivare</title>
   <para>
     I huvudf&ouml;nstret f&ouml;r <application>&app;</application> visas en lista &ouml;ver de skrivare som &auml;r tillg&auml;ngliga. Som standard visar <application>&app;</application>
    skrivarna som ikoner med skrivarens namn under ikonen. Om du vill visa en lista &ouml;ver tillg&auml;ngliga skrivare med antalet dokument som st&aring;r i k&ouml; vid varje skrivare och med skrivarens status angiven, v&auml;ljer du  <menuchoice><guimenu>Visa</guimenu><guimenuitem>Lista</guimenuitem></menuchoice>.
   </para>
   <para>
Om du vill &auml;ndra i listan &ouml;ver skrivare som visas, v&auml;ljer du <menuchoice><guimenu>Visa</guimenu>
     <guimenuitem>V&auml;lj skrivare som ska visas</guimenuitem></menuchoice>. I dialogf&ouml;nstret <guilabel>V&auml;lj skrivare som ska visas</guilabel> visas alla skrivare som &auml;r tillg&auml;ngliga p&aring; n&auml;tverket. 
V&auml;lj ett av f&ouml;ljande alternativ och klicka sedan p&aring; <guibutton>OK</guibutton>:
   </para>
     <itemizedlist>
       <listitem><para>Om du vill visa alla skrivare som &auml;r tillg&auml;ngliga p&aring; n&auml;tverket, klickar du p&aring; <guibutton>Visa alla</guibutton>. Om du v&auml;ljer det h&auml;r alternativet och en ny skrivare l&auml;ggs till i n&auml;tverket, v&auml;ljer du <menuchoice><guimenu>Visa</guimenu><guimenuitem>Uppdatera</guimenuitem></menuchoice> om du vill visa skrivaren i huvudf&ouml;nstret f&ouml;r <application>&app;</application>.</para></listitem>
       <listitem><para>Om du inte vill visa n&aring;gra skrivare klickar du p&aring; <guibutton>D&ouml;lj alla</guibutton>.</para></listitem>
       <listitem><para>Om du vill visa en del av skrivarna som &auml;r tillg&auml;ngliga p&aring; n&auml;tverket, markerar du kryssrutan intill var och en av de skrivare som du vill visa.</para></listitem>
     </itemizedlist>
	
  </sect2>
  
  <sect2 id="printman-choose">
   <title>S&aring; h&auml;r v&auml;ljer du en standardskrivare</title>
   <para>
     Om du vill ange en skrivare som standardskrivare h&ouml;gerklickar du p&aring; 
     &ouml;nskad skrivarikon i f&ouml;nstret <guilabel>GNOME:s utskriftshanterare</guilabel> och v&auml;ljer
     sedan <guimenuitem>G&ouml;r till standard</guimenuitem>. 
     N&auml;r du har valt en standardskrivare skickas alla skrivarjobb fr&aring;n andra
     program till den valda skrivaren, s&aring;vida du inte anger en
     annan skrivare n&auml;r du skriver ut ett dokument. </para>
  <para>    
 Om du vill skriva ut ett dokument med en viss skrivare kan du dra dokumentet fr&aring;n filhanteraren till ikonen eller posten i listan som motsvarar skrivaren i <application>&app;</application>. <application>&app;</application>
 st&auml;ller automatiskt  dokumentet i k&ouml; vid den skrivare som angetts.
  </para>
  
  <note id="filetypes">
    <para>
       Att dra filer p&aring; detta s&auml;ttet fungerar bara f&ouml;r de dokumenttyper som
       skrivaren accepterar.  Om metoden inte fungerar f&ouml;r ett visst dokument kan du f&ouml;rs&ouml;ka att
       skriva ut fr&aring;n det program som normalt hanterar den typen av dokument. Programmet skriver ut dokumentet till den skrivare som du angav med <application>&app;</application>.
    </para>
  </note>
  </sect2>

  <sect2 id="view-printjobs">
   <title>S&aring; h&auml;r visar du och avbryter skrivarjobb</title>
   <para>
     Om du vill visa dokument i k&ouml;n dubbelklickar du p&aring; skrivaren i huvudf&ouml;nstret f&ouml;r
     <guilabel>GNOME:s utskriftshanterare</guilabel>. Du kan ocks&aring; h&ouml;gerklicka p&aring; skrivaren och v&auml;lja
     <guimenuitem>&Ouml;ppna</guimenuitem>. <application>&app;</application> uppdaterar k&ouml;informationen och 
      status f&ouml;r skrivaren i intervaller som anges i dialogf&ouml;nstret <guilabel>Inst&auml;llningar</guilabel>.
     Om du vill uppdatera k&ouml;- och statusinformation omedelbart, v&auml;ljer du <menuchoice><guimenu>Visa</guimenu><guimenuitem>Uppdatera</guimenuitem></menuchoice>.
   </para>
   <para>
     S&aring; h&auml;r g&ouml;r du om du vill avbryta ett utskriftsjobb, hitta ett dokument som st&aring;r i k&ouml; till en viss skrivare eller visa en lista &ouml;ver
     dokument som st&aring;r i k&ouml; till alla skrivare:
     <orderedlist>
     <listitem><para>V&auml;lj <menuchoice><guimenu>Redigera</guimenu><guimenuitem>S&ouml;k efter
     Dokument</guimenuitem></menuchoice>. <application>&app;</application> &ouml;ppnar dialogrutan 
     <guilabel>S&ouml;k efter dokument som skrivs ut</guilabel>. </para></listitem>
     <listitem><para>Ange namnet p&aring; det dokument som du s&ouml;ker efter i textrutan
     <guilabel>Dokumentnamn</guilabel>. </para></listitem>
     <listitem><para>Markera s&ouml;kalternativen som du vill anv&auml;nda enligt f&ouml;ljande:
     	<itemizedlist>
     	<listitem><para><guilabel>Exakt matchning</guilabel></para>
     	<para>S&ouml;k efter dokument med filnamn som &auml;r en exakt matchning med inneh&aring;llet i textrutan
     	<guilabel>Dokumentnamn</guilabel>.</para></listitem>
     	<listitem><para><guilabel>Ignorera skillnader p&aring; gemener/VERSALER</guilabel></para>
     	<para>Ignorera gemener och versalt i s&ouml;kningen.</para></listitem>
     	<listitem><para><guilabel>Endast mina dokument</guilabel></para>
     	<para>S&ouml;k endast efter dokument som du st&auml;llt i k&ouml; vid skrivaren.</para></listitem>
     	</itemizedlist>
     </para></listitem>
     <listitem><para>Klicka p&aring; <guibutton>Starta S&ouml;kning</guibutton> f&ouml;r att starta s&ouml;kningen. S&ouml;kningens resultat visas i listrutan <guilabel>S&ouml;kresultat</guilabel>. 
     <xref linkend="search-screenshot" /> visar en s&ouml;kning som returnerade inga dokument.</para></listitem>
     <listitem><para>Om du vill g&aring; till ett dokument i en skrivark&ouml;, markerar du dokumentet och klickar sedan p&aring; <guibutton>
     G&aring; till dokument</guibutton>.</para></listitem>
     <listitem><para>Om du vill avbryta ett skrivarjobb, markerar du dokumentet och klickar sedan p&aring; <guibutton>
     Avbryt dokument</guibutton>. Om du vill markera flera dokument trycker du p&aring; <keycap>Control</keycap> 
     samtidigt som du markerar dokumenten.</para></listitem>
     <listitem><para>Om du vill st&auml;nga dialogf&ouml;nstret <guilabel>S&ouml;k efter dokument som skrivs ut</guilabel>, klickar du p&aring; <guibutton>St&auml;ng</guibutton>.</para></listitem>
     </orderedlist>
   </para>

	<figure id="search-screenshot">
	<title>F&ouml;nstret S&ouml;k efter dokument som skrivs ut</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/search.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>Visar s&ouml;kf&ouml;nstret i GNOME:s utskriftshanterare</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
         </figure>

  </sect2>
 </sect1>
 

  <sect1 id="general-prefs"> 
    <title>Inst&auml;llningar</title>
    
    <sect2 id="general-config">
      <title>S&aring; h&auml;r konfigurerar du &app;</title>

    <para>Om du vill konfigurera <application>&app;</application> v&auml;ljer du <menuchoice> <guimenu>Redigera</guimenu> <guimenuitem>Inst&auml;llningar</guimenuitem> 
</menuchoice>. </para>
      <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>Dokument att visa</guilabel> </term> 
	    <listitem>
	      <para>V&auml;lj n&aring;got av f&ouml;ljande alternativ:
	      <itemizedlist>
	      <listitem><para><guilabel>Endast mina</guilabel></para>
	      <para>Visa bara dokument som du st&auml;llt i k&ouml; vid skrivare i f&ouml;nstret <application>&app;
	      </application>.</para></listitem>
	      <listitem><para><guilabel>Allas</guilabel></para>
	      <para>Visa dokument som alla anv&auml;ndare st&auml;llt i k&ouml; vid skrivare i f&ouml;nstret <application>&app;
	      </application>.</para></listitem>
	      </itemizedlist> </para>
	      <para>Standard: <guilabel>Allas</guilabel>.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Uppdateringsintervall (sekunder)</guilabel> </term> 
	    <listitem>
	      <para>Anv&auml;nd det h&auml;r skjutreglaget om du vill ange hur ofta <application>&app;</application>
      		uppdaterar k&ouml;- och statusinformationen f&ouml;r varje skrivare. </para>
	      <para>Standard: 30 sekunder.</para>
	    </listitem>
	  </varlistentry>
	</variablelist>
  </sect2>	

  <sect2 id="per-printer">
   <title>S&aring; h&auml;r anger du egenskaperna f&ouml;r en specifik skrivare</title>
   <para>
     Om du vill visa eller ange inst&auml;llningar f&ouml;r en enstaka skrivare
     h&ouml;gerklickar du p&aring; skrivaren och v&auml;ljer sedan <guimenuitem>Egenskaper</guimenuitem>.
     <application>&app;</application> visar dialogf&ouml;nstret <guilabel>Skrivaregenskaper</guilabel>.  
   </para>
   <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>Etikett</guilabel> </term> 
	    <listitem>
	      <para>Anv&auml;nd den h&auml;r textrutan om du vill ange en textetikett f&ouml;r skrivaren. Etiketten visas i ikon- och listvyerna f&ouml;r skrivarna.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Beskrivning</guilabel> </term> 
	    <listitem>
	      <para>Anv&auml;nd den h&auml;r textrutan om du vill ge en kort beskrivning av skrivaren, till exempel vilken typ av skrivare det &auml;r och var den finns. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Ikon</guilabel> </term> 
	    <listitem>
	      <para>Anv&auml;nd det h&auml;r f&auml;ltet om du vill markera en ikon som ska representera skrivaren. Markera en ikon i den dolda listan eller klicka p&aring; <guibutton>Bl&auml;ddra</guibutton> om du vill markera en skrivarikon fr&aring;n filsystemet.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Skrivark&ouml;</guilabel> </term> 
	    <listitem>
	      <para>I det h&auml;r skrivskyddade f&auml;ltet visas namnet p&aring; skrivarens skrivark&ouml;.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Status</guilabel> </term> 
	    <listitem>
	      <para>I det h&auml;r skrivskyddade f&auml;ltet visas skrivarens aktuella status.</para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>Enhetsnamn</guilabel> </term> 
	    <listitem>
	      <para>I det h&auml;r skrivskyddade f&auml;ltet visas maskinvaruenhetsnamnet f&ouml;r skrivaren.</para>
	    </listitem>
	  </varlistentry>
	</variablelist>
   
  </sect2>
 </sect1>

</article>


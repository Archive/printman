<?xml version="1.0"?><!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY legal SYSTEM "legal.xml">
  <!ENTITY appversion "0.0.1">
  <!ENTITY manrevision "2.2">
  <!ENTITY date "November 2002">
  <!ENTITY app "GNOME Print Manager">
]>

<!--
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Apr 11, 2002
  
-->

<!-- =============Document Header ============================= -->
<article id="index" lang="ko">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
    <title>&app; 설명서 V&manrevision;</title>

    <copyright> 
      <year>2002</year> 
      <holder>Sun Microsystems</holder> 
    </copyright> 

<!-- translators: uncomment this:

  <copyright>
   <year>2002</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->
    <publisher> 
      <publishername> 그놈 문서 프로젝트 </publishername> 
    </publisher> 

   &legal;

    <authorgroup>
      <author> 
	<firstname>Sun</firstname> 
	<surname>그놈 문서 팀</surname> 
	<affiliation> 
	  <orgname>Sun Microsystems</orgname> 
	  <address> <email>gdocteam@sun.com</email> </address>
	</affiliation>
      </author>
      <author> 
	<firstname>Aaron</firstname> 
	<surname>Weber</surname> 
	<affiliation> 
	  <orgname>그놈 문서 프로젝트</orgname> 
	  <address> <email>aaron@ximian.com</email> </address>
	</affiliation>
      </author>
<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
       <othercredit role="translator">
	<firstname>Latin</firstname> 
	<surname>Translator 1</surname> 
	<affiliation> 
	  <orgname>Latin Translation Team</orgname> 
	  <address> <email>translator@gnome.org</email> </address> 
	</affiliation>
	<contrib>Latin translation</contrib>
      </othercredit>
-->
    </authorgroup>

    <revhistory>
      <revision> 
	<revnumber>&app; 설명서 V&manrevision;</revnumber> 
	<date>&date;</date> 
	<revdescription> 
	  <para role="author">Sun 그놈 문서 팀
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">그놈 문서 프로젝트</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>그놈 인쇄 관리자 설명서 V2.1</revnumber> 
	<date>2002년 8월</date> 
	<revdescription> 
	  <para role="author">Sun 그놈 문서 팀
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="publisher">그놈 문서 프로젝트</para>
	</revdescription>
      </revision>
      <revision> 
	<revnumber>그놈 인쇄 관리자 설명서 V2.0</revnumber> 
	<date>2002년 5월</date> 
	<revdescription> 
	  <para role="author">Sun 그놈 문서 팀
	    <email>gdocteam@sun.com</email>
	  </para>
	  <para role="author">Aaron Weber
	    <email>aaron@ximian.com</email>
	  </para>
	  <para role="publisher">그놈 문서 프로젝트</para>
	</revdescription>
      </revision>
    </revhistory>

    <releaseinfo>이 설명서에서는 &app;의 버전 &appversion;에 대해 설명합니다. 
    </releaseinfo>
    <legalnotice> 
      <title>피드백</title>
      <para>&app; 응용프로그램 또는 이 설명서에 대한 버그를 보고하거나
	 의견을 제시하려면    
	<ulink url="ghelp:gnome-feedback" type="help">그놈 피드백 페이지</ulink>에 있는 지시 사항을 따르십시오. 
      </para>
<!-- Translators may also add here feedback address for translations -->
    </legalnotice>
  </articleinfo>

  <indexterm> 
    <primary>인쇄 관리자</primary> 
  </indexterm> 


 <!-- ============= Document Body ============================= -->

 <!-- ============= Introduction ============================== -->
 <sect1 id="printman-intro">
  <title>소개</title>

   <para>
    <application>&app;</application> 응용프로그램을 사용하면 프린터 및 인쇄 작업을 제어하고 관리할 수 있습니다. <application>&app;</application>을(를) 사용하여 다음 작업을 수행할 수 있습니다. 
     <itemizedlist>
        <listitem><para>기존 프린터 감지</para></listitem>
        <listitem><para>다양한 문서 유형을 인쇄하는 데 사용할 프린터 선택</para></listitem>
        <listitem><para>각 프린터에서 대기 중인 인쇄 작업 목록 보기</para></listitem>
	<listitem><para>대기 중인 인쇄 작업 취소</para></listitem>
     </itemizedlist>
   </para>
   
   <para>
    <application>&app;</application>은(는) Sun Solaris 시스템에서 사용하도록 최적화되었지만 대부분의 UNIX 또는 Linux 시스템에서 컴파일되고 실행됩니다. 
   </para>
   <note>
	<para>
  	<application>&app;</application>은(는) 사용자 수준의 프린터 관리용 응용프로그램입니다. 
  	이 응용프로그램은
<application>printtool</application>이나 다른 root 수준의 구성 
	유틸리티 대신 사용할 수 없습니다. <application>&app;</application>은(는) 네트워크에서 새 프린터를 감지할 수 없으며 드라이버 옵션의 선택을 허용하지 않습니다. 
 	</para>
   </note>
    
 </sect1>
   
    
 <sect1 id="printman-getting-started">
    <title>시작하기</title>

    <para>다음 방법으로 <application>&app;</application>을(를) 시작할 수 있습니다. 
    </para>
    <variablelist>
    	<varlistentry>
    <term><guimenu>응용프로그램</guimenu> 메뉴</term>
    		<listitem>
    		<para><menuchoice><guisubmenu>시스템 도구</guisubmenu><guimenuitem>인쇄 관리자</guimenuitem></menuchoice>를 선택합니다.</para>
    		</listitem>
    	</varlistentry>
    	<varlistentry>
    <term>명령줄</term>
    		<listitem>
    		<para><command>gnome-print-manager</command>를 입력한 다음 <keycap>Return</keycap> 키를 누릅니다. </para>
    		<para>		 
    		</para>
    		</listitem>
    	</varlistentry>
    </variablelist>
    
    <para><application>&app;</application>을(를) 시작하면 다음 창이 표시됩니다.</para>
    
    <figure id="main-window">
    <title>그놈 인쇄 관리자 창</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/main-window.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>그놈 인쇄 관리자 주 창이 표시됩니다.</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
    </figure>
   
  </sect1>
  
  <sect1 id="printman-usage"> 
    <title>사용</title>

  <sect2 id="printman-display">
   <title>사용 가능한 프린터를 보려면</title>
   <para>
     <application>&app;</application> 주 창에는 사용할 수 있는 프린터 목록이 표시됩니다. 기본적으로 <application>&app;</application>은(는) 프린터를 아이콘으로 표시하며 아이콘 아래에 프린터 이름을 표시합니다. 사용할 수 있는 프린터 목록을 각 프린터에 대기 중인 문서 수 및 프린터 상태와 함께 보려면 <menuchoice><guimenu>보기</guimenu><guimenuitem>목록</guimenuitem></menuchoice>을 선택합니다. 
   </para>
   <para>
     표시된 프린터 목록을 수정하려면 <menuchoice><guimenu>보기</guimenu>
     <guimenuitem>표시할 프린터 선택</guimenuitem></menuchoice>을 선택합니다. <guilabel>표시할 프린터 선택</guilabel> 대화 상자에는 네트워크에서 사용할 수 있는 모든 프린터가 표시됩니다. 
     다음 옵션 중 하나를 선택하고 <guibutton>확인</guibutton>을 클릭합니다. 
   </para>
     <itemizedlist>
       <listitem><para>네트워크에서 사용할 수 있는 모든 프린터를 표시하려면 <guibutton>
       모두 표시</guibutton>를 클릭합니다. 이 옵션을 선택한 상태에서 새 프린터가 네트워크에 추가되는 경우 <menuchoice><guimenu>보기</guimenu><guimenuitem>새로 고침</guimenuitem></menuchoice>을 선택하여 <application>&app;</application> 주 창에 프린터를 표시합니다. </para></listitem>
       <listitem><para>프린터를 표시하지 않으려면 <guibutton>모두 숨기기</guibutton>를 클릭합니다. </para></listitem>
       <listitem><para>네트워크에서 사용할 수 있는 프린터의 하위 세트를 표시하려면 표시할 각 프린터 옆에 있는 체크 상자를 선택하십시오. </para></listitem>
     </itemizedlist>
	
  </sect2>
  
  <sect2 id="printman-choose">
   <title>기본 프린터를 선택하려면</title>
   <para>
     프린터를 기본 프린터로 선택하려면  
     <guilabel>그놈 인쇄 관리자</guilabel> 창에서 해당 프린터를 나타내는 아이콘을 마우스 오른쪽 버튼으로 클릭한 다음  
     <guimenuitem>기본값으로 설정</guimenuitem>을 선택합니다. 

     기본 프린터를 선택하면 문서를 인쇄할 때 다르게 지정하지 않는 한 다른 
     응용프로그램의 모든 인쇄 요청이 선택한 프린터로 전달됩니다. </para>
  <para>    
     문서를 특정 프린터로 인쇄하려면 파일 관리자에서 <application>&app;</application>의 프린터를 나타내는 아이콘이나 목록 항목으로 문서를 끌면 됩니다. <application>&app;</application>이(가) 문서를 지정된 프린터에 자동으로 대기시킵니다. 
  </para>
  
  <note id="filetypes">
    <para>
       끌어서 놓기를 사용하는 인쇄 작업은 인쇄될 파일이 해당 프린터가 지원하는 문서 유형일 경우에만 가능합니다. 끌어서 놓기를 사용하는 인쇄 작업이 특정 문서에 대해 수행되지 않으면 일반적으로 해당 유형의 문서를 처리하는 응용프로그램에서 문서를 인쇄해 보십시오. 응용프로그램은 <application>&app;</application>에서 선택한 프린터로 문서를 인쇄합니다. 
    </para>
  </note>
  </sect2>

  <sect2 id="view-printjobs">
   <title>인쇄 작업을 보거나 취소하려면</title>
   <para>
     프린터의 대기열에 있는 문서를 보려면 
     <guilabel>그놈 인쇄 관리자</guilabel> 주 창에 있는 프린터를 두 번 클릭합니다. 또는 프린터를 마우스 오른쪽 버튼으로 클릭한 다음 <guimenuitem>열기</guimenuitem>를 선택합니다. <application>&app;</application>은(는) <guilabel>환경 설정</guilabel> 대화 상자에 지정된 간격마다 대기열 정보와 프린터 상태를 갱신합니다. 
     대기열 및 상태 정보를 즉시 새로 고치려면 <menuchoice><guimenu>보기</guimenu><guimenuitem>새로 고침</guimenuitem></menuchoice>을 선택하십시오. 
   </para>
   <para>
     인쇄 작업을 취소하려면 프린터에 대기 중인 문서를 찾거나 모든 프린터에서 대기 중인 문서 목록을 보고 다음 단계를 수행하십시오. 
     <orderedlist>
     <listitem><para><menuchoice><guimenu>편집</guimenu><guimenuitem>문서 검색</guimenuitem></menuchoice>을 선택합니다. <application>&app;</application>은(는)
     <guilabel>인쇄 중인 문서 검색 창</guilabel> 대화 상자를 엽니다. </para></listitem>
     <listitem><para>검색할 문서의 이름을 <guilabel>문서 이름</guilabel> 텍스트 상자에 입력합니다. </para></listitem>
     <listitem><para>다음 검색 옵션 중에서 필요한 옵션을 선택합니다. 
     	<itemizedlist>
     	<listitem><para><guilabel>정확히 일치</guilabel> </para>
     	<para>파일 이름이 <guilabel>문서 이름</guilabel> 텍스트 상자의 내용과 정확히 일치하는 문서를 검색합니다. </para></listitem>
     	<listitem><para><guilabel>대소문자 무시</guilabel> </para>
     	<para>검색할 때 파일 이름의 대소문자를 무시합니다. </para></listitem>
     	<listitem><para><guilabel>내 문서만</guilabel> </para>
     	<para>자신이 프린터에 대기시킨 문서만 검색합니다. </para></listitem>
     	</itemizedlist>
     </para></listitem>
     <listitem><para><guibutton>검색 시작</guibutton>을 클릭하여 검색을 시작합니다. 검색의 결과가 <guilabel>검색 결과</guilabel> 목록에 표시됩니다. 
     <xref linkend="search-screenshot" />은(는) 문서가 반환되지 않은 결과를 표시합니다. </para></listitem>
     <listitem><para>프린터 대기열에 있는 문서로 이동하려면 해당 문서를 선택한 다음 <guibutton>
     문서로 이동</guibutton>을 클릭합니다. </para></listitem>
     <listitem><para>인쇄 작업을 취소하려면 해당 문서를 선택한 다음 <guibutton>
     문서 취소</guibutton>를 클릭합니다. 여러 문서를 선택하려면 <keycap>Ctrl</keycap> 키를 누른 채 문서를 선택합니다. </para></listitem>
     <listitem><para><guilabel>인쇄 중인 문서 검색 창</guilabel> 대화 상자를 닫으려면 <guibutton>닫기</guibutton>를 클릭합니다. </para></listitem>
     </orderedlist>
   </para>

	<figure id="search-screenshot">
	<title>인쇄 중인 문서 검색 대화 상자</title>
	<screenshot>
		<mediaobject>
			<imageobject>
				<imagedata fileref="figures/search.png" srccredit="Aaron Weber" format="PNG"/>
			</imageobject>
			<textobject>
				<phrase>그놈 인쇄 관리자의 검색 창을 표시합니다.</phrase>
		        </textobject>
		</mediaobject>
         </screenshot>
         </figure>

  </sect2>
 </sect1>
 

  <sect1 id="general-prefs"> 
    <title>환경 설정</title>
    
    <sect2 id="general-config">
      <title>&app;을(를) 구성하려면</title>

    <para><application>&app;</application>을(를) 구성하려면 
<menuchoice> <guimenu>편집</guimenu>
  <guimenuitem>환경 설정</guimenuitem>
  </menuchoice>을 선택합니다. </para>
      <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>표시할 문서</guilabel> </term> 
	    <listitem>
	      <para>다음 옵션 중 하나를 선택합니다. 
	      <itemizedlist>
	      <listitem><para><guilabel>내 문서만</guilabel> </para>
	      <para>자신이 프린터에 대기시킨 문서만 <application>&app;
	      </application> 창에 표시합니다. </para></listitem>
	      <listitem><para><guilabel>모든 사람의 문서</guilabel> </para>
	      <para>프린터에 대기 중인 모든 문서를 <application>&app;
	      </application> 창에 표시합니다. </para></listitem>
	      </itemizedlist> </para>
	      <para>기본값: <guilabel>모든 사람의 문서</guilabel></para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>갱신 간격(초)</guilabel> </term> 
	    <listitem>
	      <para>이 슬라이더를 사용하여 <application>&app;</application>에서 대기열 정보 및 각 프린터의 상태를 갱신하는 빈도를 지정합니다. </para>
	      <para>기본값: 30초</para>
	    </listitem>
	  </varlistentry>
	</variablelist>
  </sect2>	

  <sect2 id="per-printer">
   <title>특정 프린터의 등록 정보를 설정하려면</title>
   <para>
     한 프린터에 대한 등록 정보를 보거나 설정하려면 프린터를 마우스 오른쪽 버튼으로 
     클릭한 다음 <guimenuitem>등록 정보</guimenuitem>를 선택합니다. 
     <application>&app;</application>에서 <guilabel>프린터 등록 정보</guilabel> 대화 상자를 표시합니다. 
   </para>
   <variablelist>
	  <varlistentry> 
	    <term> 
	      <guilabel>레이블</guilabel> </term> 
	    <listitem>
	      <para>이 텍스트 상자를 사용하여 프린터의 텍스트 레이블을 지정할 수 있습니다. 레이블은 프린터의 아이콘과 목록 보기에 표시됩니다.
	      </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>설명</guilabel> </term> 
	    <listitem>
	      <para>이 텍스트 상자를 사용하여 프린터에 대한 간략한 설명(예: 프린터 종류 및 프린터 위치)을 제공할 수 있습니다. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>아이콘</guilabel> </term> 
	    <listitem>
	      <para>이 필드를 사용하여 프린터를 나타내는 데 사용할 아이콘을 선택할 수 있습니다. 드롭다운 목록에서 아이콘을 선택하거나 <guibutton>찾아보기</guibutton>를 클릭하여 파일 시스템에서 프린터 아이콘을 선택합니다. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>프린터 대기열</guilabel> </term> 
	    <listitem>
	      <para>이 읽기 전용 필드에는 프린터의 프린터 대기열 이름이 표시됩니다. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>상태</guilabel> </term> 
	    <listitem>
	      <para>이 읽기 전용 필드에는 프린터의 현재 상태가 표시됩니다. </para>
	    </listitem>
	  </varlistentry>
	  <varlistentry> 
	    <term> 
	      <guilabel>장치 이름</guilabel> </term> 
	    <listitem>
	      <para>이 읽기 전용 필드에는 프린터의 하드웨어 장치 이름이 표시됩니다. </para>
	    </listitem>
	  </varlistentry>
	</variablelist>
   
  </sect2>
 </sect1>

</article>
